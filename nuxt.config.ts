// https://nuxt.com/docs/api/configuration/nuxt-config

import { locales } from './locales/index'

export default defineNuxtConfig({
  app: {
    head: {
           link: [{ rel: 'icon', type: 'image/svg', href: "./favicon.png" }]
        }
  },
  devtools: { enabled: true },
  css: [
    'vuetify/lib/styles/main.css',
    '@mdi/font/css/materialdesignicons.min.css'
  ],
  build: {
    transpile: ['vuetify'],
    node: {
      fs: 'empty'
    },
  },
  modules: [
    ['@pinia/nuxt',{ autoImports: ['defineStore'] }],
    '@sidebase/nuxt-auth',
    '@nuxtjs/i18n',
    '@vee-validate/nuxt',
  ],
  imports: {
    dirs: ['stores']
  },
  auth: {   
    globalAppMiddleware: true,
    navigateUnauthenticatedTo: '/login',
    provider: {            
      type: 'local'        
    },
    baseUrl: 'http://localhost:3000/',
  },
  i18n: {
    locales: Object.keys(locales).map(key => ({
      code: key,
      name: locales[key],
      iso: key
    })),
    detectBrowserLanguage: {
        useCookie: true,
        cookieKey: 'hepe_i18n_redirected',
        redirectOn: 'root'
    },
    strategy: 'no_prefix',
    defaultLocale: 'es'
  },
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL || 'https://localhost:3000/login',
    },
  },
  components: [
    {
      path: '~/components', // will get any components nested in let's say /components/test too
      pathPrefix: false,
    }
  ],
  routeRules: {
    '/': { ssr: false }
  },
})
