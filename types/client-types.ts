interface Course {
  id: number,
  name: string,
  academicYear: string,
  region: string,
  locale: string,
}

interface BasicItem {
  id: number,
  content: string,
}

interface EvaluationCriteria extends BasicItem {}
interface BasicWisdom extends BasicItem {}
interface UserDefinedBasicWisdom extends BasicWisdom {
  isUserDefined: true
}
interface EcosocialEvaluationCriteria extends BasicItem {}
interface EcosocialBasicWisdom extends BasicItem {}
interface UserDefinedEcosocialBasicWisdom extends EcosocialBasicWisdom {
  isUserDefined: true
}

interface SpecificSkill extends BasicItem {
  evCrs: Array<EvaluationCriteria>
  evCrToBWMatrix: Array<Array<boolean>>
}

interface SSIndexes {
  sSIndex: number
  evCrIndex: number
  bWIndex: number
}

interface EcosocialLearning extends BasicItem {
  group: string
  eEvCrs: Array<EcosocialEvaluationCriteria>
  selectedEEvCrs: Array<boolean>
  eBWs: Array<EcosocialBasicWisdom>
  eEvCrToEBWMatrix: Array<Array<boolean>>
  uDEBWs: Array<UserDefinedEcosocialBasicWisdom>
  eEvCrToUDEBWMatrix: Array<Array<boolean>>
}

interface ELIndexes {
  eLIndex: number
  eEvCrIndex: number
  eBWIndex: number
}

interface BasicWisdomTree extends BasicWisdom {
  children: Array<BasicWisdomTree>
}

interface EvCrIndexes {
  sSIndex: number
  evCrIndex: number
}

interface EEvCrIndexes {
  eLIndex: number
  eEvCrIndex: number
}

interface BWIndexes {
  bWIndex: number
  keep: boolean // Set to true on step 8, set to false if checkbox unset
}

interface EBWIndexes {
  eLIndex: number
  eBWIndex: number
  keep: boolean // Set to true on step 8, set to false if checkbox unset
}

interface UDEBWIndexes {
  eLIndex: number
  uDEBWIndex: number
  keep: boolean // Set to true on step 8, set to false if checkbox unset
}

interface LearningUnit {
  order: number
  name: string
  partialEvCrs: Array<EvCrIndexes>
  evCrToBWMatrix: Array<Array<boolean>> // Temporary var to handle changes in previous steps (from step2 to step8). The first index of this matrix references the selectedEvCrs positions
  partialBWs: Array<BWIndexes>  // Final selection of bWs, they need to be updated
  partialEEvCrs: Array<EEvCrIndexes>
  eEvCrToEBWMatrix: Array<Array<boolean>> // The first index of this matrix references the selectedEEvCrs positions
  partialEBWs: Array<EBWIndexes>
  eEvCrToUDEBWMatrix: Array<Array<boolean>> // The first index of this matrix references the selectedEEvCrs positions
  partialUDEBWs: Array<UDEBWIndexes>
  uDBWs: Array<BasicWisdom>
  filterUDBWs: Array<boolean>
}

interface LUBWIndexes {
  lUIndex: number
  pBWIndex: number
  uDBWIndex: number
}

interface LUEBWIndexes {
  lUIndex: number
  eLIndex: number
  eBWIndex: number
  uDEBWIndex: number
}

interface ProgramRecord {
  id: number
  userId: string
  stringifiedProgram: string
}

interface ProgramState {
  id: number
  course: Course
  specificSkills: Array<SpecificSkill>
  basicWisdoms: Array<BasicWisdom>
  ecosocialLearnings: Array<EcosocialLearning>
  learningUnits: Array<LearningUnit>
  currentStep: number
  lastSavedProgram?: string
  saved?: boolean
}

interface DocText {
  content: string
}

interface DocBasicWisdom  extends DocText {}
interface DocEvaluationCriteria extends DocText {}
interface DocEcosocialBasicWisdom  extends DocText {}
interface DocEcosocialEvaluationCriteria extends DocText {}

interface DocSpecificSkill extends DocText {
  evCrs: Array<DocEvaluationCriteria>
}

interface DocEcosocialLearning extends DocText {
  eEvCrs: Array<DocEcosocialEvaluationCriteria>
}

interface DocLearningUnit {
  name: string
  sSs: Array<DocSpecificSkill>
  eLs: Array<DocEcosocialLearning>
  bWs: Array<DocBasicWisdom>
}