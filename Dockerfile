ARG NODE_VERSION=20.10

FROM node:${NODE_VERSION}-slim as base

# Install openssl
RUN apt-get update -y && apt-get install -y openssl

ARG PORT=3000

ENV NODE_ENV=production

WORKDIR /src

# Build
FROM base as build

COPY --link package.json package-lock.json .
RUN npm install --production=false

COPY --link . .

RUN npm run db:generate
RUN npm run build
RUN npm prune

# Run
FROM base

ENV PORT=$PORT

COPY --from=build /src/.output /src/
# Optional, only needed if you rely on unbundled dependencies
# COPY --from=build /src/node_modules /src/node_modules
COPY --from=build /src/server/emails /src/server/emails
COPY --from=build /src/locales /src/locales

CMD [ "node", "./server/index.mjs" ]

EXPOSE $PORT