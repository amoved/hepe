import type { SMTPConfig, SmtpConnection } from '../domain/smtpConnection';
import type { Notifier } from "../domain/notifier";
import { MailerService } from '../application/mailer.service';

export class MailerController {
    private readonly mailerService: MailerService;

    constructor(mailerService: MailerService){
        this.mailerService = mailerService;
    }

    public async verifySmtpConnection(options: SmtpConnection): Promise<Boolean>{
        return await this.mailerService.verifySmtpConnection(options)
    }

    public updateNotifier(notifier: Notifier){
        return this.mailerService.updateNotifier(notifier)
    }

    public updateSmtpConfig(config: SMTPConfig){
        return this.mailerService.updateSmtpConfig(config)
    }

    public async send(addresses: string[], template: string, locale: string) {
        return await this.mailerService.send(addresses, template, locale)
    }
}

