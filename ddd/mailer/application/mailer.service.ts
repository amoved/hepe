import { configDomainEventEmitter } from "~/ddd/config/domain/events";
import { Notifier } from "../domain/notifier";
import type { IMailerPort, MailerOptions } from "../domain/ports/mailer.interface"
import type { SMTPConfig, SmtpConnection } from '../domain/smtpConnection';
import { authDomainEventEmitter } from '~/ddd/auth/domain/events';
import type { User } from "~/ddd/auth/domain/user";
import type { ILoggerPort } from '~/ddd/log/application/logger.port.interface';

export class MailerService {
    private loggerPort: ILoggerPort
    private mailerPort: IMailerPort
    private options: MailerOptions
    private baseUrl: string

    constructor(baseUrl: string, mailerOptions: MailerOptions, mailerPort: IMailerPort,loggerPort: ILoggerPort){
        this.loggerPort = loggerPort
        this.options = mailerOptions
        this.mailerPort = mailerPort
        this.baseUrl = baseUrl

        // Bind this to the function context
        this.notifyUserCreated = this.notifyUserCreated.bind(this)
        this.notifyUserPasswordRecovery = this.notifyUserPasswordRecovery.bind(this)

        // User service events
        authDomainEventEmitter.on('USER_CREATED', this.notifyUserCreated)
        this.loggerPort.debug(`[Mailer] Suscribed to USER_CREATED event.`)

        authDomainEventEmitter.on('USER_RECOVER', this.notifyUserPasswordRecovery)
        this.loggerPort.debug(`[Mailer] Suscribed to USER_CREATED event.`)

        configDomainEventEmitter.on('SMTP_UPDATED', this.updateConfig)
        this.loggerPort.debug(`[Mailer] Suscribed to SMTP_UPDATED event.`)
    }

    public async verifySmtpConnection(options: SmtpConnection): Promise<boolean>{
        return await this.mailerPort.verifySmtpConnection(options)
    }

    public updateNotifier(notifier: Notifier){
        return this.mailerPort.updateNotifier(notifier)
    }

    public updateSmtpConfig(config: SMTPConfig) {
        this.loggerPort.debug(`[Mailer/updateSmtpConfig] Updating SMTP config`)
        return this.mailerPort.updateSmtpConfig(config)
    }

    public async send(addresses: string[], template: string, locale: string, params: object = {}) {
        return await this.mailerPort.send(addresses, template, locale, params)
    }

    private updateConfig(config: SMTPConfig) {
        this.loggerPort.debug(`[Mailer/updateConfig] Updating SMTP config`)
        this.updateNotifier(new Notifier(this.options.notifier.fromName, config.email, this.options.notifier.locale))
        this.updateSmtpConfig(config)
    }

    private async notifyUserCreated(user: User): Promise<void> {
        console.log(`[Mailer] On my way to welcome ${user.name}`)
        try {
            const verificationLink = `${this.baseUrl}/verify?code=${user.temporaryAccessToken}`
            await this.mailerPort.send([user.email],'welcome',user.locale,{userName: user.name,verificationLink})
        } catch (e) {
            console.log(e)
        }
        
    }

    private async notifyUserPasswordRecovery(user: User):Promise<void> {
        console.log(`[Mailer] Sending user recovery link to ${user.name}`)
        try {
            const recoveryLink = `${this.baseUrl}/recover?code=${user.temporaryAccessToken}`
            await this.mailerPort.send([user.email],'recover',user.locale,{recoveryLink})
        } catch (e) {
            console.log(e)
        }
    }


}