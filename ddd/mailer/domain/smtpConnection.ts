export interface SMTPConfig {
    [index: string]: string | boolean | undefined

    host: string
    port: string
    useTLS: boolean
    email: string
    password: string
}

export class SmtpConnection {
    host: string
    port: string
    useTLS: boolean
    email: string
    password: string

    constructor(host: string, port: string, useTLS: boolean, email:string, password: string) {
        this.host = host
        this.port = port
        this.useTLS = useTLS
        this.email = email
        this.password = password
    }
}

