import type { SMTPConfig, SmtpConnection } from '../smtpConnection'
import type { Notifier } from '../notifier';

export interface MailerOptions {
    notifier: Notifier,
    smtpConnection: SmtpConnection
}

export interface IMailerPort {
    verifySmtpConnection( config: SmtpConnection ): Promise<boolean>;
    updateNotifier( config: Notifier ): boolean;
    updateSmtpConfig( config: SMTPConfig ): boolean;
    send( to: string[], template: string, locale: string, params: object ): Promise<boolean>;
}