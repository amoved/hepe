export class Notifier {
    fromName: string
    fromAddress: string
    locale: string

    constructor(fromName: string, fromAddress: string, locale: string = 'es') {
        this.fromName = fromName
        this.fromAddress = fromAddress
        this.locale = locale
    }
}

