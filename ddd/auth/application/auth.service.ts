import { User, Status, Role } from '../domain/user'
import { License } from '../domain/license'
import { type IAuthRepository } from "../domain/ports/auth.repository.interface"
import type { ICryptoPort } from '~/ddd/shared/application/ports/crypto.port.interface'
import type { ILoggerPort } from '~/ddd/log/application/logger.port.interface'
import { authDomainEventEmitter } from '../domain/events'
import { educationDomainEventEmitter } from '../../education/domain/events'

export class AuthService {
    private tokenExpiresIn:string = '6h' 
    private loggerPort: ILoggerPort
    private authRepository: IAuthRepository
    private cryptoPort: ICryptoPort

    constructor(authRepository: IAuthRepository, cryptoPort: ICryptoPort, loggerPort: ILoggerPort){
        this.loggerPort = loggerPort
        this.authRepository = authRepository
        this.cryptoPort = cryptoPort

        // Bind this to the function context
        this.useLicense = this.useLicense.bind(this)

        educationDomainEventEmitter.on('PROGRAM_COMPLETED', this.useLicense)
        console.log("Auth service suscribed to PROGRAM_COMPLETED event")
    }

    public async licenses(): Promise<Array<License> | false>{
        this.loggerPort.debug("[Auth: licenses] Get all licenses")
        return await this.authRepository.getAllLicenses()
    }

    public async license(id: string): Promise<License | false>{
        this.loggerPort.debug("[Auth: license] Get license by id")
        return await this.authRepository.getLicenseById(id)
    }

    public async licenseUsageLimit(id: string): Promise<number | false>{
        this.loggerPort.debug("[Auth: licenseUsageLimit] Get license usage limit")
        const license: License = await this.authRepository.getLicenseById(id)
        if(license){
            return license.usageLimit
        }
        return false
    }

    public async createLicense( name: string, maxUsers: number, usageLimit: number ): Promise<License | false> {
        this.loggerPort.debug("[Auth: createLicense] License creation request")

        const code = 'HEPE-' + String(Date.now())

        const license = await this.authRepository.createLicense(name, code, maxUsers, usageLimit)

        // Log results
        if(!!license){
            this.loggerPort.info(`[Auth: createLicense] License ${license.id} successfuly created`)
            authDomainEventEmitter.emit('LICENSE_CREATED', license)
            this.loggerPort.debug("[LICENSE_CREATED] Domain event emitted!!")
        } else {
            this.loggerPort.error("[Auth: createLicense] License registration failed")
        }

        return license
    }

    public async updateLicense( id: string, name: string,  maxUsers: number, usageLimit: number ): Promise<License | false> {
        this.loggerPort.debug(`[Auth: updateLicense] License ${id} update request`)

        let license = await this.authRepository.getLicenseById(id)

        if(!!license){
            // Validate params
            if(license.maxUsers > maxUsers || license.usageLimit > usageLimit) {
                return false
            }
            license = await this.authRepository.updateLicense(id, name, maxUsers, usageLimit)
            this.loggerPort.info(`[Auth: updateLicense] License ${license.id} successfuly updated`)
            authDomainEventEmitter.emit('LICENSE_UPDATED', license)
            this.loggerPort.debug("[LICENSE_UPDATED] Domain event emitted!!")
        } else {
            this.loggerPort.error("[Auth: updateLicense] License update failed")
        }

        return license
    }

    public async deleteLicense( id: string ): Promise<boolean> {
        this.loggerPort.debug(`[Auth: delete] License ${id} deletion request`)

        const deleted: boolean = await this.authRepository.deleteLicense(id)
        if(deleted) {
            authDomainEventEmitter.emit('LICENSE_DELETED', id)
            this.loggerPort.debug("[LICENSE_DELETED] Domain event emitted!!")
        }

        return deleted
    }

    public async useLicense( userId: string): Promise<void> {

        this.loggerPort.debug(`[Auth: useLicense] User ${userId} has used the licensed`)

        let user: User | false = await this.authRepository.getUserById(userId)
        if(user.id) {
            this.loggerPort.debug(`[Auth: useLicense] User found, updating license counter`)
            const userUpdated = await this.authRepository.setLicenseUsageCounter(user.id, user.licenseUsageCounter + 1)
        }
    }

    // Registers a user account that will need to be activated trhough e-mail.
    public async register(name: string, email: string, password: string, code: string, locale: string): Promise<User | false>{

        this.loggerPort.debug("[Auth: register] User registration request")
    
        this.loggerPort.debug("[Auth: register] Validating license")
        const license = await this.authRepository.getLicenseByCode(code)
        
        // Check license exist and its not full yet
        if(license.id == undefined || license.numUsers === license.maxUsers) {
            this.loggerPort.debug(`[Auth: register] License doesn't exist or is full`)
            return false
        }

        this.loggerPort.debug(`[Auth: register] License name: ${license.name}`)

        const passwordHash = await this.cryptoPort.hashPassword(password)

        let user = await this.authRepository.registerUser(name, email, passwordHash, license.id, locale)
        const payload = { id: user.id }
        const temporaryAccessToken = this.cryptoPort.generateAccessToken(payload,this.tokenExpiresIn)
        user = await this.authRepository.setUserTemporaryAccessToken(user.id,temporaryAccessToken)

        // Log results
        if(!!user){
            this.loggerPort.info(`[Auth: register] User ${user.id} successfuly created`)
            authDomainEventEmitter.emit('USER_CREATED', user)
            this.loggerPort.debug("[USER_CREATED] Domain event emitted!!")
        } else {
            this.loggerPort.error("[Auth: register] User registration failed")
        }

        return user
    }

    public async verify(temporaryAccessToken: string): Promise <boolean>{

        this.loggerPort.info(`[User: verify] Start verification process for ${temporaryAccessToken}`)
        
        try {
            // Check access token on db and its validity
            let user = await this.authRepository.getUserByTemporaryAccessToken(temporaryAccessToken)
            this.loggerPort.info(`Found user with id: ${user.id}`)
            
            const tokenData = this.cryptoPort.verifyAccessToken(temporaryAccessToken)
            
            this.loggerPort.info(`[User: verify] User to verify: ${tokenData.id}`)
            this.loggerPort.info(`[User: verify] Token: ${temporaryAccessToken}`)
            
            // If valid token
            if(!!user && tokenData.id){
                user = await this.authRepository.getUserById(tokenData.id)
                // If user exists in DB
                if(!!user){
                    // Clear temporary access token
                    user = await this.authRepository.setUserTemporaryAccessToken(user.id,'')
                    // Verify user
                    const updatedUser = await this.authRepository.setUserVerified(user.id)
                    // // Increment license user counter and add user to license list
                    const license = await this.authRepository.getLicenseById(user.licenseId)
                    this.authRepository.setLicenseNumUsers(license.id, license.numUsers+1)
                    this.authRepository.addLicenseUser(license.id, user.id)
                    // Notify other domains
                    authDomainEventEmitter.emit('USER_VERIFIED', updatedUser)
                    return true
                }
            }
        } catch(e) {
            this.loggerPort.error("[User: verify] Verification process failed")
            this.loggerPort.error(e)
        }
        this.loggerPort.info(`[User: verify] No user found or access token is invalid`)


        return false
    }

    public async recover(email: string): Promise <boolean>{

        this.loggerPort.debug("[User: recover] User recover request")

        // Find user by email
        let user = await this.authRepository.getUserByEmail(email)
        // Generate link
        const payload = { id: user.id }
        const temporaryAccessToken = await this.cryptoPort.generateAccessToken(payload,this.tokenExpiresIn)
        user = await this.authRepository.setUserTemporaryAccessToken(user.id,temporaryAccessToken)
        // Set pending password restore
        user = await this.authRepository.setUserPendingPasswordRestore(user.id, true)

        if(!!user){
            authDomainEventEmitter.emit('USER_RECOVER', user)
            this.loggerPort.debug("[USER_RECOVER] Domain event emitted!!")
            return true
        }

        return false
    }

    public async restore(temporaryAccessToken: string, password: string): Promise<User | false> {

        try {
            // Check access token on db and its validity
            let user = await this.authRepository.getUserByTemporaryAccessToken(temporaryAccessToken)
            const tokenData = this.cryptoPort.verifyAccessToken(temporaryAccessToken)
            
            if(!!user && tokenData.id){
                // Set new password
                const passwordHash = await this.cryptoPort.hashPassword(password)
                user = await this.authRepository.setUserPassword(user.id, passwordHash)
                // Clear temporary access token and pendnig password restore
                user = await this.authRepository.setUserTemporaryAccessToken(user.id, '')
                user = await this.authRepository.setUserPendingPasswordRestore(user.id, false)

                return user
            }

        } catch(e) {
            this.loggerPort.error(e)
        }

        throw createError({
            statusCode: 403,
            statusMessage: "[User: restore] Not authorized",
        });

        return false

    }

    public async login(email: string, password: string): Promise<string> {

        // Check if user exists
        let user = await this.authRepository.getUserByEmail(email)
        
        // Check if user is verified
        if(user && user.verified){
            try {
                // Check if password is correct
                const authenticated = await this.cryptoPort.testPassword(password, user.passwordHash)
                // Set LOGGED_IN status
                user = await this.authRepository.setUserStatus(user.id, Status.LOGGED_IN)
                
                if ( !!!user ) {
                    throw createError({
                      statusCode: 403,
                      statusMessage: "[User: login] Not authorized",
                    });
                } else {
                    this.loggerPort.debug(`[User: login] ${user.id} logged in.`)
                } 
                
                // Get temporary access token
                const accessToken = await this.cryptoPort.generateAccessToken({id: user.id},this.tokenExpiresIn)

                return accessToken
            } catch (error) {
                this.loggerPort.error(error)
                throw(error)
            }
        }

        throw createError({
            statusCode: 403,
            statusMessage: "[User: login] User doesn't exist or is not verified",
        });

        return false

    }

    public async session(id: string): Promise<User | false> {
        return await this.authRepository.getUserById(id)
    }

    public async logout(id: string): Promise<boolean> {
        const user = await this.authRepository.setUserStatus(id, Status.LOGGED_OUT)
        this.loggerPort.debug(`[User: logout] ${user?.id} logged out.`)
        return !!user
    }
}