import { DomainEventEmitter } from '../../shared/domain/DomainEventEmitter';
import type { User } from "./user"
import type { License } from "./license"

// Define all events names and input params
type EventMap = {
    'USER_CREATED': [user: User],
    'USER_VERIFIED': [user: User],
    'USER_RECOVER': [user: User],
    'USER_LOGGED_IN': [user: User],
    'USER_LOGGED_OUT': [user: User],
    'USER_UPDATED': [user: User],
    'USER_DELETED': [user: User],
    'LICENSE_CREATED': [license: License],
    'LICENSE_UPDATED': [license: License],
    'LICENSE_DELETED': [id: String],
}

export const authDomainEventEmitter = new DomainEventEmitter<EventMap>()