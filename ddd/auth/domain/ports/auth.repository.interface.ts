import type { User, Status, Role } from '../user'
import type { License } from '../license'

export interface IAuthRepository {
    getAllLicenses(): Promise<Array<License> | false>
    registerUser(name: string, email: string, passwordHash: string, licenseId: string, locale: string): Promise<User | false>;
    createLicense(name: string, code: string, maxUsers: number, usageLimit: number): Promise<License | false>;
    updateLicense(id: string, name: string, maxUsers: number, usageLimit: number): Promise<License | false>;
    deleteLicense(id: string): Promise<boolean>;
    getLicenseById(id: string): Promise<License | false>;
    getLicenseByCode(code: string): Promise<License | false>;
    getUserById(id: string): Promise<User | false>;
    getUserByEmail(email: string): Promise<User | false>;
    getUserByTemporaryAccessToken(temporaryAccessToken: string): Promise<User | false>;
    setLicenseNumUsers(id: string, numUsers: number): Promise<boolean>;
    setLicenseUsageCounter(id: string, licenseUsageCounter: number): Promise<boolean>
    addLicenseUser(id: string, userId: string): Promise<boolean>;
    setUserStatus(id: string, status: Status): Promise<User | false>;
    setUserRole(id: string, role: Role): Promise<User | false>;
    setUserPassword(id: string, passwordHash: string): Promise<User | false>;
    setUserVerified(id: string): Promise<User | false>;
    setUserPendingPasswordRestore(id: string, pending: boolean): Promise<User | false>;
    setUserTemporaryAccessToken(id: string, temporaryAccessToken: string): Promise<User | false>;
}