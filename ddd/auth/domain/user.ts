export enum Status {
    LOGGED_IN,
    LOGGED_OUT
}

export enum Role {
    USER,
    ADMIN
}

export class User {
    id: string
    name: string
    email: string
    passwordHash: string
    locale: string
    status: Status
    role: Role
    createdAt: Date
    verified: boolean
    pendingPasswordRestore: boolean
    temporaryAccessToken: string // Used for email verification and password recovery
    licenseUsageCounter: number

    constructor(name: string, email:string, passwordHash: string, locale: string = 'es') {
        this.id = '' //uuid
        this.name = name
        this.email = email
        this.passwordHash = passwordHash
        this.locale = locale
        this.status = Status.LOGGED_IN 
        this.role = Role.USER
        this.createdAt = new Date()
        this.verified = false
        this.pendingPasswordRestore = false
        this.temporaryAccessToken = '' // Used for email verification and password recovery
        this.licenseUsageCounter = 0
    }
}

