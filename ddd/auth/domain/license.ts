export class License {
    id: string
    name: string
    code: string
    numUsers: number
    maxUsers: number
    usageCounter: number
    usageLimit: number

    constructor(name: string, email:string, passwordHash: string, locale: string = 'es') {
        this.id = '' //uuid
        this.name = ''
        this.code = ''
        this.numUsers = 0
        this.maxUsers = 1
        this.usageCounter = 0
        this.usageLimit = 1
    }
}

