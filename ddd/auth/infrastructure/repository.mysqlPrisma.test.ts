import { LoggerAdapterWinston } from '../../log/infrastructure/adapter.winston';
import { AuthRepositoryPrismaMySQL } from './repository.mysqlPrisma'
import { config } from '/server/config'
import { expect, describe, it, vi } from 'vitest'
import { prisma } from "/prisma/__mocks__/client"

vi.mock('/prisma/client')

describe('Licenses', () => {

    const loggerPort = new LoggerAdapterWinston(config);
    const authRepository = new AuthRepositoryPrismaMySQL(loggerPort);

    it('Should get all licenses', async () => {
        const mockedLicenses = [{ id: '1234', name: 'Colegio Tetuan', code: 'HEPE-123', numUsers: 0, maxUsers: 1, usageLimit: 1  }]
        prisma.license.findMany.mockResolvedValue(mockedLicenses)
        const licenses = await authRepository.getAllLicenses()
        expect(licenses).toStrictEqual(mockedLicenses)
    })

    it('Should get license by its id', async () => {
        const mockedLicense = { id: '1234', name: 'Colegio Tetuan', code: 'HEPE-123', numUsers: 0, maxUsers: 1, usageLimit: 1  }
        prisma.license.findUnique.mockResolvedValue(mockedLicense)
        const license = await authRepository.getLicenseById(mockedLicense.id)
        expect(license).toStrictEqual(mockedLicense)
        expect(prisma.license.findUnique).toHaveBeenCalledWith({
            where: { id: mockedLicense.id }
        })
    })

    it('Should get license by its code', async () => {
        const mockedLicense = { id: '1234', name: 'Colegio Tetuan', code: 'HEPE-123', numUsers: 0, maxUsers: 1, usageLimit: 1  }
        prisma.license.findUnique.mockResolvedValue(mockedLicense)
        const license = await authRepository.getLicenseByCode(mockedLicense.code)
        expect(license).toStrictEqual(mockedLicense)
        expect(prisma.license.findUnique).toHaveBeenCalledWith({
            where: { code: mockedLicense.code }
        })
    })

    it('Should create a new license', async () => {
        const mockedLicense = { id: '1234', name: 'Colegio Tetuan', code: 'HEPE-123', numUsers: 0, maxUsers: 1, usageLimit: 1  }
        prisma.license.create.mockResolvedValue(mockedLicense)
        const license = await authRepository.createLicense(mockedLicense.name,mockedLicense.code,mockedLicense.maxUsers,mockedLicense.usageLimit)
        expect(license).toStrictEqual(mockedLicense)
        expect(prisma.license.create).toHaveBeenCalledWith({
            data: { name: mockedLicense.name, code: mockedLicense.code, maxUsers: mockedLicense.maxUsers, usageLimit: mockedLicense.usageLimit }
        })
    })

    it('Should update an existing license', async () => {
        const mockedLicense = { id: '1234', name: 'Colegio Tetuan', code: 'HEPE-123', numUsers: 0, maxUsers: 1, usageLimit: 1  }
        prisma.license.update.mockResolvedValue(mockedLicense)
        const license = await authRepository.updateLicense(mockedLicense.id,mockedLicense.name,mockedLicense.maxUsers,mockedLicense.usageLimit)
        expect(license).toStrictEqual(mockedLicense)
        expect(prisma.license.update).toHaveBeenCalledWith({
            where: { id: mockedLicense.id },
            data: { name: mockedLicense.name, maxUsers: mockedLicense.maxUsers, usageLimit: mockedLicense.usageLimit }
        })
    })

})