import { User } from "~/ddd/auth/domain/user";
import { License } from "~/ddd/auth/domain/license";
import { AuthService } from "~/ddd/auth/application/auth.service";

export class AuthController {
    private readonly authService: AuthService;

    constructor(authService: AuthService){
        this.authService = authService;
    }

    public async licenses(): Promise<Array <License> | false> {
        return await this.authService.licenses()
    }

    public async license(id: string): Promise<Array <License> | false> {
        return await this.authService.license(id)
    }

    public async licenseUsageLimit(id: string): Promise<Number | false>{
        return await this.authService.licenseUsageLimit(id)
    }

    public async register( name: string, email: string, password: string, code: string, locale: string ): Promise<User | false> {
        return await this.authService.register(name, email, password, code, locale)
    }

    public async createLicense( name: string, maxUsers: number, usageLimit: number ): Promise<License | false> {
        return await this.authService.createLicense(name, maxUsers, usageLimit)
    }

    public async updateLicense( id: string, name: string, maxUsers: number, usageLimit: number ): Promise<License | false> {
        return await this.authService.updateLicense(id, name, maxUsers, usageLimit)
    }

    public async deleteLicense( id: string): Promise<boolean> {
        return await this.authService.deleteLicense(id)
    }

    public async verify( temporaryAccessToken: string ): Promise<boolean> {
        return await this.authService.verify(temporaryAccessToken)
    }

    public async login( email: string, password: string ): Promise<User | false> {
        return await this.authService.login(email, password)
    }

    public async session(id: string): Promise<User | false> {
        return await this.authService.session(id)
    }

    public async logout(id: string): Promise<boolean> {
        return await this.authService.logout(id)
    }

    public async recover( email: string ): Promise<User | false> {
        return await this.authService.recover(email)
    }

    public async restore( temporaryAccessToken: string, password: string ): Promise<User | false> {
        return await this.authService.restore(temporaryAccessToken, password)
    }
}