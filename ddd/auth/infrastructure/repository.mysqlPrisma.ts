import type { IAuthRepository } from "../domain/ports/auth.repository.interface";
import { Log, LogType } from "../../log/domain/log";
import type { Role, Status, User } from "../domain/user";
import type { License } from "../domain/license";
import { mapDomainToPrismaStatus, mapPrismaToDomainRole, mapPrismaToDomainStatus } from "./prisma.utils";
import prisma, { Prisma } from "~/prisma"; 
// import { prisma } from "../../../prisma/client"; // import prisma from client.ts to be able to mock it in tests
import type { ILoggerPort } from "/ddd/log/application/logger.port.interface";

export class AuthRepositoryPrismaMySQL implements IAuthRepository {

    private loggerPort

    constructor(loggerPort: ILoggerPort) {
        this.loggerPort = loggerPort
    }

    private async findAllRecords(entity: string): Promise<Array<User | License> | false> {
        try {
            const records = await prisma[entity].findMany()
            if(entity === 'user') {
                // TODO: map status and role for each record
                // return {
                //     ...record,
                //     status: mapPrismaToDomainStatus(record.status),
                //     role: mapPrismaToDomainRole(record.role)
                // }
                return records
            } else {
                return records
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
        }
        return false
    }

    private async findRecordByUniqueField(entity: string, key: string, value: any): Promise<User | License | false> {
        try {
            const record = await prisma[entity].findUnique({
                where: {
                    [key]: value
                }
            })
            if(entity === 'user') {
                return {
                    ...record,
                    status: mapPrismaToDomainStatus(record.status),
                    role: mapPrismaToDomainRole(record.role)
                }
            } else {
                return {
                    ...record
                }
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async findRecordByField(entity: string, key: string, value: any): Promise<User | License | false> {
        try {
            const record = await prisma[entity].findFirst({
                where: {
                    [key]: value
                }
            })
            if(entity === 'user') {
                return {
                    ...record,
                    status: mapPrismaToDomainStatus(record.status),
                    role: mapPrismaToDomainRole(record.role)
                }
            } else {
                return {
                    ...record
                }
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async updateRecord(entity: string, where: object, data: object): Promise<User | License | false> {
        try {
            const record = await prisma[entity].update({
                where,
                data
            })
            if(entity === 'user') {
                return {
                    ...record,
                    status: mapPrismaToDomainStatus(record.status),
                    role: mapPrismaToDomainRole(record.role)
                }
            } else {
                return {
                    ...record
                }
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async deleteRecord(entity: string, where: object): Promise<boolean> {
        try {
            const record = await prisma[entity].delete({
                where
            })
            if(record){
                return true
            }
        } catch(e) {
            this.loggerPort.error("Error deleting entity")
            this.loggerPort.error(e)
        }
        return false
    }

    private async deleteRecords(entity: string, where: object): Promise<boolean> {
        try {
            const result = await prisma[entity].deleteMany({
                where
            })
            if(result){
                return true
            }
        } catch(e) {
            this.loggerPort.error("Error deleting many")
            this.loggerPort.error(e)
        }
        return false
    }

    public async getAllLicenses(): Promise<Array<License> | false> {
        return await this.findAllRecords('license')
    }

    public async getLicenseById(id: string): Promise<License | false> {
        return await this.findRecordByUniqueField('license','id',id)
    }

    public async getLicenseByCode(code: string): Promise<License | false> {
        return await this.findRecordByUniqueField('license','code',code)
    }

    public async getUserById(id: string): Promise<User | false> {
        return await this.findRecordByUniqueField('user','id',id)
    }

    public async getUserByEmail(email: string): Promise<User | false> {
        return await this.findRecordByUniqueField('user','email',email)
    }

    public async getUserByTemporaryAccessToken(temporaryAccessToken: string): Promise<User | false> {
        return await this.findRecordByField('user','temporaryAccessToken',temporaryAccessToken)
    }

    public async registerUser(name: string, email: string, passwordHash: string, licenseId: string, locale: string = 'es'): Promise<User | false> {
        try {
            const user = await prisma.user.create({
                data: {
                    name,
                    email,
                    passwordHash,
                    licenseId,
                    locale
                }
            })
            return {
                ...user,
                status: mapPrismaToDomainStatus(user.status),
                role: mapPrismaToDomainRole(user.role)
            }
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async createLicense(name: string, code: string, maxUsers: number, usageLimit: number): Promise<License | false> {
        try {
            return await prisma.license.create({
                data: {
                    name,
                    code,
                    maxUsers,
                    usageLimit
                }
            })
        } catch(e) {
            console.log("Lcense creation error in db")
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle name exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing name.")
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "createLicense" action.')
                }
            }
            
        }
        return false
    }

    public async updateLicense(id: string, name: string, maxUsers: number, usageLimit: number): Promise<License | false> {
        return await this.updateRecord('license', { id }, { name, maxUsers, usageLimit })
    }

    public async deleteLicense(id: string): Promise<boolean> {
        // Delete all users that depen on the license
        await this.deleteRecords('user', {
            license: {
                id
            }
        })
        // Delete license
        return await this.deleteRecord('license', { id })
    }

    public async setLicenseNumUsers(id: string, numUsers: number): Promise<boolean> {
        return await this.updateRecord('license', { id }, { numUsers })
    }

    public async setLicenseUsageCounter(id: string, licenseUsageCounter: number): Promise<boolean> {
        return await this.updateRecord('user', { id }, { licenseUsageCounter })
    }

    public async addLicenseUser(id: string, userId: string): Promise<boolean> {
        // Connect user to license
        const data = {
            users: {
                connect: {
                    id: userId
                }
            }
        }
        return await this.updateRecord('license', { id }, data)
        
    }

    public async setUserStatus(id: string, status: Status): Promise<User | false> {
        return await this.updateRecord('user', { id }, { status: mapDomainToPrismaStatus(status) })
    }

    public async setUserRole(id: string, role: Role): Promise<User | false> {
        // return await this.updateRecord('user', { id }, { role })
    }

    public async setUserPassword(id: string, passwordHash: string): Promise<User | false>{
        return await this.updateRecord('user', { id }, { passwordHash })
    }

    public async setUserVerified(id: string): Promise<User | false>{
        return await this.updateRecord('user', { id }, { verified: true })
    }

    public async setUserPendingPasswordRestore(id: string, pending: boolean): Promise<User | false>{
        return await this.updateRecord('user', { id }, { pendingPasswordRestore: pending })
    }

    public async setUserTemporaryAccessToken(id: string, temporaryAccessToken: string): Promise<User | false>{
        return await this.updateRecord('user', { id }, { temporaryAccessToken })
    }
}