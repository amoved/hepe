// source: https://invidious.protokolla.fi/watch?v=Pl7pDjWd830
// This code uses Mapped Types and Generic constrains
type Listener<T extends Array<any>> = (...args: T) => void

export class DomainEventEmitter<EventMap extends Record<string, Array<any>>> {
    private eventListeners: { [K in keyof EventMap]?: Set<Listener<EventMap[k]>> } = {}

    on<K extends keyof EventMap>(eventName: K, listener: Listener<EventMap[K]>) {
        const listeners = this.eventListeners[eventName] ?? new Set()
        listeners.add(listener)
        this.eventListeners[eventName] = listeners
    }

    emit<K extends keyof EventMap>(eventName: K, ...args: EventMap[K]){
        const listeners = this.eventListeners[eventName] ?? new Set()
        for (const listener of listeners) {
            listener(...args)
        }
    }
}