export interface ICryptoPort {
    hashPassword(password: string): Promise<string>
    testPassword(password: string, passwordHash: string): Promise<boolean>
    generateAccessToken(payload: object, expiresIn?: string): string
    verifyAccessToken(token: string): object
}