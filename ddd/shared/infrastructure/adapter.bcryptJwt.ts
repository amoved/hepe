import type { ICryptoPort } from "../application/ports/crypto.port.interface";
import bcrypt from 'bcryptjs' 
import jwt from "jsonwebtoken";

export class AuthAdapterBcryptJWT implements ICryptoPort {
    public async hashPassword(password: string): Promise<string> {
        const salt = await bcrypt.genSalt(10)
        return await bcrypt.hash(password,salt)
    }

    public async testPassword(password: string, passwordHash: string): Promise<boolean> {        
        return await bcrypt.compare(password, passwordHash)
    }

    public generateAccessToken(payload: object, expiresIn?: string = '8h'): string {
        return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn });
    }

    public verifyAccessToken(authorization: string): object { 
        const token = authorization.includes("Bearer") ? authorization.split(" ")[1] : authorization // Remove the initial 'Bearer' prefix
        const data = jwt.verify(token, process.env.JWT_SECRET)
        return data
    }
}