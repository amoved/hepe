import type { IConfigRepository } from '../domain/ports/config.repository.interface';
import type { ILoggerPort } from '~/ddd/common/application/ports/logger.port.interface';
import prisma from "~/prisma";
import type { ConfigObject } from '../domain/config';

export class ConfigRepositoryPrismaMySQL implements IConfigRepository {

    private loggerPort

    constructor(loggerPort: ILoggerPort) {
        this.loggerPort = loggerPort
    }

    public async getAll(): Promise<ConfigObject> {
        let config: ConfigObject = {}
        try {
            const storedConfig = await prisma.config.findMany()
            storedConfig.forEach(row => {
                if(row.value === 'true'){
                    config[row.name] = true
                } else if(row.value === 'false') {
                    config[row.name] = false
                }
                config[row.name] = row.value
            })
        } catch(e) {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                this.loggerPort.error(e.message)
            } else {
                this.loggerPort.error('Unhandled exception error at "config.repository.get" action.')
            }
        }
        return config
    }

    public async getByName(name: string): Promise<string | false> {
        try {
            const setting = await prisma.config.findUnique({
                where: {
                    name
                }
            })
            return setting?.value || false
        } catch(e) {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                this.loggerPort.error(e.message)
            } else {
                this.loggerPort.error('Unhandled exception error at "config.repository.get" action.')
            }
        }
        return false
    }

    public async create(name: string, value: string | boolean): Promise<boolean> {
        if(typeof value === 'boolean'){
            value = value ? 'true' : 'false' 
        }

        try {
            const config = await prisma.config.create({
                data: {
                    name,
                    value
                }
            })
            return true
        } catch(e) {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                this.loggerPort.error(e.message)
            } else {
                this.loggerPort.error('Unhandled exception error at "config.repository.create" action.')
            }
        }
        return false
    }

    public async update(name: string, value: string | boolean): Promise<boolean> {
        if(typeof value === 'boolean'){
            value = value ? 'true' : 'false' 
        }
        
        try {
            await prisma.config.update({
                where: {
                    name,
                },
                data: {
                    value
                }
            })
            return true
        } catch(e) {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                this.loggerPort.error(e.message)
            } else {
                this.loggerPort.error('Unhandled exception error at "config.repository.update" action.')
            }
        }
        return false
    }

}