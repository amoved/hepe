import type { IMailerPort } from './../../mailer/domain/ports/mailer.interface';
import { Config, type SMTPConfig, type ConfigObject } from './../domain/config';
import { type IConfigRepository } from "../domain/ports/config.repository.interface";
import type { ICryptoPort } from "~/ddd/shared/application/ports/crypto.port.interface";
import type { ILoggerPort } from '~/ddd/log/application/ports/logger.port.interface';
import { configDomainEventEmitter } from '../domain/events';


export class ConfigService {
    private loggerPort: ILoggerPort
    private configRepository: IConfigRepository
    private cryptoPort: ICryptoPort
    private mailerPort: IMailerPort
    
    private config: Config
    private configUniqueKeyNames: Record<string,string> = {
        'smtp-host': 'smtp-host',
        'smtp-port': 'smtp-port',
        'smtp-use-tls': 'smtp-use-tls',
        'smtp-email': 'smtp-email',
        'smtp-password': 'smtp-password',
    }
    
    
    constructor(config: ConfigObject, configRepository: IConfigRepository, cryptoPort: ICryptoPort, loggerPort: ILoggerPort, mailerPort: IMailerPort){
        this.loggerPort = loggerPort
        this.configRepository = configRepository
        this.cryptoPort = cryptoPort
        this.mailerPort = mailerPort
        this.config = new Config(config)
    }

    public async init(){
        // Check if config exists on DB
        const storedConfig = await this.configRepository.getAll()

        // If not
        if(Object.keys(storedConfig).length === 0){
            // Store config in DB
            const config = this.config.get()
            await this.configRepository.create(this.configUniqueKeyNames['smtp-host'], config.smtp.host)
            await this.configRepository.create(this.configUniqueKeyNames['smtp-port'], config.smtp.port)
            await this.configRepository.create(this.configUniqueKeyNames['smtp-use-tls'], config.smtp.useTLS)
            await this.configRepository.create(this.configUniqueKeyNames['smtp-email'], config.smtp.email)
            await this.configRepository.create(this.configUniqueKeyNames['smtp-password'], config.smtp.password)
        }
    }
    
    public get(){
        return this.config.get()
    }

    public getSmtp(){
        return this.config.getSmtp()
    }

    public async create(name: string, value: string | boolean): Promise<boolean>{
        return await this.configRepository.create(name, value)     
    }

    private async update(name: string, value: string | boolean): Promise<boolean>{
        return await this.configRepository.update(name, value)     
    }

    public async updateSmtp(config: SMTPConfig): Promise<SMTPConfig | false>{
        
        // Check if candidate config its different from current 
        const currentConfig = this.config.getSmtp()
        if(JSON.stringify(config) === JSON.stringify(currentConfig)){
            console.log("The new config its the same")
            return false
        }
        // Update config
        try {
            await this.update(this.configUniqueKeyNames['smtp-host'], config.host)
            await this.update(this.configUniqueKeyNames['smtp-port'], config.port)
            await this.update(this.configUniqueKeyNames['smtp-use-tls'], config.useTLS)
            await this.update(this.configUniqueKeyNames['smtp-email'], config.email)
            await this.update(this.configUniqueKeyNames['smtp-password'], config.password)
            this.config.setSmtp(config)
            configDomainEventEmitter.emit('SMTP_UPDATED',config)
        } catch(e) {
            console.log(e)
        }
        
        return this.config.getSmtp() || false
    }

    public async verifySmtpConnection(config: SMTPConfig): Promise<Boolean>{
        return await this.mailerPort.verifySmtpConnection(config)
    }

}
