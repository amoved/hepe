export interface SMTPConfig {
    [index: string]: string | boolean

    host: string
    port: string
    useTLS: boolean
    email: string
    password: string
}

export interface ConfigObject {
    [index: string]: string | boolean | object

    smtp?: SMTPConfig
}


export class Config {

    private config: ConfigObject

    constructor(config: ConfigObject) {
        // Clone the object
        this.config = JSON.parse(JSON.stringify(config))
    }

    public get(){
        return this.config
    }

    // TODO: handle "restart" scenario in which config is in the DB
    public getSmtp(){
        return JSON.parse(JSON.stringify(this.config.smtp))
    }

    public setSmtp(config: SMTPConfig){
        return this.config.smtp = JSON.parse(JSON.stringify(config))
    }
}

