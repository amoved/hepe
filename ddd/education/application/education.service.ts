import { Region } from '../domain/region'
import { Locale } from '../domain/locale'
import { Course } from '../domain/course'
import { type IEducationRepository } from "../domain/ports/education.repository.interface";
import { type IEducationFileGenerator } from "../domain/ports/education.fileGenerator.interface";
import type { ILoggerPort } from '~/ddd/log/application/logger.port.interface';
import type { SpecificSkill } from '../domain/specificSkill';
import type { EvaluationCriteria } from '../domain/evaluationCriteria';
import type { BasicWisdom } from '../domain/basicWisdom';
import type { EcosocialLearning } from '@prisma/client';
import type { EcosocialEvaluationCriteria } from '../domain/ecosocialEvaluationCriteria';
import type { AcademicCycle } from '../domain/academicCycle';
import type { Program } from '../domain/program';
import type { LearningUnit } from '../domain/learningUnit';
import { educationDomainEventEmitter } from '../domain/events';

export class EducationService {
    private loggerPort: ILoggerPort
    private educationRepository: IEducationRepository
    private educationFileGenerator: IEducationFileGenerator

    constructor(educationRepository: IEducationRepository, educationFileGenerator: IEducationFileGenerator, loggerPort: ILoggerPort){
        this.loggerPort = loggerPort
        this.educationRepository = educationRepository
        this.educationFileGenerator = educationFileGenerator
    }

    public async getProgram(userId: string): Promise<Program | false> {
        return await this.educationRepository.getProgram(userId)
    }

    public async updateProgram(id: number, userId: string, programObj: object): Promise<Program | false>{
        this.loggerPort.debug("[Education: updateProgram] Saving program...")
        return await this.educationRepository.setProgram(id, userId, programObj)
    }

    public async regions(): Promise<Array<Region> | false>{
        this.loggerPort.debug("[Education: regions] Get all regions")
        return await this.educationRepository.getRegions()
    }

    public async localesFromRegion(regionName: string): Promise<Array<Locale> | false>{
        this.loggerPort.debug("[Education: locales] Get locales from a region")
        return await this.educationRepository.getLocalesFromRegion(regionName)
    }

    public async courseFromId(id: number): Promise<Course | false>{
        this.loggerPort.debug("[Education: course] Get course from id")
        return await this.educationRepository.getCourseFromId(id)
    }

    public async coursesFromRegionAndLocale(regionName: string, localeName: string): Promise<Array<Course> | false>{
        this.loggerPort.debug("[Education: courses] Get courses from a region and locale")
        return await this.educationRepository.getCoursesFromRegionAndLocale(regionName, localeName)
    }

    public async specificSkillsFromCourse(courseId: number): Promise<Array<SpecificSkill> | false>{
        this.loggerPort.debug("[Education: specificSkills] Get specific skills from course")
        return await this.educationRepository.getSpecificSkillsFromCourse(courseId)
    }

    public async evaluationCriteriaFromSpecificSkill(specificSkillId: number): Promise<Array<EvaluationCriteria> | false>{
        this.loggerPort.debug("[Education: evaluationCriteria] Get evaluation criteria from specific skill")
        return await this.educationRepository.getEvaluationCriteriaFromSpecificSkill(specificSkillId)
    }

    public async basicWisdomsFromCourse(courseId: number): Promise<Array<BasicWisdom> | false>{
        this.loggerPort.debug("[Education: basicWisdoms] Get basic wisdoms from course")
        return await this.educationRepository.getBasicWisdomsFromCourse(courseId)
    }

    public async ecosocialLearnings(localeName: string, academicYearName: string, ecosocialLearningIds: Array<number> = []): Promise<Array<EcosocialLearning> | false>{
        this.loggerPort.debug("[Education: ecosocialLearnings] Get ecosocial learnings")
        return await this.educationRepository.getEcosocialLearnings(localeName, academicYearName, ecosocialLearningIds)
    }

    public async ecosocialEvaluationCriteriaFromEcosocialLearnings(ecosocialLearningIds: Array<EcosocialEvaluationCriteria>,academicCycle: AcademicCycle): Promise<Array<EcosocialEvaluationCriteria> | false>{
        this.loggerPort.debug("[Education: ecosocialEvaluationCriteriaFromEcosocialLearnings] Get ecosocial evaluation criteria")
        return await this.educationRepository.getEcosocialEvaluationCriteriaFromEcosocialLearnings(ecosocialLearningIds,academicCycle)
    }

    public async generateProgramDoc(title: string, learningUnits: Array<LearningUnit>, texts: object): Promise<Uint8Array | false> {
        this.loggerPort.debug("[Education: generateProgramDoc] Generating document...")
        return await this.educationFileGenerator.generateEcosocialProgram(title, learningUnits, texts)
    }

    public async completeProgram(id: number, userId: string): Promise<boolean> {
        this.loggerPort.debug("[Education: completeProgram] Deleting program...")

        const deleted = await this.educationRepository.deleteProgram(id)
        // Log results
        if(!!deleted){
            this.loggerPort.info(`[Education: completeProgram] Program ${id} successfuly deleted`)
            educationDomainEventEmitter.emit('PROGRAM_COMPLETED', userId)
            this.loggerPort.debug("[PROGRAM_COMPLETED] Domain event emitted!!")
        } else {
            this.loggerPort.error("[Education: completeProgram] Program complete failed")
        }

        return deleted
    }
    
}