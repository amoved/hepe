export class Locale {
    id: number
    name: string
    abbreviation: string

    constructor(name: string, abbreviation: string) {
        this.id = -1
        this.name = name
        this.abbreviation = abbreviation
    }
}

