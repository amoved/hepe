export class EvaluationCriteria {
    id: number
    content: string
    specificSkillId: number

    constructor(content: string, specificSkillId: number = -1) {
        this.id = -1
        this.content = content
        this.specificSkillId = specificSkillId
    }
}

