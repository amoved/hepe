import type { EvaluationCriteria } from "./evaluationCriteria"

export class SpecificSkill {
    id: number
    content: string
    evalutaionCriterias: Array<EvaluationCriteria>
    courseId: number

    constructor(content: string, evalutaionCriterias: Array<EvaluationCriteria>, courseId: number) {
        this.id = -1
        this.content = content
        this.evalutaionCriterias = evalutaionCriterias
        this.courseId = courseId
    }
}

