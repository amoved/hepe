import type { SpecificSkill } from "./specificSkill"
import type { EcosocialLearning } from "./ecosocialLearning"

export class LearningUnit {
    name: string
    sSs: Array<SpecificSkill>
    eLs: Array<EcosocialLearning>

    constructor(name: string, sSs: Array<SpecificSkill>, eLs: Array<EcosocialLearning>) {
        this.name = ''
        this.sSs = sSs
        this.eLs = eLs
    }
}

