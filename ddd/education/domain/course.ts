import { AcademicCycle } from "./academicCycle"

export class Course {
    id: number
    name: string
    academicYear: number
    academicCycle: AcademicCycle
    regionId: number
    localeId: number

    constructor(name: string, academicYear: number = 10, academicCycle: AcademicCycle = AcademicCycle.SECUNDARIA, regionId: number = -1, localeId: number = -1) {
        this.id = -1
        this.name = name
        this.academicYear = academicYear
        this.academicCycle = academicCycle
        this.regionId = regionId
        this.localeId = localeId
    }
}

