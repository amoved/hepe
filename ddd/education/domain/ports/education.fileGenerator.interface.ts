import type { LearningUnit } from "../learningUnit"

export interface IEducationFileGenerator {
    generateEcosocialProgram(title: string, learningUnits: Array<LearningUnit>, texts: object): Promise< Blob | false>
}