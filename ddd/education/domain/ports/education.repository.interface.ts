import type { Region } from '../region'
import type { Locale } from '../locale'
import type { Course } from '../course'
import type { SpecificSkill } from '../specificSkill';
import type { EvaluationCriteria } from '../evaluationCriteria';
import type { BasicWisdom } from '../basicWisdom';
import type { EcosocialLearning } from '../ecosocialLearning';
import type { EcosocialEvaluationCriteria } from '../ecosocialEvaluationCriteria';
import type { AcademicCycle } from '../academicCycle';
import type { Program } from '../program';

export interface IEducationRepository {
    getProgram(userId: string): Promise<Program | false>
    setProgram(id: number, userId: string, programObj: object): Promise<Program | false>
    createLocale(name: string, abbreviation:string): Promise<Locale | false>;
    getLocales(): Promise<Array<Locale> | false>;
    createRegion(name: string, loales: Array<Locale>): Promise<Region | false>;
    getRegions(): Promise<Array<Region> | false>;
    createCourse(name: string, academicYear: number, regionId: number): Promise<Course | false>;
    getCoursesFromRegionAndLocale(regionName: string, localeName: string): Promise<Array<Course> | false> 
    getSpecificSkillsFromCourse(courseId: number): Promise<Array<SpecificSkill> | false> 
    getEvaluationCriteriaFromSpecificSkill(specificSkillId: number): Promise<Array<EvaluationCriteria> | false> 
    getBasicWisdomsFromCourse(courseId: number): Promise<Array<BasicWisdom> | false> 
    getEcosocialLearnings(localeName: string, academicYearName: string, ecosocialLearningIds: Array<number>): Promise<Array<EcosocialLearning> | false> 
    getEcosocialEvaluationCriteriaFromEcosocialLearnings(ecosocialLearningIds: Array<EcosocialEvaluationCriteria>,academicCycle: AcademicCycle) : Promise<Array<EcosocialEvaluationCriteria> | false> 
    deleteProgram(id: string) : Promise<boolean>
}