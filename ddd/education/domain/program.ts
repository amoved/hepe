export class Program {
    id: number
    userId: string
    stringifiedProgram: string

    constructor(userId: string, stringifiedProgram: string) {
        this.id = -1
        this.userId = userId
        this.stringifiedProgram = stringifiedProgram
    }
}

