import type { Locale } from "./locale"

export class Region {
    id: number
    name: string
    locales: Array<Locale>

    constructor(name: string, locales: Array<Locale> = []) {
        this.id = -1
        this.name = name
        this.locales = locales
    }
}

