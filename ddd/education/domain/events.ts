import { DomainEventEmitter } from '../../shared/domain/DomainEventEmitter';
import type { Course } from './course';
import type { Region } from "./region"

// Define all events names and input params
export type EventMap = {
    'REGION_CREATED': [region: Region],
    'COURSE_CREATED': [couser: Course],
    'PROGRAM_COMPLETED': [id: number],
}

export const educationDomainEventEmitter = new DomainEventEmitter<EventMap>()