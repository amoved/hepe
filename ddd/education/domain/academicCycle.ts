export enum AcademicCycle {
    INFANTIL,
    PRIMARIA,
    SECUNDARIA
}