export class BasicWisdom {
    id: number
    content: string
    children: BasicWisdom[]
    courseId: number

    constructor(content: string, children: BasicWisdom[] = [], courseId: number = '') {
        this.id = -1
        this.content = content
        this.children = children
        this.courseId = courseId
    }
}

