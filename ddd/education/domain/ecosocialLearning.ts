import type { EcosocialBasicWisdom } from "./ecosocialBasicWisdom"
import type { EcosocialEvaluationCriteria } from "./ecosocialEvaluationCriteria"
import type { EcosocialLearningGroup } from "./ecosocialLearningGroup"

export class EcosocialLearning {
    id: number
    group: EcosocialLearningGroup
    content: string
    ecosocialEvaluationCriterias: Array<EcosocialEvaluationCriteria>
    ecosocialBasicWisdoms: Array<EcosocialBasicWisdom>

    constructor(group: EcosocialLearningGroup, content: string, ecosocialEvaluationCriterias: Array<EcosocialEvaluationCriteria>, ecosocialBasicWisdoms: Array<EcosocialBasicWisdom>) {
        this.id = -1
        this.group = group
        this.content = content
        this.ecosocialEvaluationCriterias = ecosocialEvaluationCriterias
        this.ecosocialBasicWisdoms = ecosocialBasicWisdoms
    }
}

