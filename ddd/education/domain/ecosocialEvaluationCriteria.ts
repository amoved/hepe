import type { AcademicCycle } from "./academicCycle"

export class EcosocialEvaluationCriteria {
    id: number
    content: string
    academicCycle: AcademicCycle
    ecosocialLearningId: number

    constructor(content: string, academicCycle: AcademicCycle, ecosocialLearningId: number = -1) {
        this.id = -1
        this.content = content
        this.academicCycle = academicCycle
        this.ecosocialLearningId = ecosocialLearningId
    }
}
