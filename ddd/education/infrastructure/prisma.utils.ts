import { EcosocialLearningGroup as PrismaEcosocialLearningGroup } from "@prisma/client"
import { EcosocialLearningGroup } from "../domain/ecosocialLearningGroup"

export function mapDomainToPrismaEcosocialLearningGroup(ecosocialLearningGroup: EcosocialLearningGroup) {
    switch(ecosocialLearningGroup){
        case EcosocialLearningGroup.ECODEPENDENCIA:
            return PrismaEcosocialLearningGroup.ECODEPENDENCIA 
        case EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA:
            return PrismaEcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA 
        case EcosocialLearningGroup.CRISIS_CIVILIZATORIA:
            return PrismaEcosocialLearningGroup.CRISIS_CIVILIZATORIA 
        case EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL:
            return PrismaEcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL 
        case EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO:
            return PrismaEcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO 
        case EcosocialLearningGroup.JUSTICIA:
            return PrismaEcosocialLearningGroup.JUSTICIA 
        case EcosocialLearningGroup.DEMOCRACIA:
            return PrismaEcosocialLearningGroup.DEMOCRACIA
        case EcosocialLearningGroup.TECNICAS_ECOSOCIALES:
            return PrismaEcosocialLearningGroup.TECNICAS_ECOSOCIALES
    }
} 

export function mapPrismaToDomainEcosocialLearningGroup(ecosocialLearningGroup: PrismaEcosocialLearningGroup) {
    switch(ecosocialLearningGroup){
        case PrismaEcosocialLearningGroup.ECODEPENDENCIA:
            return EcosocialLearningGroup.ECODEPENDENCIA 
        case PrismaEcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA:
            return EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA 
        case PrismaEcosocialLearningGroup.CRISIS_CIVILIZATORIA:
            return EcosocialLearningGroup.CRISIS_CIVILIZATORIA 
        case PrismaEcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL:
            return EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL 
        case PrismaEcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO:
            return EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO 
        case PrismaEcosocialLearningGroup.JUSTICIA:
            return EcosocialLearningGroup.JUSTICIA 
        case PrismaEcosocialLearningGroup.DEMOCRACIA:
            return EcosocialLearningGroup.DEMOCRACIA
        case PrismaEcosocialLearningGroup.TECNICAS_ECOSOCIALES:
            return EcosocialLearningGroup.TECNICAS_ECOSOCIALES
    }
} 

export function mapDomainEcosocialLearningGroupToString(ecosocialLearningGroup: EcosocialLearningGroup): string {
    switch(ecosocialLearningGroup) {
        case EcosocialLearningGroup.ECODEPENDENCIA:
            return 'ecosocialLearningGroupEcodependencia'
        case EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA:
            return 'ecosocialLearningGroupFuncionamientoDeLaBiosfera'
        case EcosocialLearningGroup.CRISIS_CIVILIZATORIA:
            return 'ecosocialLearningGroupCrisisCivilizatoria'
        case EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL:
            return 'ecosocialLearningGroupAgentesDeCambioSocial'
        case EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO:
            return 'ecosocialLearningGroupDesarrolloPersonalYComunitario'
        case EcosocialLearningGroup.JUSTICIA:
            return 'ecosocialLearningGroupJusticia'
        case EcosocialLearningGroup.DEMOCRACIA:
            return 'ecosocialLearningGroupDemocracia'
        case EcosocialLearningGroup.TECNICAS_ECOSOCIALES:
            return 'ecosocialLearningGroupTecnicasEcosociales'
    }
}

