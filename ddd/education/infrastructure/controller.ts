import { SpecificSkill } from './../domain/specificSkill';
import { EducationService } from "../application/education.service";
import { Region } from "../domain/region"
import { Locale } from "../domain/locale";
import type { BasicWisdom } from '../domain/basicWisdom';
import type { EcosocialLearning } from '@prisma/client';
import type { EcosocialEvaluationCriteria } from '../domain/ecosocialEvaluationCriteria';
import type { AcademicCycle } from '../domain/academicCycle';
import type { Program } from '../domain/program';
import type { Course } from '../domain/course';
import type { LearningUnit } from '../domain/learningUnit';

export class EducationController {
    private readonly educationService: EducationService;

    constructor(educationService: EducationService){
        this.educationService = educationService;
    }

    public async getProgram(userId: string): Promise<Program | false> {
        return await this.educationService.getProgram(userId)
    }

    public async updateProgram(id: number, userId: string, programObj: object): Promise<Program | false> {
        return await this.educationService.updateProgram(id, userId, programObj)
    }

    public async regions(): Promise<Array <Region> | false> {
        return await this.educationService.regions()
    }

    public async locales(regionName: string): Promise<Array <Locale> | false> {
        return await this.educationService.localesFromRegion(regionName)
    }

    public async course(id: number): Promise<Course | false> {
        return await this.educationService.courseFromId(id)
    }

    public async courses(regionName: string, localeName: string): Promise<Array <Course> | false> {
        return await this.educationService.coursesFromRegionAndLocale(regionName, localeName)
    }

    public async specificSkills(courseId: number): Promise<Array <SpecificSkill> | false> {
        return await this.educationService.specificSkillsFromCourse(courseId)
    }

    public async evaluationCriteria(specificSkillId: number): Promise<Array <SpecificSkill> | false> {
        return await this.educationService.evaluationCriteriaFromSpecificSkill(specificSkillId)
    }

    public async basicWisdoms(courseId: number): Promise<Array <BasicWisdom> | false> {
        return await this.educationService.basicWisdomsFromCourse(courseId)
    }

    public async ecosocialLearnings(locale: string, academicYear: string, ecosocialLearningIds: Array<number> = []): Promise<Array <EcosocialLearning> | false> {
        return await this.educationService.ecosocialLearnings(locale, academicYear, ecosocialLearningIds)
    }

    public async ecosocialEvaluationCriteria(ecosocialLearningIds: Array<EcosocialEvaluationCriteria>,academicCycle: AcademicCycle): Promise<Array <EcosocialEvaluationCriteria> | false> {
        return await this.educationService.ecosocialEvaluationCriteriaFromEcosocialLearnings(ecosocialLearningIds,academicCycle)
    }

    public async generateProgramDoc(title: string, learningUnits: Array<LearningUnit>, texts: object): Promise<Uint8Array | false> {
        return await this.educationService.generateProgramDoc(title, learningUnits, texts)
    }

    public async completeProgram(id: number, userId: string): Promise<boolean> {
        return await this.educationService.completeProgram(id, userId)
    }
    
}