import { EcosocialEvaluationCriteria } from './../domain/ecosocialEvaluationCriteria';
import { EcosocialBasicWisdom } from './../domain/ecosocialBasicWisdom';
import type { IEducationRepository } from "../domain/ports/education.repository.interface";
import type { Region } from "../domain/region";
import type { Locale } from "../domain/locale";
import type { Course } from "../domain/course";
import prisma, { Prisma, type BasicWisdomItem } from "~/prisma"; 
// import { prisma } from "../../../prisma/client"; // import prisma from client.ts to be able to mock it in tests
import type { ILoggerPort } from "/ddd/log/application/logger.port.interface";
import type { SpecificSkill } from "../domain/specificSkill";
import type { BasicWisdom } from "../domain/basicWisdom";
import type { EcosocialLearning } from "../domain/ecosocialLearning";
import type { AcademicCycle } from '../domain/academicCycle';
import type { EvaluationCriteria } from '../domain/evaluationCriteria';
import type { Program } from '../domain/program';

export class EducationRepositoryPrismaMySQL implements IEducationRepository {

    private loggerPort

    constructor(loggerPort: ILoggerPort) {
        this.loggerPort = loggerPort
    }

    private unflattenBasicWisdoms(basicWisdomItems: Array<BasicWisdomItem>, parentId: number): Array<BasicWisdom> {
        const filteredBasicWisdomsItems = new Array(...basicWisdomItems).filter(b => b.parentId === parentId)
        const basicWisdoms: Array<BasicWisdom> = []
        filteredBasicWisdomsItems.forEach(b => (basicWisdoms.push({
            id: b.id,
            content: b.content,
            courseId: b.courseId,
            children: this.unflattenBasicWisdoms(basicWisdomItems,b.id)
        })))

        return basicWisdoms 
    }

    private async createRecord(entity: string, data: object): Promise<Program | false> {
        try {
            const record = await prisma[entity].create({ data })
            return record
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async deleteRecord(entity: string, where: object): Promise<boolean> {
        try {
            const record = await prisma[entity].delete({ where })
            return record
        } catch(e) {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                this.loggerPort.error(e.message)
            } else {
                this.loggerPort.error('Unhandled exception error.')
            }
            
        }
        return false
    }

    private async updateRecord(entity: string, where: object, data: object): Promise<Program | false> {
        try {
            const record = await prisma[entity].update({ where, data })
            return record
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async findAllRecords(entity: string, include: object): Promise<Array<Course | Region | Locale> | false> {
        try {
            if(include) {
                const records = await prisma[entity].findMany({include})
                return records
            } else {
                const records = await prisma[entity].findMany()
                return records
            }
            
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
        }
        return false
    }

    private async findRecords(entity: string, where: object, include: object = {}): Promise<Array<Course | Region | Locale | BasicWisdomItem | EvaluationCriteria | EcosocialLearning> | false> {
        try {
            const records = await prisma[entity].findMany({ where, include })
            return records
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    private async findRecord(entity: string, where: object, include: object = {}): Promise<Array<Course | Region | Locale | BasicWisdomItem | EvaluationCriteria | EcosocialLearning> | false> {
        try {
            const record = await prisma[entity].findUnique({ where, include })
            return record
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    this.loggerPort.error("Tried to register with an already existing email.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    this.loggerPort.error(e.message)
                } else {
                    this.loggerPort.error('Unhandled exception error at "register" action.')
                }
            }
            
        }
        return false
    }

    public async getProgram(userId: string): Promise<false | Program> {
        return await this.findRecords('program', { userId })
    }

    public async setProgram(id: number, userId: string, programObj: object): Promise<false | Program> {
        if(id === -1) {
            return await this.createRecord('program', { userId, programObj })
        } else {
            // this.loggerPort.debug(`[Education: setProgram] Current program length: ${programObj.length}`)
            return await this.updateRecord('program', { id }, { userId, programObj })
        }
        
    }

    public async getLocales(): Promise<Array<Locale> | false> {
        return await this.findAllRecords('locale')
    }

    public async getLocalesFromRegion(regionName: string): Promise<Array<Locale> | false> {
        const locales = await this.findRecords('locale',{
            regions: {
                some: {
                    name: regionName
                }
            }
        })
        return locales
    }

    public async getRegions(): Promise<Array<Region> | false> {
        return await this.findAllRecords('region')
    }

    public async getCourseFromId(id: number): Promise<Course | false> {
        const course = await this.findRecord('course',{
            id
        },{
            region: true,
            locale: true
        })
        return course
    }

    public async getCoursesFromRegionAndLocale(regionName: string, localeName: string): Promise<Array<Course> | false> {
        const courses = await this.findRecords('course',{
            region: {
                name: regionName
            },
            locale: {
                name: localeName    
            }
        },
        { 
            academicYear: true 
        }
        )
        return courses
    }

    public async getSpecificSkillsFromCourse(courseId: number): Promise<Array<SpecificSkill> | false> {
        const specificSkills = await this.findRecords('specificSkill',{
                course: {
                    id: courseId
                }
            },
            {
               evaluationCriterias: true 
            }
        )
        return specificSkills
    }

    public async getEvaluationCriteriaFromSpecificSkill(specificSkillId: number): Promise<Array<SpecificSkill> | false> {
        const evaluationCriteria = await this.findRecords('evaluationCriteria',{
            specificSkill: {
                id: specificSkillId
            }
        })
        return evaluationCriteria
    }

    public async getBasicWisdomsFromCourse(courseId: number): Promise<Array<BasicWisdom> | false> {
        const basicWisdomItems = await this.findRecords('basicWisdomItem',{
            course: {
                id: courseId
            }
        })

        const basicWisdoms = this.unflattenBasicWisdoms(basicWisdomItems,-1)
        return basicWisdoms
    }

    public async getEcosocialLearnings(localeName: string, academicYearName: string, ecosocialLearningIds: Array<number> = []): Promise<Array<EcosocialLearning> | false> {
        let where = {
            locale: {
                is: {
                    name: localeName
                }
            }
        }

        if(ecosocialLearningIds.length > 0)
            where.id = { in: ecosocialLearningIds }

        const ecosocialLearnings = await this.findRecords('ecosocialLearning',
            where,
            {
                ecosocialBasicWisdoms: {
                    include: {
                        academicYears: true
                    }
                },
                ecosocialEvaluationCriterias: {
                    include: {
                        academicYears: true
                    }
                },
            }
        )
        if(ecosocialLearnings){
            ecosocialLearnings.forEach((eL: EcosocialLearning) => {
                eL.ecosocialBasicWisdoms = eL.ecosocialBasicWisdoms.filter((eBW: EcosocialBasicWisdom) => {
                    let belongsToThisAcademicYear = false
                    eBW.academicYears.forEach(a => {
                        if(a.name == academicYearName){
                            belongsToThisAcademicYear = true
                        } 
                    })
                    return belongsToThisAcademicYear

                })
                eL.ecosocialEvaluationCriterias = eL.ecosocialEvaluationCriterias.filter((eEvCr: EcosocialEvaluationCriteria) => {
                    let belongsToThisAcademicYear = false
                    eEvCr.academicYears.forEach(a => {
                        if(a.name == academicYearName){
                            belongsToThisAcademicYear = true
                        } 
                    })
                    return belongsToThisAcademicYear
                })
            })
        }
        return ecosocialLearnings
    }

    public async getEcosocialEvaluationCriteriaFromEcosocialLearnings(ecosocialLearningIds: Array<EcosocialEvaluationCriteria>,academicCycle: AcademicCycle) : Promise<Array<EcosocialEvaluationCriteria> | false> {
        const ecosocialEvaluationCriteria = await this.findRecords('ecosocialEvaluationCriteria',
            {
                ecosocialLearningId: { in: ecosocialLearningIds },
                academicCycle
            }
        )
        return ecosocialEvaluationCriteria
    }

    public async deleteProgram(id: number) : Promise<boolean> {
        const deleted = await this.deleteRecord('program',
            {
                id
            }
        )
        return deleted
    }
    
}