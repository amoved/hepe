import type { IEducationFileGenerator } from "../domain/ports/education.fileGenerator.interface"

import * as fs from "fs"
import path from 'path';

import { 
    Header, 
    Document, 
    Paragraph, 
    ImageRun, 
    Table,
    TableRow,
    TableCell,
    HeadingLevel, 
    WidthType,
    Packer,
    VerticalAlign, 
    AlignmentType,
    convertMillimetersToTwip,
    PageOrientation,
    TextRun,
    BorderStyle,
} from "docx"

interface DocItem {
    name?: string
    content?: string
    evCrs?: Array<DocItem>
    eEvCrs?: Array<DocItem>
    bWs?: Array<DocItem>
    eBWs?: Array<DocItem>
}

interface DocLearningUnit {
    name: string
    sSs: Array<DocItem>
    eLs: Array<DocItem>
    bWs: Array<DocItem>
}

export class EducationFileGeneratorDocx implements IEducationFileGenerator {

    private checkedText = 'x'

    private font = 'Arial'

    private fontSizeXs = '10pt'
    private fontSizeSm = '12pt'
    private fontSizeMd = '16pt'
    private fontSizeLg = '18pt'
    private fontSizeXl = '22pt'
    private fontSizeXxl = '26pt'

    private colorsTextPrimary = "000000"
    private colorsTextSecondary = "323232"
    private colorsTextHighlight = "666666"
    private colorsCellHighlightLight = "fdeeb5"
    private colorsCellBasic = "FFFFFF"
    private colorsCellBasicShaded = "F2F2F2"
    private colorsCellHighlight = "34ADD8"
    private colorsCellHighlightDark = "2084A6"

    private spacingRootSize = 128
    private spacingXxs = this.spacingRootSize * .25    
    private spacingXs = this.spacingRootSize * .5
    private spacingSm = this.spacingRootSize * .75
    private spacingMd = this.spacingRootSize * 1
    private spacingLg = this.spacingRootSize * 1.5
    private spacingXl = this.spacingRootSize * 2
    private spacingXxl = this.spacingRootSize * 4

    // basic cell style
    private bCS = {
        shading: {
            fill: this.colorsCellBasic,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }

    // basic cell style, left aligned
    private bCSL = {
        shading: {
            fill: this.colorsCellBasic,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }

    // basic shaded cell style
    private bSCS = {
        shading: {
            fill: this.colorsCellBasicShaded,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }

    // header cell style
    private hLUCS = {
        shading: {
            fill: this.colorsCellHighlight,
            // type: ShadingType.REVERSE_DIAGONAL_STRIPE,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }
    // header cell style
    private hCS = {
        shading: {
            fill: this.colorsCellHighlightDark,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }

    // header cell style
    private hLCS = {
        shading: {
            fill: this.colorsCellHighlightLight,
            color: "auto",
        },
        verticalAlign: VerticalAlign.CENTER
    }

    private styles: object = {
        default: {
            document: {
                run: {
                    font: this.font,
                    size: this.fontSizeSm
                }
            },
            heading1: {
                run: {
                    size: this.fontSizeXxl,
                    bold: true,
                    italics: true,
                    color: this.colorsTextPrimary,
                },
                paragraph: {
                    spacing: {
                        after: this.spacingXxl,
                    },
                },
            },
            heading2: {
                run: {
                    size: this.fontSizeLg,
                    bold: true,
                },
                paragraph: {
                    spacing: {
                        after: this.spacingXl,
                    },
                },
            },
            heading3: {
                run: {
                    size: this.fontSizeLg,
                    bold: true,
                },
                paragraph: {
                    spacing: {
                        after: this.spacingLg,
                    },
                },
            },
            heading4: {
                run: {
                    size: this.fontSizeMd,
                    bold: true,
                },
                paragraph: {
                    spacing: {
                        after: this.spacingMd,
                    },
                },
            }
        }
    }

    private generateDocItemText(docItem: DocItem): Paragraph {
        return new Paragraph({
            text: docItem.content,
            bullet: {
                level: 0
            }
        })
    }

    private generateEvCrParagraph(evCr: DocItem): Array<Paragraph> {
        const evCrTextIndex = evCr.content?.substring(0, evCr.content?.indexOf(' '));
        const evCrParagraphs: Array<Paragraph> = [
            new Paragraph({
                text: evCrTextIndex,
                heading: HeadingLevel.HEADING_4,
                // bullet: {
                //     level: 1
                // }
            }),
            new Paragraph({
                text: evCr.content,
            })
        ]
        evCr.bWs?.forEach(bW => {
            evCrParagraphs.push(this.generateDocItemText(bW))
        })
        return evCrParagraphs
    }

    private generateEEvCrParagraph(eEvCr: DocItem): Array<Paragraph> {
        const eEvCrTextIndex = eEvCr.content?.substring(0, eEvCr.content?.indexOf(' '));
        const eEvCrParagraphs: Array<Paragraph> = [
            new Paragraph({
                text: eEvCrTextIndex,
                heading: HeadingLevel.HEADING_4,
            }),
            new Paragraph({
                text: eEvCr.content,
            })
        ]
        eEvCr.eBWs?.forEach(bW => {
            eEvCrParagraphs.push(this.generateDocItemText(bW))
        })
        return eEvCrParagraphs
    }

    private generateSSParagraph(specificSkill: DocItem): Array<Paragraph> {
        let sSTextIndex = specificSkill.content?.substring(0, specificSkill.content?.indexOf(' '));
        sSTextIndex = sSTextIndex.substring(0, sSTextIndex.indexOf('.'))
        let sSParagraphs: Array<Paragraph> = [
            new Paragraph({
                text: sSTextIndex,
                heading: HeadingLevel.HEADING_3,
            }),
            new Paragraph({
                text: specificSkill.content,
                
            }),
        ]
        specificSkill.evCrs?.forEach(evCr => {
            sSParagraphs = sSParagraphs.concat(this.generateEvCrParagraph(evCr))
        })
        return sSParagraphs
    }

    private generateELParagraph(ecosocialLearning: DocItem): Array<Paragraph> {
        const eLTextIndex = ecosocialLearning.content?.substring(0, ecosocialLearning.content?.indexOf(' '));
        let eLParagraphs: Array<Paragraph> = [
            new Paragraph({
                text: eLTextIndex,
                heading: HeadingLevel.HEADING_3,
            }),
            new Paragraph({
                text: ecosocialLearning.content,
            }),
        ]
        ecosocialLearning.eEvCrs?.forEach(eEvCr => {
            eLParagraphs = eLParagraphs.concat(this.generateEEvCrParagraph(eEvCr))
        })
        return eLParagraphs
    }

    private generateLUParagraph(learningUnit: DocLearningUnit): Array<Paragraph> {
        let uLParagraphs: Array<Paragraph> = [new Paragraph({
            text: learningUnit.name,
            heading: HeadingLevel.HEADING_2
        })]
        learningUnit.sSs.forEach(sS => {
            uLParagraphs = uLParagraphs.concat(this.generateSSParagraph(sS))
        })
        learningUnit.eLs.forEach(eL => {
            uLParagraphs = uLParagraphs.concat(this.generateELParagraph(eL))
        })
        return uLParagraphs
    }

    private generateTableCell(text: string | Array<Paragraph>, cellOptions: object = {},paragraphOptions: object = {}): TableCell {
        if(Array.isArray(text)) {
            return new TableCell({
                children: [...text],
                ...cellOptions
            })
           
        } else {
            return new TableCell({
                children: [
                    new Paragraph({
                        text,
                        alignment: AlignmentType.CENTER,
                        ...paragraphOptions
                    }
                )],
                ...cellOptions
            })
        }
    }

    // Generates a row given cells content and (optionally) options and paragraphOptions 
    private generateTableRow(cellsContent: Array<string>,cellsOptions: Array<object> = [],paragraphsOptions: Array<object> = []): TableRow {
        const cells: Array<TableCell> = []
        if(cellsOptions.length > 0 && paragraphsOptions.length > 0){
            cellsContent.forEach((content,i) => cells.push(this.generateTableCell(content,cellsOptions[i],paragraphsOptions[i])))
        }else if(cellsOptions.length > 0){
            cellsContent.forEach((content,i) => cells.push(this.generateTableCell(content,cellsOptions[i])))
        } else {
            cellsContent.forEach((content,i) => cells.push(this.generateTableCell(content)))
        }
        const row: TableRow = new TableRow({
            children: cells,
        });
        return row
    }

    // For both evCr's and eEvCr's
    private getRowInfo(evCrContent: string, lUs: Array<DocLearningUnit>): Array<string> {
        // Get info        
        let sSOrEL = ''
        const lUAppereances: Array<string> = Array.from({length: lUs.length}, () => '') 
        let sum = 0
        lUs.forEach((lU,i) => {
            lU.sSs?.forEach(sS => {
                sS.evCrs.forEach(e => {
                    if(e.content == evCrContent){
                        // Grab only the reference
                        sSOrEL = sS.content.substring(0, sS.content.indexOf(' '))
                        // Add appereance
                        lUAppereances[i] = this.checkedText
                        sum += 1
                    }
                })
            })
            lU.eLs?.forEach(eL => {
                eL.eEvCrs.forEach(e => {
                    if(e.content == evCrContent){
                        // Grab only the reference
                        sSOrEL = eL.content.substring(0, eL.content.indexOf(' '))
                        // Add appereance
                        lUAppereances[i] = this.checkedText
                        sum += 1
                    }
                })
            })
        })

        // Row cells content
        const rowCellsText = [sSOrEL,evCrContent.substring(0, evCrContent.indexOf(' ')),...lUAppereances,sum.toString()]

        return rowCellsText
    }

    private generateLUsStatsTableRows(lUs: Array<DocLearningUnit>, texts: object): Array<TableRow> {
        const rows: Array<TableRow> = []

        let mergedSSs: Array<object> = []
        let mergedELs: Array<object> = []
        const mergedEvCrs: Array<string> = []
        const mergedEEvCrs: Array<string> = []

        // Texts and styles of table cells
        // Learning units have to span for the number of lU
        const firstHeadersTexts = ['','',texts.learningUnits,'']
        const firstHeadersOptions= [{},{},{columnSpan: lUs.length},{}]
        const firstHeadersParagraphOptions = [{},{},{heading: HeadingLevel.HEADING_2},{}]
        const secondHeadersTexts = [texts.sSOrEls,texts.evaluationCriterias]
        const secondHeadersOptions= [this.hCS,this.hCS]
        const evCrRowOptions: Array<object> = [this.bSCS,this.bSCS]
        let sSsTotalsTexts = [texts.specificSkills]
        let eLsTotalsTexts = [texts.ecosocialLearnings]
        let evCrsTotalsTexts = [texts.evaluationCriterias]
        const sSsTotalsOptions: Array<object> = [{...this.hCS,columnSpan: 2}]
        const eLsTotalsOptions: Array<object> = [{...this.hCS,columnSpan: 2}]
        const evCrsTotalsOptions: Array<object> = [{...this.hCS,columnSpan: 2}]

        lUs.forEach(lU => {
            // Add all sSs and eLs to its arr
            mergedSSs = mergedSSs.concat([...lU.sSs])
            mergedELs = mergedELs.concat([...lU.eLs])
            // Add learning unit title as header
            secondHeadersTexts.push(lU.name)
            secondHeadersOptions.push(this.hLUCS)
            // Add learning units cells styles
            sSsTotalsOptions.push(this.bSCS)
            eLsTotalsOptions.push(this.bSCS)
            evCrsTotalsOptions.push(this.bSCS)
            evCrRowOptions.push(this.bCS)
        })

        // Add last cell of second headers
        secondHeadersTexts.push(texts.totalOfEvCrAppearences)
        secondHeadersOptions.push(this.hCS)
        evCrRowOptions.push(this.bSCS)

        mergedSSs.forEach(sS => {
            // Merge all evCrs without repeating
            sS.evCrs?.forEach(e => {
                const eIndex = mergedEvCrs.findIndex(ee => ee.content == e.content)
                if(eIndex == -1) {
                    mergedEvCrs.push(e.content)
                }
            })
        })
        mergedELs.forEach(eL => {
            // Merge eEvCr's if exist
            eL.eEvCrs?.forEach(e => {
                const eIndex = mergedEEvCrs.findIndex(ee => ee.content == e.content)
                if(eIndex == -1) {
                    mergedEEvCrs.push(e.content)
                }
            })
        })

        // Generate rows info
        const sSRowsInfo: Array<Array<string>> = []
        const eLRowsInfo: Array<Array<string>> = []
        mergedEvCrs.forEach(e => {
            sSRowsInfo.push(this.getRowInfo(e,lUs))
        })
        mergedEEvCrs.forEach(e => {
            eLRowsInfo.push(this.getRowInfo(e,lUs))   
        })

        // Calculate totals
        const arrOfLUsLength: Array<number> = Array.from({length: lUs.length}, () => 0)
        const tempTotalsMatrix: Array<Array<number>> = [
            [...arrOfLUsLength],
            [...arrOfLUsLength],
            [...arrOfLUsLength]
        ]
        let prevSS: Array<Array<string>> = []
        let prevEL: Array<Array<string>> = []
        sSRowsInfo[0].forEach(col => {
            prevSS.push([])
            prevEL.push([])
        })
        sSRowsInfo.forEach((row,rowIndex) => {
            row.forEach((content,colIndex) => {
                if(content == this.checkedText){
                    // First two positions of sSRowInfo and eLRowsInfo are the text reference, therefore substract 2 to the colIndex
                    tempTotalsMatrix[2][colIndex-2] += 1
                    // If its a new SS add one to counter
                    if(!prevSS[colIndex-2].includes(row[0])){
                        tempTotalsMatrix[0][colIndex-2] += 1 
                        prevSS[colIndex-2].push(row[0])
                    }
                }
            })
        })
        eLRowsInfo.forEach((row,rowIndex) => {
            row.forEach((content,colIndex) => {
                if(content == this.checkedText){
                    // First two positions of sSRowInfo and eLRowsInfo are the text reference, therefore substract 2 to the colIndex
                    tempTotalsMatrix[2][colIndex-2] += 1
                    // If its a new EL add one to counter
                    if(!prevEL[colIndex-2].includes(row[0])){
                        tempTotalsMatrix[1][colIndex-2] += 1
                        prevEL[colIndex-2].push(row[0])
                    }
                }
            })
        })
        // Add an empty string at the end for last column
        sSsTotalsTexts = sSsTotalsTexts.concat(...tempTotalsMatrix[0].map(String),'')
        eLsTotalsTexts = eLsTotalsTexts.concat(...tempTotalsMatrix[1].map(String),'')
        evCrsTotalsTexts = evCrsTotalsTexts.concat(...tempTotalsMatrix[2].map(String),'')

        // Sort rows
        sSRowsInfo.sort((r1,r2) => {
            return r1[0].localeCompare(r2[0])
        })
        eLRowsInfo.sort((r1,r2) => {
            return r1[0].localeCompare(r2[0])
        })

        // Generate headers
        rows.push(this.generateTableRow(firstHeadersTexts,firstHeadersOptions,firstHeadersParagraphOptions))
        rows.push(this.generateTableRow(secondHeadersTexts,secondHeadersOptions))
        // Generate rows
        sSRowsInfo.forEach(rowInfo => rows.push(this.generateTableRow(rowInfo,evCrRowOptions)))
        eLRowsInfo.forEach(rowInfo => rows.push(this.generateTableRow(rowInfo,evCrRowOptions)))
        // Generate totals
        rows.push(this.generateTableRow(sSsTotalsTexts,sSsTotalsOptions))
        rows.push(this.generateTableRow(eLsTotalsTexts,eLsTotalsOptions))
        rows.push(this.generateTableRow(evCrsTotalsTexts,evCrsTotalsOptions))

        return rows
    }

    private generateHeaderTable(title: string, fuhemLogo: ImageRun, vinculoEcosocialLogo: ImageRun): Table {
        const noBorders =  {
            top: {
                style: BorderStyle.NONE,
                size: 0,
            },
            bottom: {
                style: BorderStyle.NONE,
                size: 0,
            },
            left: {
                style: BorderStyle.NONE,
                size: 0,
            },
            right: {
                style: BorderStyle.NONE,
                size: 0,
            },
        }

        return new Table({
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            children: [
                                new Paragraph({
                                    text: title,
                                    heading: HeadingLevel.HEADING_4,
                                })    
                            ],
                            verticalAlign: AlignmentType.CENTER,
                            borders: noBorders
                        }),
                        new TableCell({
                            children: [
                                new Paragraph({
                                    children: [fuhemLogo],
                                }
                            )],
                            borders: noBorders
                        }),
                        new TableCell({
                            children: [
                                new Paragraph({
                                    children: [vinculoEcosocialLogo],
                                }
                            )],
                            borders: noBorders
                        }),
                    ],
                })
            ],
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            },
            columnWidths: [convertMillimetersToTwip(192), convertMillimetersToTwip(30), convertMillimetersToTwip(18)],
            borders: noBorders
        })
    }

    private generateLUSummaryTitleTable(lUName: string, index: number, texts: object): Table {
        const rows: Array<TableRow> = []

        // Texts and styles of table cells
        const headersTexts = [texts.learningUnit + ` #${index+1}`,lUName]
        const headersOptions= [this.bCS,this.hLUCS]
        // Paragraph options
        const pOpt = {alignment: AlignmentType.CENTER,heading:HeadingLevel.HEADING_2}
        
        // Generate headers
        rows.push(this.generateTableRow(headersTexts,headersOptions,[pOpt,pOpt]))

        return new Table({
            rows,
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            },
            columnWidths: [convertMillimetersToTwip(100), convertMillimetersToTwip(140)],
        })
    }

    private generateLUSummaryTable(lU: DocLearningUnit, texts: object): Table {
        const rows: Array<TableRow> = []

        // Texts and styles of table cells
        const headersTexts = [texts.sSOrEls,texts.evaluationCriterias,'',texts.basicWisdoms]
        const headersOptions= [this.hCS,this.hCS,{...this.bCS,width:{ size: 5, type: WidthType.PERCENTAGE}},this.hLCS]
        // Paragraph options
        const pOpt = {alignment: AlignmentType.LEFT}
        
        // Generate headers
        rows.push(this.generateTableRow(headersTexts,headersOptions))
        
        // Sum the total number of evCrs and eEvCrs
        const numOfRows = lU.sSs.reduce((acc,curr) => acc + curr.evCrs?.length,0) + lU.eLs.reduce((acc,curr) => acc + curr.eEvCrs?.length,0)

        // bWs string
        let bWsContent: Array<Paragraph> = []
        lU.bWs.forEach(b => {
            bWsContent.push(
                new Paragraph({
                    text: '- ' + b.content,
                    ...pOpt
                })
            ) 
        })

        // Generate sS rows
        lU.sSs.forEach( (sS,i) => {
            sS.evCrs?.forEach((evCr,j) => {
                if(i == 0 && j == 0){
                    // If is the first row add sS and span it to the evCr's length
                    const spanedBCS = {...this.bCS,rowSpan:sS.evCrs?.length}
                    // Also add the white column and bW's column and span to all rows
                    const spanedWhiteCol = {...this.bCS,width:{ size: 5, type: WidthType.PERCENTAGE},rowSpan: numOfRows}
                    const spanedBWsCol = {...this.bCS,rowSpan: numOfRows}
                    rows.push(this.generateTableRow([sS.content,evCr.content,'',bWsContent],[spanedBCS,this.bCS,spanedWhiteCol,spanedBWsCol],[pOpt,pOpt,pOpt,pOpt]))
                } else if(j == 0 && i != 0){
                    const spanedBCS = {...this.bCS,rowSpan:sS.evCrs?.length}
                    rows.push(this.generateTableRow([sS.content,evCr.content],[spanedBCS,this.bCS],[pOpt,pOpt]))
                } else {
                    rows.push(this.generateTableRow([evCr.content],[this.bCS],[pOpt]))
                }

            })
        })

        // Generate eL rows
        lU.eLs.forEach( eL => {
            eL.eEvCrs?.forEach((eEvCr,i) => {
                if(i == 0){
                    // If is the first row add eL and span it to the eEvCr's length
                    const spanedBCS = {...this.bCS,rowSpan:eL.eEvCrs?.length}
                    rows.push(this.generateTableRow([eL.content,eEvCr.content],[spanedBCS,this.bCS],[pOpt,pOpt]))
                }else {
                    rows.push(this.generateTableRow([eEvCr.content],[this.bCS],[pOpt]))
                }

            })
        })

        return new Table({
            rows,
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            },
            columnWidths: [convertMillimetersToTwip(65), convertMillimetersToTwip(70), convertMillimetersToTwip(5), convertMillimetersToTwip(100)],
        })
    }

    public async generateEcosocialProgram(title: string, learningUnits: Array<DocLearningUnit>, texts: object): Promise< Blob | false > {

        console.log("[generateEcosocialProgram] Starting file generation...")
        
        const logoFuhemImgPath = path.join(process.cwd(), 'public', 'logo_fuhem.png');
        const fuhemLogo = new ImageRun({
            data: fs.readFileSync(logoFuhemImgPath),
            transformation: {
                width: 100,
                height: 80,
            },
        })
        
        const logoVinculoEcosocialImgPath = path.join(process.cwd(), 'public', 'logo_vinculo_ecosocial.png');
        const vinculoEcosocialLogo = new ImageRun({
            data: fs.readFileSync(logoVinculoEcosocialImgPath),
            transformation: {
                width: 60,
                height: 80,
            },
        })

        const pageBreak = new Paragraph({
            text: " ",
            pageBreakBefore: true
        })

        const spacer = new Paragraph({
            text: " ",
        })

        // Generate learning unit sections
        const lUsSummaryTables: Array<Table | Paragraph> = []
        learningUnits.forEach((lU,i) => {
            lUsSummaryTables.push(this.generateLUSummaryTitleTable(lU.name,i,texts))
            lUsSummaryTables.push(spacer)
            lUsSummaryTables.push(this.generateLUSummaryTable(lU,texts))
            lUsSummaryTables.push(pageBreak)
        })

        let lUsTable = new Table({
            rows: this.generateLUsStatsTableRows(learningUnits,texts),
            width: {
                size: 100,
                type: WidthType.PERCENTAGE,
            },
        })

        const programDoc = new Document({
            creator: "FUHEM + amoved with love",
            title,
            sections: [
                {
                    headers: {
                        default: new Header({
                            children: [this.generateHeaderTable(title,fuhemLogo,vinculoEcosocialLogo)],
                        }),
                    },
                    properties: {
                        page: {
                            size: {
                                orientation: PageOrientation.LANDSCAPE,
                            },
                        },
                    },
                    children: [...lUsSummaryTables,lUsTable]
                },
            ],
            styles: this.styles
        });

        try {
            const fileBuffer = await Packer.toBlob(programDoc)
            return fileBuffer
        } catch(e) {
            console.log("Error reading file")
            console.log(e)
        }

        return false
    }
}