import { createLogger, transports, format } from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import type { ILoggerPort } from "../application/logger.port.interface";

export class LoggerAdapterWinston implements ILoggerPort {

    private logger: any

    constructor(config: any) {

        const customFormat = format.printf(info => {
            if (info.stack) {
              return `${info.timestamp} ${info.level}: ${info.message} \r\n${info.stack}`
            } else {
              return `${info.timestamp} ${info.level}: ${info.message}`
            }
        })
          
        const logger = createLogger({
            exitOnError: false,
            transports: process.env.NODE_ENV !== 'production'
              ? [new transports.Console(
                  {
                    level: config.log_level || 'debug',
                    format: format.combine(format.splat(), format.timestamp(), format.colorize(), customFormat)
                  }
                )]
              : [new DailyRotateFile({
                  level: config.log_level || 'info',
                  filename: config.log_path + `/${process.env.APP_NAME}.%DATE%.log`,
                  symlinkName: `${process.env.APP_NAME}.log`,
                  createSymlink: true,
                  zippedArchive: true,
                  maxSize: '10m',
                  maxFiles: '10d',
                  format: format.combine(format.timestamp(), customFormat)
                }),
                new transports.Console(
                  {
                    level: config.log_level || 'info',
                    format: format.combine(format.timestamp(), format.colorize(), customFormat)
                  }
                )]
        })
          
        logger.info(`Logging to ${config.log_path}/${process.env.APP_NAME}.log (level: ${config.log_level})`)
        
        this.logger = logger
    }

    public info(message: string) {
        this.logger.info(message)
    }
    public debug(message: string) {
        this.logger.debug(message)
    }
    public warning(message: string) {
        this.logger.warning(message)
    }
    public error(message: string) {
        this.logger.error(message)
    }

}


