export enum LogType {
    INFO,
    WARNING,
    ERROR
}

export class Log {
    type: LogType
    message: string
    code: string

    constructor(type: LogType, message: string, code: string) {
        this.type = type;
        this.message = message;
        this.code = code;
    }
}

