import es from './locales/es.json'
import ca from './locales/ca.json'
import eu from './locales/eu.json'
import gl from './locales/gl.json'

export default defineI18nConfig(() => ({
    legacy: false,
    fallbackLocale: 'es',
    messages: {
      es,
      ca,
      eu,
      gl
    }
  }))