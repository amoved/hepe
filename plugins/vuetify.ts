import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { es, en } from 'vuetify/locale'

export const bordersColors = {
    green: '#94D892',
    violet: '#AE8CF8',
    blue: '#7FB5F9',
    red: '#EF8481',
    pink: '#F1A0EC',
    teal: '#44AFB6',
    grey: '#AAC7D0',
    orange: '#FCCE8A',
}

export const lightTheme = {
    dark: false,
    colors: {
        background: '#E2E5E6',
        surface: '#FFFFFF',
        'surface-bright': '#F3F4F6',
        primary: '#00793c',
        // primary: '#00C42E',
        text: '#111827',
        ['light-border']: '#E2E5E6',
        ['medium-grey']: '#6A717E',
        ['medium-grey-2']: '#C2C4C9',
        ['dark-grey']: '#111827',
        selected: '#E7B81F',
        ['sidebar-active-item']: '#F8F8FA',
        ['not-selected']: '#C2C4C9',
        ['light-blue']: '#d6e2e7',
        ['medium-blue']: '#6591A4',
        ...bordersColors
        
    },
    variables: {
        'body-font-family': 'sans-serif',
    },
    options: {
        customProperties: true,
    },
}
  
export default defineNuxtPlugin(nuxtApp => {
    const vuetify = createVuetify({
        theme: {
            defaultTheme: 'lightTheme',
            themes: {
              lightTheme,
            },
        },
        components,
        directives,
        // TODO: add locales
        // https://vuetifyjs.com/en/features/internationalization/#getting-started
        locale: {
            locale: 'es',
            fallback: 'en',
            messages: { es, en }
        }
    })

    nuxtApp.vueApp.use(vuetify)
})
