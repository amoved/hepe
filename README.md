# HEPE 🌱👩🏽‍🏫

Hepe o Herramienta Escolar para la Programación Ecosocial es una aplicación web para facilitar la realización de programaciones curriculares ecosociales compatibles con la administración.

## Arquitectura

Esta app está basada en una plantilla de Nuxt que estamos desarrollando, está diseñada de la siguiente manera:

### Stack

Principales tecnologías:

- TypeScript y JavaScript: lenguajes de programación
- [Vue 3]((https://vuejs.org)): framework de JS para desarrollo frontend
- Nuxt 3: framework de Vue para aplicaciones fullstack
- Vuetify 3: biblioteca de componentes UI
- Material Design Icons: biblioteca de iconos
- Prisma: ORM
- MySQL: base de datos

Utilidades:
- Pug: preprocesador de HTML
- Sass: preprocesador de CSS

Módulos de Nuxt:
- [Pinia](https://pinia.vuejs.org/introduction.html): control de estado de la aplicación
- nuxt-auth (sidebar): facilita la gestión de credenciales en el cliente y permite integrar con OAuth y MagicLinks.
- i18n: soporte multi-idiomas de forma sencilla.
- [vee-validate](https://vee-validate.logaretm.com/v4/integrations/nuxt/): módulo de validación para campos de texto.


## Puesta en marcha

### Instalación de dependencias

```bash
# npm
npm install
```

### Desarrollo en local

Seleccionar una base de datos y herramientas de desarrollo:

#### Base de datos en tu máquina local 
Puedes crear una base de datos en local y añadir su ruta de acceso al archivo `.env`.

#### Base de datos y gestor dockerizados 
Puedes utilizar los contenedores que proporcionamos. Para ello, renombra el archivo `.env-example` a `.env` y actualiza los valores de las variables. Recuerda respetar el formato de enlace a la base de datos (DB_URL) que [soporta Prisma](https://pris.ly/d/connection-strings). También debes actualizar el proveedor en `./prisma/shcema.prisma` en caso de que selecciones una base de datos diferente.

Por último ejecuta:
```bash
docker compose up
```

**Nota**: el volumen de la base de datos está configurado para ser almacenado en ./containers/db/data, puedes cambiar la configuración si no se adapta a tus necesidades. 

Ahora puedes acceder a PhPMyAdmin y dar permisos de la base de datos al usuario configurado.

<img src="./docs/assets/img/phpmyadmin_add_user_priv.png">

#### Crear una base de datos y cargar con datos iniciales

Conecta con la base de datos, actualiza los modelos y carga los datos definidos en `prisma/schema.prisma`

```bash
npx prisma migrate dev --name initial-migration
```
Cuando se ejecuta este comando por primera vez la carga de datos se realiza automáticamente. 

Si no es la primera vez podemos utilizar los comandos `db:generate` y `db:seed`:
```bash
npm run db:generate
npm run db:seed
```

#### Iniciar la aplicación 

Por último, lanzamos la aplicación

```bash
# npm
npm run dev
```
La aplicación se lanzará en [http://localhost:3000](http://localhost:3000)

## Preparación para producción

Compilar apliación para producción

```bash
# npm
npm run build
```

Previsualizar la versión de producción en local:

```bash
# npm
npm run preview
```

## Licencia
Este proyecto está licenciado bajo AGPLv3 o posterior
