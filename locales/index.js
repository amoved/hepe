export const locales = {
  ca: 'Català',
  gl: 'Galego',
  es: 'Castellano',
  eu: 'Euskara',
}
