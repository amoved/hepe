import type { EcosocialLearning as PrismaEcosocialLearning } from "@prisma/client";
import { v4 as uuidv4 } from 'uuid'

function checkSSIndexes(
    specificSkills: Array<SpecificSkill>, 
    indexes: EvCrIndexes, 
    evCrId: number
): boolean {
    return specificSkills[indexes.sSIndex].evCrs[indexes.evCrIndex].id == evCrId
}

function checkELIndexes(
    ecosocialLearnings: Array<EcosocialLearning>, 
    indexes: EEvCrIndexes, 
    eEvCrId: number
): boolean {
    return ecosocialLearnings[indexes.eLIndex].eEvCrs[indexes.eEvCrIndex].id == eEvCrId
}

export const useProgramStore = defineStore('program', {
    state: (): ProgramState => ({
        id: -1,
        course: {
            id: -1,
            name: '',
            academicYear: '',
            region: '',
            locale: '',
        },
        specificSkills: [],
        basicWisdoms: [],
        ecosocialLearnings: [],
        learningUnits: [],
        currentStep: 1,
    }),
    actions: {
        setId(id: number): void {
            this.id = id
        },
        setCourse(course: Course): void {
            this.course = course
        },
        resetCourse(): void {
            this.course = {
                id: -1,
                name: '',
                academicYear: '',
                region: '',
                locale: ''
            }
        },
        setSpecificSkills(specificSkills: Array<SpecificSkill>): void {
            this.specificSkills = [...specificSkills]
        },
        // Set the last level of basic wisdom
        setBasicWisdoms(basicWisdoms: Array<BasicWisdom>): void {
            this.basicWisdoms = [...basicWisdoms]
        },
        addSS(specificSkill: SpecificSkill): void {
            this.specificSkills.push(specificSkill)
            console.debug("Add specific skill")
        },
        addBWToEvCr(sSId: number, evCrId: number, bWId: number): void {
            const { sSIndex, evCrIndex, bWIndex } = useSpecificSkillsIndexes(this.specificSkills,this.basicWisdoms,sSId,evCrId,bWId)
            this.specificSkills[sSIndex].evCrToBWMatrix[evCrIndex][bWIndex] = true
            console.debug("Add bW to evCr")

            // update LUs if they exist
            if(this.learningUnits.length > 0){
                this.learningUnits.forEach(lU => {
                    // Check if lU contains this evCr
                    const pEvCrIndex = lU.partialEvCrs.findIndex(indexes => checkSSIndexes(this.specificSkills,indexes,evCrId))
                    if(pEvCrIndex > -1){
                        console.debug(`Update ${lU.name}`)
                        lU.evCrToBWMatrix[pEvCrIndex][bWIndex] = true
                        // If no other evCr added this bW, add it to partial
                        const pBWIndex = lU.partialBWs.findIndex(({bWIndex: index}) => index == bWIndex)
                        if(pBWIndex == -1){
                            console.debug(`Add bW to partials`)
                            lU.partialBWs.push({
                                bWIndex,
                                keep: true
                            })
                        }
                    }
                })
            }
        },
        removeBWFromEvCr(sSId: number, evCrId: number, bWId: number): void {
            const { sSIndex, evCrIndex, bWIndex } = useSpecificSkillsIndexes(this.specificSkills,this.basicWisdoms,sSId,evCrId,bWId)
            this.specificSkills[sSIndex].evCrToBWMatrix[evCrIndex][bWIndex] = false
            console.debug("Remove bW from evCr")

            // update LUs if they exist
            if(this.learningUnits.length > 0){
                this.learningUnits.forEach(lU => {
                    // Check if lU contains this evCr
                    const pEvCrIndex = lU.partialEvCrs.findIndex(indexes =>  checkSSIndexes(this.specificSkills,indexes,evCrId))
                    if(pEvCrIndex > -1){
                        console.debug(`Update ${lU.name}`)
                        lU.evCrToBWMatrix[pEvCrIndex][bWIndex] = false
                        let counter = 0
                        lU.evCrToBWMatrix.forEach(bWFilter => counter = bWFilter[bWIndex] ? counter + 1 : counter )
                        // If no other evCr added this bW, remove it from partial
                        const pBWIndex = lU.partialBWs.findIndex(({bWIndex: index}) => index == bWIndex)
                        if(pBWIndex > -1 && counter == 0){
                            console.debug(`Remove bW from partials`)
                            lU.partialBWs.splice(pBWIndex,1)
                        }
                    }
                })
            }
        },
        setEcosocialLearnings(ecosocialLearnings: Array<EcosocialLearning>): void {
            this.ecosocialLearnings = [...ecosocialLearnings]
        },
        createEcosocialLearning(ecosocialLearning: PrismaEcosocialLearning): void {
            console.debug("[store] Create ecosocial learning")
            let eEvCrs: Array<EcosocialEvaluationCriteria> = []
            ecosocialLearning.ecosocialEvaluationCriterias.forEach(e => eEvCrs.push({
                id: e.id,
                content: e.content
            }))
            let eBWs: Array<EcosocialBasicWisdom> = []
            ecosocialLearning.ecosocialBasicWisdoms.forEach(b => eBWs.push({
                id: b.id,
                content: b.content
            }))
            let eEvCrToEBWMatrix: Array<Array<boolean>> = []
            let eEvCrToUDEBWMatrix: Array<Array<boolean>> = []
            ecosocialLearning.ecosocialEvaluationCriterias.forEach(() => {
                eEvCrToUDEBWMatrix.push([])
                eEvCrToEBWMatrix.push(
                    Array.from(
                        { length: ecosocialLearning.ecosocialBasicWisdoms.length }, 
                        () => false
                    )
                )
            })
            const selectedEEvCrs = Array.from(
                { length: ecosocialLearning.ecosocialEvaluationCriterias.length }, 
                () => false
            )
            this.ecosocialLearnings.push({
                id: ecosocialLearning.id,
                content: ecosocialLearning.content,
                group: ecosocialLearning.group,
                eEvCrs,
                selectedEEvCrs,
                eBWs,
                eEvCrToEBWMatrix,
                uDEBWs: [],
                eEvCrToUDEBWMatrix,
            })
        },
        deleteEcosocialLearning(ecosocialLearning: PrismaEcosocialLearning): void {
            const index = this.ecosocialLearnings.findIndex(eL => eL.id == ecosocialLearning.id)
            if(index > -1){
                this.ecosocialLearnings.splice(index,1)
                console.debug("[store] Delete ecosocial learning")
                // Update lUs
                this.learningUnits.forEach(lU => {
                    // Deallocate eEvCrs associated to this eL
                    const eEvCrsToDeallocate = lU.partialEEvCrs.filter(p => p.eLIndex == index)
                    eEvCrsToDeallocate.forEach(indexes => {
                        this.deallocateEEvCr(
                            lU.name,
                            this.ecosocialLearnings[indexes.eLIndex].id,
                            this.ecosocialLearnings[indexes.eLIndex].eEvCrs[indexes.eEvCrIndex].id
                        )
                    })
                    // Update eL indexes
                    lU.partialEEvCrs.forEach(p => {
                        if(p.eLIndex > index){
                            p.eLIndex = p.eLIndex - 1
                        }
                    })
                    lU.partialEBWs.forEach(p => {
                        if(p.eLIndex > index){
                            p.eLIndex = p.eLIndex - 1
                        }
                    })
                    lU.partialUDEBWs.forEach(p => {
                        if(p.eLIndex > index){
                            p.eLIndex = p.eLIndex - 1
                        }
                    })
                })
            } else {
                console.debug("[store] No ecosocial learning found to delete")
            }
        },
        addEEvCr(eLId: number, eEvCrId: number): void {
            const { eLIndex, eEvCrIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId)
            this.ecosocialLearnings[eLIndex].selectedEEvCrs[eEvCrIndex] = true
            console.debug("[store] Add eEvCr")
        },
        removeEEvCr(eLId: number, eEvCrId: number): void {
            const { eLIndex, eEvCrIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId)
            this.ecosocialLearnings[eLIndex].selectedEEvCrs[eEvCrIndex] = false
            console.debug("[store] Remove eEvCr")

            // Update lUs
            if(this.learningUnits.length > 0){
                this.learningUnits.forEach(lU => {
                    const pIndex = lU.partialEEvCrs.findIndex(p => p.eLIndex == eLIndex && p.eEvCrIndex == eEvCrIndex)
                    if(pIndex > -1){
                        this.deallocateEEvCr(lU.name,eLId,eEvCrId)
                    }
                })
            }
        },
        addEBW(eLId: number, eEvCrId: number, eBWId: number, isUserDefined: boolean = false): void {
            const { eLIndex, eEvCrIndex, eBWIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId,eBWId)
            // Check whether it is a user defined eBW or not
            if(isUserDefined){
                this.ecosocialLearnings[eLIndex].eEvCrToUDEBWMatrix[eEvCrIndex][eBWIndex] = true
                // update LUs if they exist
                if(this.learningUnits.length > 0){
                    this.learningUnits.forEach(lU => {
                        // Check if lU contains this eEvCr
                        const pEEvCrIndex = lU.partialEEvCrs.findIndex(indexes => eLIndex == indexes.eLIndex && eEvCrIndex == indexes.eEvCrIndex)
                        if(pEEvCrIndex > -1){
                            console.debug(`Update ${lU.name}`)
                            lU.eEvCrToUDEBWMatrix[pEEvCrIndex][eBWIndex] = true
                            // If no other eEvCr added this uDEBW, add it to partial
                            const pUDEBWIndex = lU.partialUDEBWs.findIndex(({uDEBWIndex: index}) => index == eBWIndex)
                            if(pUDEBWIndex == -1){
                                console.debug(`Add uDEBW to partials`)
                                lU.partialUDEBWs.push({
                                    eLIndex,
                                    uDEBWIndex: eBWIndex,
                                    keep: true
                                })
                            }
                        }
                    })
                }
            } else {
                this.ecosocialLearnings[eLIndex].eEvCrToEBWMatrix[eEvCrIndex][eBWIndex] = true
                // update LUs if they exist
                if(this.learningUnits.length > 0){
                    this.learningUnits.forEach(lU => {
                        // Check if lU contains this eEvCr
                        const pEEvCrIndex = lU.partialEEvCrs.findIndex(indexes => eLIndex == indexes.eLIndex && eEvCrIndex == indexes.eEvCrIndex)
                        if(pEEvCrIndex > -1){
                            console.debug(`Update ${lU.name}`)
                            lU.eEvCrToEBWMatrix[pEEvCrIndex][eBWIndex] = true
                            // If no other eEvCr added this eBW, add it to partial
                            const pEBWIndex = lU.partialEBWs.findIndex(({eBWIndex: index}) => index == eBWIndex)
                            if(pEBWIndex == -1){
                                console.debug(`Add eBW to partials`)
                                lU.partialEBWs.push({
                                    eLIndex,
                                    eBWIndex,
                                    keep: true
                                })
                            }
                        }
                    })
                }
            }
            console.debug("[store] Add eBW")
        },
        removeEBW(eLId: number, eEvCrId: number, eBWId: number, isUserDefined: boolean = false): void {
            const { eLIndex, eEvCrIndex, eBWIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId,eBWId)
            // Toggle selection, check whether it is a user defined eBW or not
            if(isUserDefined){
                this.ecosocialLearnings[eLIndex].eEvCrToUDEBWMatrix[eEvCrIndex][eBWIndex] = false
                // update LUs if they exist
                if(this.learningUnits.length > 0){
                    this.learningUnits.forEach(lU => {
                        // Check if lU contains this eEvCr
                        const pEEvCrIndex = lU.partialEEvCrs.findIndex(indexes => eLIndex == indexes.eLIndex && eEvCrIndex == indexes.eEvCrIndex)
                        if(pEEvCrIndex > -1){
                            console.debug(`Update ${lU.name}`)
                            lU.eEvCrToUDEBWMatrix[pEEvCrIndex][eBWIndex] = false
                            let counter = 0
                            lU.eEvCrToUDEBWMatrix.forEach(uDEBWFilter => counter = uDEBWFilter[eBWIndex] ? counter + 1 : counter )
                            // If no other eEvCr added this uDEBW, remove it from partial
                            const pUDEBWIndex = lU.partialUDEBWs.findIndex(({uDEBWIndex: index}) => index == eBWIndex)
                            if(pUDEBWIndex > -1 && counter == 0){
                                console.debug(`Remove uDEBW from partials`)
                                lU.partialUDEBWs.splice(pUDEBWIndex,1)
                            }
                        }
                    })
                }
            } else {
                this.ecosocialLearnings[eLIndex].eEvCrToEBWMatrix[eEvCrIndex][eBWIndex] = false
                // update LUs if they exist
                if(this.learningUnits.length > 0){
                    this.learningUnits.forEach(lU => {
                        // Check if lU contains this eEvCr
                        const pEEvCrIndex = lU.partialEEvCrs.findIndex(indexes => eLIndex == indexes.eLIndex && eEvCrIndex == indexes.eEvCrIndex)
                        if(pEEvCrIndex > -1){
                            console.debug(`Update ${lU.name}`)
                            lU.eEvCrToEBWMatrix[pEEvCrIndex][eBWIndex] = false
                            let counter = 0
                            lU.eEvCrToEBWMatrix.forEach(eBWFilter => counter = eBWFilter[eBWIndex] ? counter + 1 : counter )
                            // If no other eEvCr added this eBW, remove it from partial
                            const pEBWIndex = lU.partialEBWs.findIndex(({eBWIndex: index}) => index == eBWIndex)
                            if(pEBWIndex > -1 && counter == 0){
                                console.debug(`Remove eBW from partials`)
                                lU.partialEBWs.splice(pEBWIndex,1)
                            }
                        }
                    })
                }
            }
            console.debug("[store] Remove eBW")
        },
        createUDEBW(content: string, eLId: number, eEvCrId: number): UserDefinedEcosocialBasicWisdom {
            const { eLIndex, eEvCrIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId)
            
            // Create uDEBW
            const userDefinedEBW: UserDefinedEcosocialBasicWisdom = {
                id: this.ecosocialLearnings[eLIndex].uDEBWs.length,
                content, // Gets value from text field
                isUserDefined: true
            }

            // Store it and save map value 
            this.ecosocialLearnings[eLIndex].uDEBWs.push(userDefinedEBW)
            this.ecosocialLearnings[eLIndex].eEvCrToUDEBWMatrix.forEach((arr,i) => arr.push(i == eEvCrIndex))
            

            return userDefinedEBW
        },
        setLearningUnits(learningUnits: Array<LearningUnit>): void {
            this.learningUnits = [...learningUnits]
        },
        createLearningUnit(name: string): void {
            const numOfLU = this.learningUnits.length
            this.learningUnits.push({
                order:  numOfLU > 0 ? this.learningUnits[numOfLU-1].order + 1 : 1,
                name,
                partialEvCrs: [],
                partialBWs: [],
                evCrToBWMatrix: [],
                partialEEvCrs: [],
                eEvCrToEBWMatrix: [],
                partialEBWs: [],
                eEvCrToUDEBWMatrix: [],
                partialUDEBWs: [],
                uDBWs: [],
                filterUDBWs: [],
            })  
            console.debug("[store] Create learning unit")
        },
        deleteLearningUnit(name: string): void {
            const index = this.learningUnits.findIndex(l => l.name == name)
            this.learningUnits.splice(index,1)
            console.debug("[store] Delete learning unit")
        },
        allocateEvCr(lUName: string, sSId: number, evCrId: number): void {
            const lUIndex = this.learningUnits.findIndex(l => l.name == lUName)
            const { sSIndex, evCrIndex } = useSpecificSkillsIndexes(this.specificSkills, this.basicWisdoms,sSId,evCrId)
            const lU = this.learningUnits[lUIndex]

            // Store the evCr
            const evCrIndexes: EvCrIndexes = { sSIndex, evCrIndex }
            lU.partialEvCrs.push(evCrIndexes)
            // Add a new row to the matrix
            const bWsFilter = [...this.specificSkills[sSIndex].evCrToBWMatrix[evCrIndex]]
            lU.evCrToBWMatrix.push(bWsFilter)
            
            // Store mapped bWs only if they are not already stored
            bWsFilter.forEach((addBW,bWIndex) => {
                const isAdded = lU.partialBWs.findIndex(({bWIndex: index}) => bWIndex == index ) > -1
                if(addBW && !isAdded){
                    lU.partialBWs.push({
                        bWIndex: bWIndex,
                        keep: true
                    }) 
                }
            })
            
            console.debug("[store] Allocate evCr")
        },
        deallocateEvCr(lUName: string, sSId: number, evCrId: number): void {
            const lUIndex = this.learningUnits.findIndex(l => l.name == lUName)
            const { sSIndex } = useSpecificSkillsIndexes(this.specificSkills,this.basicWisdoms,sSId,evCrId)
            const lU = this.learningUnits[lUIndex]

            // Delete stored evCr
            const pEvCrIndex = lU.partialEvCrs.findIndex(indexes => checkSSIndexes(this.specificSkills,indexes,evCrId))
            lU.partialEvCrs.splice(pEvCrIndex,1)
            // Delete the row from the matrix
            lU.evCrToBWMatrix.splice(pEvCrIndex,1)
            
            // Delete stored bWs
            const counters = Array.from({length: lU.partialBWs.length}, () => 0)
            lU.partialBWs = lU.partialBWs.filter(({bWIndex,keep},pBWIndex) => {
                // Count the number of evCrs that add this bW
                lU.evCrToBWMatrix.forEach(bWFilter => counters[pBWIndex] = bWFilter[bWIndex] ? counters[pBWIndex] + 1 : counters[pBWIndex])
                // Only remove if there is no other EvCr that is allocated which contains the bW
                if(keep && counters[pBWIndex] == 0){
                    console.debug(`Remove bW from partials`)
                    return false
                } 
                return true
            })

            console.debug("[store] Result",lU.partialBWs)
            console.debug("[store] Deallocate evCr")
        },
        allocateEEvCr(lUName: string, eLId: number, eEvCrId: number): void {
            const lUIndex = this.learningUnits.findIndex(l => l.name == lUName)
            const { eLIndex, eEvCrIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId)
            const lU = this.learningUnits[lUIndex]

            // Store the eEvCr
            const eEvCrIndexes: EEvCrIndexes = { eLIndex, eEvCrIndex }
            lU.partialEEvCrs.push(eEvCrIndexes)
            // Add a new row to the eBW matrix
            const eBWsFilter = [...this.ecosocialLearnings[eLIndex].eEvCrToEBWMatrix[eEvCrIndex]]
            lU.eEvCrToEBWMatrix.push(eBWsFilter)
            // Store its eBWs only if they are not already stored
            eBWsFilter.forEach((addEBW,eBWIndex) => {
                const isAdded = lU.partialEBWs.findIndex(p => p.eLIndex == eLIndex && p.eBWIndex == eBWIndex) != -1
                if(addEBW && !isAdded){
                    lU.partialEBWs.push({ 
                        eLIndex, 
                        eBWIndex, 
                        keep: true 
                    })
                }
            })
            // Add a new row to the uDEBW matrix
            const uDEBWsFilter = [...this.ecosocialLearnings[eLIndex].eEvCrToUDEBWMatrix[eEvCrIndex]]
            lU.eEvCrToUDEBWMatrix.push(uDEBWsFilter)
            // Store its uDEBWs only if it wasn't already stored
            uDEBWsFilter.forEach((addUDEBW,uDEBWIndex) => {
                const isAdded = lU.partialUDEBWs.findIndex(p => p.eLIndex == eLIndex && p.uDEBWIndex == uDEBWIndex) != -1
                if(addUDEBW && !isAdded){
                    lU.partialUDEBWs.push({ 
                        eLIndex, 
                        uDEBWIndex, 
                        keep: true 
                    })
                }
            })

            console.debug("[store] Allocate eEvCr")
        },
        deallocateEEvCr(lUName: string, eLId: number, eEvCrId: number): void {
            const lUIndex = this.learningUnits.findIndex(l => l.name == lUName)
            const { eLIndex, eEvCrIndex } = useEcosocialLearningIndexes(this.ecosocialLearnings,eLId,eEvCrId)
            const lU = this.learningUnits[lUIndex]

            // Delete the eEvCr
            const pIndex = lU.partialEEvCrs.findIndex(indexes => checkELIndexes(this.ecosocialLearnings,indexes,eEvCrId))
            lU.partialEEvCrs.splice(pIndex,1)
            // Delete the row from the eBWs matrix
            lU.eEvCrToEBWMatrix.splice(pIndex,1)

            // Delete stored eBWs
            let eBWCounters = Array.from({length: lU.partialEBWs.length}, () => 0)
            lU.partialEBWs = lU.partialEBWs.filter(({eLIndex,eBWIndex,keep},pEBWIndex) => {
                // Count the number of eEvCrs that add this eBW
                lU.eEvCrToEBWMatrix.forEach((eBWFilter,pEEvCrIndex) => {
                    // Only check for eBWs of the same eL
                    if(lU.partialEEvCrs[pEEvCrIndex].eLIndex == eLIndex){
                        eBWCounters[pEBWIndex] = eBWFilter[eBWIndex] ? eBWCounters[pEBWIndex] + 1 : eBWCounters[pEBWIndex]
                    }
                })
                // Only remove if there is no other EvCr that is allocated which contains the bW
                if(keep && eBWCounters[pEBWIndex] == 0){
                    console.debug(`Remove eBW from partials`)
                    return false
                }
                return true
            })
            
            // Delete the row from the uDEBWs matrix
            lU.eEvCrToUDEBWMatrix.splice(pIndex,1)

            // Delete stored uDEBWs
            let uDEBWCounters = Array.from({length: lU.partialUDEBWs.length}, () => 0)
            lU.partialUDEBWs = lU.partialUDEBWs.filter(({eLIndex,uDEBWIndex,keep},pUDEBWIndex) => {
                // Count the number of eEvCrs that add this eBW
                lU.eEvCrToEBWMatrix.forEach((uDEBWFilter,pEEvCrIndex) => {
                    // Only check for uDEBWs of the same eL
                    if(lU.partialEEvCrs[pEEvCrIndex].eLIndex == eLIndex){
                        uDEBWCounters[pUDEBWIndex] = uDEBWFilter[uDEBWIndex] ? uDEBWCounters[pUDEBWIndex] + 1 : uDEBWCounters[pUDEBWIndex]
                    }
                })
                // Only remove if there is no other EvCr that is allocated which contains the bW
                if(keep && uDEBWCounters[pUDEBWIndex] == 0){
                    console.debug(`Remove eBW from partials`)
                    return false
                }
                return true
            })

            console.debug("[store] Deallocate eEvCr")
        },
        createUDBW(content: string, lUName: string): UserDefinedBasicWisdom {
            const lUIndex = this.learningUnits.findIndex(lU => lU.name == lUName)

            // Create uDBW
            const userDefinedBW: UserDefinedBasicWisdom = {
                id: this.learningUnits[lUIndex].uDBWs.length,
                content, // Gets value from text field
                isUserDefined: true
            }

            // Store it and save map value 
            this.learningUnits[lUIndex].uDBWs.push(userDefinedBW)
            // Also store in partial 
            this.learningUnits[lUIndex].filterUDBWs.push(true)

            console.debug('[store] uDBW created')
            return userDefinedBW
        },
        addBWToLU(lUName: string, bWId: number, isUserDefined: boolean = false): void {
            const { lUIndex, pBWIndex, uDBWIndex } = useLearningUnitBWIndexes(this.learningUnits,lUName,this.basicWisdoms,bWId, isUserDefined)
            const lU = this.learningUnits[lUIndex]
            // Check whether it is a user defined bW or not           
            if(isUserDefined){
                lU.filterUDBWs[uDBWIndex] = true
            } else {
                lU.partialBWs[pBWIndex].keep = true
            }
            console.debug("[store] Add bW to lU")
        },
        removeBWFromLU(lUName: string, bWId: number, isUserDefined: boolean = false): void {
            const { lUIndex, pBWIndex, uDBWIndex } = useLearningUnitBWIndexes(this.learningUnits,lUName,this.basicWisdoms,bWId)
            const lU = this.learningUnits[lUIndex]
            // Toggle selection, check whether it is a user defined bW or not
            if(isUserDefined){
                lU.filterUDBWs[uDBWIndex] = false
            } else {
                lU.partialBWs[pBWIndex].keep = false
            }
            console.debug("[store] Remove bW from lU")
        },
        addEBWToLU(lUName: string, eLId: number, eBWId: number, isUserDefined: boolean = false): void {
            const { lUIndex, eLIndex, uDEBWIndex, eBWIndex } = useLearningUnitEBWIndexes(this.learningUnits,lUName,this.ecosocialLearnings,eLId,eBWId,isUserDefined)
            const lU = this.learningUnits[lUIndex]

            // Check whether it is a user defined eBW or not
            if(isUserDefined){
                const pUDEBWIndex = lU.partialUDEBWs.findIndex(indexes => indexes.eLIndex == eLIndex && indexes.uDEBWIndex == uDEBWIndex)
                lU.partialUDEBWs[pUDEBWIndex].keep = true
            } else {
                const pEBWIndex = lU.partialEBWs.findIndex(indexes => indexes.eLIndex == eLIndex && indexes.eBWIndex == eBWIndex)
                lU.partialEBWs[pEBWIndex].keep = true
            }
            console.debug("[store] Add eBW to lU")
        },
        removeEBWFromLU(lUName: string, eLId: number, eBWId: number, isUserDefined: boolean = false): void {
            const { lUIndex, eLIndex, uDEBWIndex, eBWIndex } = useLearningUnitEBWIndexes(this.learningUnits,lUName,this.ecosocialLearnings,eLId,eBWId,isUserDefined)
            const lU = this.learningUnits[lUIndex]
            
            // Toggle selection, check whether it is a user defined eBW or not
            if(isUserDefined){
                const pUDEBWIndex = lU.partialUDEBWs.findIndex(indexes => indexes.eLIndex == eLIndex && indexes.uDEBWIndex == uDEBWIndex)
                lU.partialUDEBWs[pUDEBWIndex].keep = false
            } else {
                const pEBWIndex = lU.partialEBWs.findIndex(indexes => indexes.eLIndex == eLIndex && indexes.eBWIndex == eBWIndex)
                lU.partialEBWs[pEBWIndex].keep = false
            }
            console.debug("[store] Remove eBW from lU")
        },
        changeLUOrder(lUIndex: number, order: number): void {
            this.learningUnits[lUIndex].order = order
            console.debug("[store] Order changed for lU")
        },
        setCurrentStep(step: number): void {
            this.currentStep = step
        },
        setLastSavedProgram(lastSavedProgram: string): void {
            this.lastSavedProgram = lastSavedProgram
        },
        resume(program: ProgramState): void {
            this.setId(program.id)
            this.setCourse(program.course)
            this.setBasicWisdoms(program.basicWisdoms)
            this.setSpecificSkills(program.specificSkills)
            this.setEcosocialLearnings(program.ecosocialLearnings)
            this.setLearningUnits(program.learningUnits)
            this.setCurrentStep(program.currentStep)
            this.setLastSavedProgram(program.lastSavedProgram)
        },
        resetProgram(): void {
            this.resetCourse()
            this.specificSkills = []
            this.basicWisdoms = []
            this.ecosocialLearnings = []
            this.learningUnits = []
            this.currentStep = 1
            this.lastSavedProgram = ''             
        },
        reset(): void {
            this.id = -1
            this.resetCourse()
            this.specificSkills = []
            this.basicWisdoms = []
            this.ecosocialLearnings = []
            this.learningUnits = []
            this.currentStep = 1
            this.lastSavedProgram = ''             
        }
    }
});