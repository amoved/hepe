import { Prisma, PrismaClient, type Status, type Role, type AcademicYear, type AcademicCycle, EcosocialLearningGroup, type Locale, type BasicWisdomItem } from '@prisma/client'
import bcrypt from 'bcryptjs'
import fs from 'fs'
import { parse } from 'csv-parse'
// import { stringToEcosocialLearningGroup } from '~/utils/ecosocialLearnings';

const prisma = new PrismaClient()

// let itemsCreatedCounter = 0
// let itemsAlreadyCreatedCounter = 0

const admin: Role = "ADMIN"
const user: Role = "USER"
const status: Status = "LOGGED_OUT"

// From positions 0-10 -> Infantil, 11-27 -> Primaria, 28-53 Secundaria
const globalAcademicYears: Array<string> = [
    "1º ciclo Inf",
    "2º ciclo Inf",
    "1º curso 2º ciclo Inf",
    "2º curso 2º ciclo Inf",
    "3º curso 2º ciclo Inf",
    "1r cicle Inf",
    "2n cicle Inf",
    "Haurren lehen zikloa",
    "Haurren bigarren zikloa",
    "1º EPO",
    "2º EPO",
    "3º EPO",
    "4º EPO",
    "5º EPO",
    "6º EPO",
    "1º ciclo EPO",
    "2º ciclo EPO",
    "3º ciclo EPO",
    "1r cicle EPO",
    "2n cicle EPO",
    "3r cicle EPO",
    "DBK lehen zikloa",
    "DBK bigarren zikloa",
    "DBK hirugarren zikloa",
    "1º ESO",
    "2º ESO",
    "3º ESO",
    "4º ESO",
    "1º y 2º ESO",
    "1º a 3º ESO",
    "1º y 3º ESO",
    "2º y 3º ESO",
    "3º y 4º ESO",
    "1r ESO",
    "2n ESO",
    "3r ESO",
    "4t ESO",
    "1r i 2n ESO",
    "1r a 3r ESO",
    "1r i 3r ESO",
    "2n i 3r ESO",
    "3r i 4t ESO",
    "DBHko 1",
    "DBHko 2",
    "DBHko 3",
    "DBHko 4",
    "DBHKo 1 eta 2",
    "DBHKo 3 eta 4"
]

async function createRecord(entity: string, data: object): Promise<Program | false> {
    try {
        const record = await prisma[entity].create({ data })
        return record
    } catch(e) {
        console.log(e)
    }
    return false
}

async function updateRecord(entity: string, where: object, data: object): Promise<Program | false> {
    try {
        const record = await prisma[entity].update({ where, data })
        return record
    } catch(e) {
        if(e instanceof Prisma.PrismaClientKnownRequestError){
            // Handle e-mail exists
            if(e.code === 'P2002') {
                console.log("Tried to register with an already existing email.")
                // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
            }
        } else {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                console.log(e.message)
            } else {
                console.log('Unhandled exception error at "register" action.')
            }
        }
        
    }
    return false
}

async function findAllRecords(entity: string, include: object = {}): Promise<Array<Course | Region | Locale> | false> {
    try {
        if(include) {
            const records = await prisma[entity].findMany({include})
            return records
        } else {
            const records = await prisma[entity].findMany()
            return records
        }
        
    } catch(e) {
        if(e instanceof Prisma.PrismaClientKnownRequestError){
            // Handle e-mail exists
            if(e.code === 'P2002') {
                console.log("Tried to register with an already existing email.")
                // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
            }
        } else {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                console.log(e.message)
            } else {
                console.log('Unhandled exception error at "register" action.')
            }
        }
    }
    return false
}

async function findRecords(entity: string, where: object, include: object = {}): Promise<Array<Course | Region | Locale | BasicWisdomItem | EvaluationCriteria | EcosocialLearning> | false> {
    try {
        const records = await prisma[entity].findMany({ where, include })
        return records
    } catch(e) {
        if(e instanceof Prisma.PrismaClientKnownRequestError){
            // Handle e-mail exists
            if(e.code === 'P2002') {
                console.log("Tried to register with an already existing email.")
                // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
            }
        } else {
            // Unknown error
            if(e instanceof Object && Object.hasOwn(e, 'message')){
                console.log(e.message)
            } else {
                console.log('Unhandled exception error at "register" action.')
            }
        }
        
    }
    return false
}

async function findRecord(entity: string, where: object, include: object = {}): Promise<Course | Region | Locale | BasicWisdomItem | EvaluationCriteria | EcosocialLearning | false> {
    try {
        const record = await prisma[entity].findUnique({ where, include })
        return record
    } catch(e) {
        try {
            const record = await prisma[entity].findFirst({ where, include })
            return record
        } catch(e) {
            if(e instanceof Prisma.PrismaClientKnownRequestError){
                // Handle e-mail exists
                if(e.code === 'P2002') {
                    console.log("Tried to register with an already existing field.")
                    // return new Log(LogType.INFO, $t("register.recover"), ErrorCodes.registerEmail)
                }
            } else {
                // Unknown error
                if(e instanceof Object && Object.hasOwn(e, 'message')){
                    console.log(e.message)
                } else {
                    console.log('Unhandled exception error.')
                }
            }
            
        }
        return false
    }
}

async function createLocale(name: string) {
    // Check if locale exists
    // console.log("Check if locale exists: ", name)
    const locale: Locale = await findRecord('locale',{
        name
    })
    // console.log("locale: ", locale)
    // If it doesn't exists create it
    if(!locale){
        // console.log("Create new locale")
        const newLocale: Locale = await createRecord('locale',{
            name
        })
        // console.log("new locale: ", newLocale)
    }
}

async function createRegion(name: string,localeName: string) {
    // console.log("Check if region exists: ", name)
    const region: Region = await findRecord('region',{
        name
    },{
        locales: true
    })
    // console.log("region: ", region)
    // If it doesn't exists create it
    if(!region){
        // console.log("Create new region")
        const newRegion: Region = await createRecord('region',{
            name,
            locales: {
                connect: [ 
                    {
                        name: localeName
                    }
                ]
            }
        })
        // console.log("New region: ", newRegion)
    // If not connected, connect it
    } else if(region.locales.findIndex(l => l.name == localeName) == -1){
        // console.log("Update region")
        const updatedRegion: Region = await updateRecord('region',{
            id: region.id,
        },
        {
            locales: {
                connect: [ 
                    {
                        name: localeName
                    }
                ]
            }
        })
        // console.log("Updated region", updatedRegion)
    }
}

async function createAcademicYear(name: string) {
    // console.log("Check if academic year exists: ", name)
    const academicYear: Course = await findRecord('academicYear',{
        name,
    },{
        courses: true,
    })
    // console.log("academicYear: ", academicYear)
    // If it doesn't exists create it
    if(!academicYear){
        // console.log("Create new academicYear")
        const newAcademicYear: AcademicYear = await createRecord('academicYear',{
            name,
        })
        // console.log("New academicYear: ", newAcademicYear)
    }
}

async function createCourse(name: string,localeName: string,regionName: string, academicYearName: string): Promise<Course> {
    // console.log("Check if course exists: ", name)
    const course: Course = await findRecord('course',{
        name: name,
        academicYear: {
            name: academicYearName,
        },
        region: {
            name: regionName
        },
        locale: {
            name: localeName
        }
    },{
        locale: true,
        region: true
    })
    // console.log("course: ", course)
    // If it doesn't exists create it
    if(!course){
        // console.log("Create new course")
        const newCourse: Course = await createRecord('course',{
            name: name,
            academicYear: {
                connect: {
                    name: academicYearName
                }
            },
            locale: {
                connect: {
                    name: localeName
                }
            },
            region: {
                connect: {
                    name: regionName
                }
            }
        })
        // console.log("New course: ", newCourse)
        return newCourse
    }
    return course
}

async function createBasicWisdomItem(content: string,courseId: string,parentId: number): Promise<BasicWisdomItem> {
    // console.log("Check if basic wisdom item exists: ", content)
    const basicWisdomItem: BasicWisdomItem = await findRecord('basicWisdomItem',{
        content,
        course: {
            id: courseId
        },
        parentId: parentId
    })
    // console.log("basicWisdomItem: ", basicWisdomItem)
    // If it doesn't exists create it
    if(!basicWisdomItem){
        // console.log("Create new basicWisdomItem")
        const newBasicWisdomItem: BasicWisdomItem = await createRecord('basicWisdomItem',{
            content,
            course: {
                connect: {
                    id: courseId
                },
            },
            parentId: parentId
        })
        // console.log("New basic wisdom item: ", newBasicWisdomItem)
        // itemsCreatedCounter++
        return newBasicWisdomItem
    }
    // } else {
    //     console.log("bW alredy exists", basicWisdomItem)
    //     console.log("content: ", content)
    //     itemsAlreadyCreatedCounter++
    // }
    return basicWisdomItem
}

async function createSpecificSkill(content: string,courseId: string): Promise<SpecificSkill> {
    // console.log("Check if specific skill  exists: ", content)
    const specificSkill: SpecificSkill = await findRecord('specificSkill',{
        content,
        course: {
            id: courseId
        }
    })
    // console.log("specificSkill: ", specificSkill)
    // If it doesn't exists create it
    if(!specificSkill){
        // console.log("Create new specificSkill")
        const newSpecificSkill: SpecificSkill = await createRecord('specificSkill',{
            content,
            course: {
                connect: {
                    id: courseId
                },
            }
        })
        // console.log("New specifik skill: ", newSpecificSkill)
        return newSpecificSkill
    }
    return specificSkill
}

async function createEvaluationCriteria(content: string,specificSkillId: string): Promise<EvaluationCriteria> {
    // console.log("Check if evaluation criteria exists: ", content)
    const evaluationCriteria: EvaluationCriteria = await findRecord('evaluationCriteria',{
        content,
        specificSkill: {
            id: specificSkillId
        }
    })
    // console.log("evaluationCriteria: ", evaluationCriteria)
    // If it doesn't exists create it
    if(!evaluationCriteria){
        // console.log("Create new evaluationCriteria")
        const newEvaluationCriteria: EvaluationCriteria = await createRecord('evaluationCriteria',{
            content,
            specificSkill: {
                connect: {
                    id: specificSkillId
                },
            }
        })
        // console.log("New evaluation criteria: ", newEvaluationCriteria)
        return newEvaluationCriteria
    }
    return evaluationCriteria
}

interface BasicWisdomItemRow {
    'content-lvl5': string
    'content-lvl4': string
    'content-lvl3': string
    'content-lvl2': string
    'content-lvl1': string
    academicYear: string
    courseName: string
    regionName: string
    localeName: string
}

const dataBWs: Array<BasicWisdomItemRow> = [] 

const loadBWs = () => {
    fs.createReadStream("./prisma/hepe-data-bWs_20240716.csv")
    .pipe(parse({columns:true}))
    .on("data", function (row) {
        dataBWs.push(row)
    })
    .on("end", async () => {
        console.log("Start loading bWs")
        //  Create academic years
        for( let i=0; i<globalAcademicYears.length; i++) {
            // console.log("Create:", globalAcademicYears[i])
            await createAcademicYear(globalAcademicYears[i])
        }
        for( let i=0; i<dataBWs.length; i++) {
            // Get or create locale/region/academicYear
            await createLocale(dataBWs[i].localeName)
            // Again with region
            await createRegion(dataBWs[i].regionName,dataBWs[i].localeName)
            // Again with the academic year
            await createAcademicYear(dataBWs[i].academicYear)
            // Again with course
            const course = await createCourse(dataBWs[i].courseName,dataBWs[i].localeName,dataBWs[i].regionName,dataBWs[i].academicYear)
            // Again with bW lvl1
            const basicWisdomItemLvl1 = await createBasicWisdomItem(dataBWs[i]["content-lvl1"],course.id,-1)
            // Again with bW lvl2
            if(basicWisdomItemLvl1 && dataBWs[i]["content-lvl2"] != ""){
                const basicWisdomItemLvl2 = await createBasicWisdomItem(dataBWs[i]["content-lvl2"],course.id,basicWisdomItemLvl1.id)
                // Again with bW lvl3
                if(basicWisdomItemLvl2 && dataBWs[i]["content-lvl3"] != ""){
                    const basicWisdomItemLvl3 = await createBasicWisdomItem(dataBWs[i]["content-lvl3"],course.id,basicWisdomItemLvl2.id)
                    // Again with bW lvl4
                    if(basicWisdomItemLvl3 && dataBWs[i]["content-lvl4"] != ""){
                        const basicWisdomItemLvl4 = await createBasicWisdomItem(dataBWs[i]["content-lvl4"],course.id,basicWisdomItemLvl3.id)
                        // Again with bW lvl5
                        if(basicWisdomItemLvl4 && dataBWs[i]["content-lvl5"] != ""){
                            const basicWisdomItemLvl5 = await createBasicWisdomItem(dataBWs[i]["content-lvl5"],course.id,basicWisdomItemLvl4.id)
                        }
                    }
                }
            }
        }
        // console.log("The number of bWs items", itemsCounter)
        loadEvCrs()

    })
} 

interface EvaluationCriteriaRow {
    content: string
    specificSkillContent: string
    academicYear: string
    courseName: string
    regionName: string
    localeName: string
}

const dataEvCrs: Array<EvaluationCriteriaRow> = [] 

const loadEvCrs = () => {
    fs.createReadStream("./prisma/hepe-data-evCrs_20240716_2.csv")
    .pipe(parse({columns:true}))
    .on("data", function (row) {
        dataEvCrs.push(row)
    })
    .on("end", async () => {
        console.log("Start loading evCrs", dataEvCrs.length)
        for( let i=0; i<dataEvCrs.length; i++) {
            // Get or create locale/region/academicYear
            await createLocale(dataEvCrs[i].localeName)
            // Again with region
            await createRegion(dataEvCrs[i].regionName,dataEvCrs[i].localeName)
            // Again with the academic year
            await createAcademicYear(dataEvCrs[i].academicYear)
            // Again with course
            const course = await createCourse(dataEvCrs[i].courseName,dataEvCrs[i].localeName,dataEvCrs[i].regionName,dataEvCrs[i].academicYear)
            // Again with specificSkill
            const specificSkill = await createSpecificSkill(dataEvCrs[i]["specificSkillContent"],course.id)
            // Again with evCr
            const evaluationCriteria = await createEvaluationCriteria(dataEvCrs[i]["content"],specificSkill.id)
            if(!evaluationCriteria){
                console.log("Failed creating eEvCr", dataEvCrs[i])
            }
        }
        // Check whats failing
        let loadedEvCrs: Array<object> = await findAllRecords('evaluationCriteria')
        loadedEvCrs = loadedEvCrs.map(l => l.content)
        let failedEvCrs = []
        dataEvCrs.forEach((data,i) => {
            if(!loadedEvCrs.includes(data.content)){
                failedEvCrs.push(data)
            } else {
                const i = loadedEvCrs.indexOf(data.content)
                loadedEvCrs.splice(i,1)
            }
        })
        console.log("Failed or repeated loads: ", failedEvCrs)
        loadEBWs()
    })
} 


/**
 * Maps the dataset academic cycle to an array of academicYear's in order to create the proper DB relations
 *
 * @param {string} academicCycle
 * @return {*}  {Array<object>}
 */
function academicCycleToAcademicYears(academicCycle:string): Array<object> {
    
    const filteredAcademicYears: Array<object> = []
    let cycleEndIndex = 0
    let cycleStartIndex = 0

    switch(academicCycle){
        case 'Infantil':
            // Get all years from this cycle
            cycleStartIndex = 0
            cycleEndIndex = 4
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Infantil-cat':
            // Get all years from this cycle
            cycleStartIndex = 5
            cycleEndIndex = 6
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Haur Hezkuntza':
            // Get all years from this cycle
            cycleStartIndex = 7
            cycleEndIndex = 8
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Primaria':
            // Get all years from this cycle
            cycleStartIndex = 9
            cycleEndIndex = 17
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Primària':
            // Get all years from this cycle
            cycleStartIndex = 18
            cycleEndIndex = 20
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Lehen Hezkuntza':
            // Get all years from this cycle
            cycleStartIndex = 21
            cycleEndIndex = 23
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Secundaria':
            // Get all years from this cycle
            cycleStartIndex = 24
            cycleEndIndex = 32
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Secundària':
            // Get all years from this cycle
            cycleStartIndex = 33
            cycleEndIndex = 41
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        case 'Bigarren Hezkuntza':
            // Get all years from this cycle
            cycleStartIndex = 42
            cycleEndIndex = 47
            for(let i = cycleStartIndex; i <= cycleEndIndex; i++){
                filteredAcademicYears.push({name: globalAcademicYears[i]})
            }
            return filteredAcademicYears

        default: 
            console.log("The provided academic cycle is not registered")
    }

    console.log(`Academic cycle not found: ${academicCycle}`)
    throw("Seeding error: Academic cycle not found")
}


/**
 * Maps the dataset string value of the EL group to an EcosocialLearningGroup value.
 *
 * @param {string} group
 * @return {*}  {EcosocialLearningGroup}
 */
function stringToEcosocialLearningGroup(group: string): EcosocialLearningGroup {
    switch(group) {
        case 'Ecodependencia':
        case 'Ecodependència':
        case 'Ekomendekotasuna':
            return EcosocialLearningGroup.ECODEPENDENCIA
        case 'Funcionamiento de la biosfera':    
        case 'Funcionament de la biosfera':    
        case 'Biosferaren funtzionamendua':    
            return EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA
        case 'Crisis civilizatoria':    
        case 'Crisi de civilització':    
        case 'Zibilizazio-krisia':    
            return EcosocialLearningGroup.CRISIS_CIVILIZATORIA
        case 'Agentes de cambio social':    
        case 'Agents de canvi ecosocial':    
        case 'Aldaketa ekosozialaren eragileak':    
            return EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL
        case 'Desarrollo personal y comunitario':    
        case 'Desenvolupament personal i comunitari':    
        case 'Garapen pertsonala eta komunitarioa':    
            return EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO
        case 'Justicia':    
        case 'Justícia':    
        case 'Justizia':    
            return EcosocialLearningGroup.JUSTICIA
        case 'Democracia':    
        case 'Democràcia':    
        case 'Demokrazia':    
            return EcosocialLearningGroup.DEMOCRACIA
        case 'Técnicas ecosociales':    
        case 'Tècniques ecosocials':    
        case 'Teknika ekosozialak':    
            return EcosocialLearningGroup.TECNICAS_ECOSOCIALES
    }
    console.log(`Group not found: ${group}`)
    throw("Seeding error: Group not found")
}

async function getEcosocialLearning(content: string,group: string,localeName:string): Promise<EcosocialLearning> {
    const ecosocialLearning: EcosocialLearning = await findRecord('ecosocialLearning',{
        content,
        locale: {
            name: localeName
        },
        group: stringToEcosocialLearningGroup(group)
    })
    return ecosocialLearning
}

async function createEcosocialLearning(content: string,group: string,localeName:string): Promise<EcosocialLearning> {
    // console.log("Check if ecosocial learning exists: ", content)
    const ecosocialLearning: EcosocialLearning = await findRecord('ecosocialLearning',{
        content,
        locale: {
            name: localeName
        },
        group: stringToEcosocialLearningGroup(group)
    })
    // console.log("ecosocialLearning: ", ecosocialLearning)
    // If it doesn't exists create it
    if(!ecosocialLearning){
        // console.log("Create new ecosocialLearning")
        const newEcosocialLearning: EcosocialLearning = await createRecord('ecosocialLearning',{
            content,
            locale: {
                connect: {
                    name: localeName
                },
            },
            group: stringToEcosocialLearningGroup(group)
        })
        // console.log("New ecosocial learning: ", newEcosocialLearning)
        return newEcosocialLearning
    }
    return ecosocialLearning
}

async function getEcosocialBasicWisdom(content: string,ecosocialLearningId: string, academicYears: Array<object>): Promise<EcosocialBasicWisdom> {
    // console.log("Check if ecosocial basic wisdom exists: ", content)
    return await findRecord('ecosocialBasicWisdom',{
        content,
        ecosocialLearning: {
            id: ecosocialLearningId
        },
        academicYears: {
            some: {
                name: {
                    in: academicYears.map(a => a.name)
                }
            }
        }
    },
    {
        academicYears: true
        
    })
}

async function createEcosocialBasicWisdom(content: string,ecosocialLearningId: string,academicYears: Array<object>): Promise<EcosocialBasicWisdom> {
    const ecosocialBasicWisdom: EcosocialBasicWisdom = await getEcosocialBasicWisdom(content, ecosocialLearningId, academicYears)
    // console.log("ecosocialBasicWisdom? ", ecosocialBasicWisdom)
    // If it doesn't exists create it
    if(!ecosocialBasicWisdom){
        // console.log("Create new ecosocialBasicWisdom")
        const newEcosocialBasicWisdom: EcosocialBasicWisdom = await createRecord('ecosocialBasicWisdom',{
            content,
            ecosocialLearning: {
                connect: {
                    id: ecosocialLearningId
                },
            },
            academicYears: {
                connect: academicYears
            }
        })
        // console.log("New ecosocial basic wisdom: ", newEcosocialBasicWisdom)
        return newEcosocialBasicWisdom
    } else if(checkAcademicYears(ecosocialBasicWisdom.academicYears,academicYears)){
        // console.log("Update ecosocialBasicWisdom")
        const updatedEcosocialBasicWisdom: EcosocialEvaluationCriteria = await updateRecord('ecosocialBasicWisdom',{
            id: ecosocialBasicWisdom.id,
        },{
            academicYears: {
                connect: academicYears
            }
        })
        // console.log("Updated ecosocial evaluation criteria: ", updatedEcosocialBasicWisdom)
        return updatedEcosocialBasicWisdom
    }
    return ecosocialBasicWisdom
}

function checkAcademicYears(recordAcademicYears: Array<object>,academicYears: Array<object>): boolean {
    let recordNames: Array<string> = []
    let isIncludedArr: Array<boolean> = []
    // Get only the names
    recordAcademicYears.forEach((r,i) => {
        recordNames.push(r.name)
    })
    // Check for each academic year
    academicYears.forEach(a => {
        isIncludedArr.push(recordNames.includes(a.name))
    })
    // If one or more are not included return false
    return !isIncludedArr.includes(false)
}

async function createEcosocialEvaluationCriteria(content: string,ecosocialLearningId: string,academicYears: Array<object>): Promise<EcosocialEvaluationCriteria> {
    // console.log("Check if ecosocial evaluation criteria exists: ", content)
    let academicYearsFilter: Array<string> = []
    academicYears.forEach(a => academicYearsFilter.push(a.name))
    const ecosocialEvaluationCriteria: EcosocialEvaluationCriteria = await findRecord('ecosocialEvaluationCriteria',{
        content,
        ecosocialLearning: {
            id: ecosocialLearningId
        },
        academicYears: {
            every: {
                name: {
                    in: academicYearsFilter
                }
            }
        }
    },
    {
        academicYears: true
    })
    // console.log("ecosocialEvaluationCriteria? ", ecosocialEvaluationCriteria)
    // If it doesn't exists create it
    if(!ecosocialEvaluationCriteria){
        // console.log("Create new ecosocialEvaluationCriteria")
        const newEcosocialEvaluationCriteria: EcosocialEvaluationCriteria = await createRecord('ecosocialEvaluationCriteria',{
            content,
            ecosocialLearning: {
                connect: {
                    id: ecosocialLearningId
                },
            },
            academicYears: {
                connect: academicYears
            }
        })
        // console.log("New ecosocial evaluation criteria: ", newEcosocialEvaluationCriteria)
        return newEcosocialEvaluationCriteria
    // If exists but the academic years doesn't contain current cycle, update it
    } else if(!checkAcademicYears(ecosocialEvaluationCriteria.academicYears,academicYears)){
        // console.log("Update ecosocialEvaluationCriteria")
        const updatedEcosocialEvaluationCriteria: EcosocialEvaluationCriteria = await updateRecord('ecosocialEvaluationCriteria',{
            id: ecosocialEvaluationCriteria.id,
        },{
            academicYears: {
                connect: academicYears
            }
        })
        // console.log("Updated ecosocial evaluation criteria: ", updatedEcosocialEvaluationCriteria)
        return updatedEcosocialEvaluationCriteria
    }
    return ecosocialEvaluationCriteria
}

interface EcosocialBasicWisdomRow {
    content: string
    ecosocialLearning: string
    ecosocialLearningGroup: string
    academicCycle: string
    localeName: string
}

const dataEBWs: Array<EcosocialBasicWisdomRow> = [] 

const loadEBWs = () => {
    // console.log("--------------------- eBWs ---------------------")

    fs.createReadStream("./prisma/hepe-data-eBWs_20240710_2.csv")
    .pipe(parse({columns:true}))
    .on("data", function (row) {
        dataEBWs.push(row)
    })
    .on("end", async () => {
        console.log("Start loading eBWs: ", dataEBWs.length)
        for( let i=0; i<dataEBWs.length; i++) {
            // console.log("Check if ecosocial basic wisdom exists: ", content)
            // Get or create ecosocial learning
            try {
                const ecosocialLearning: EcosocialLearning = await createEcosocialLearning(dataEBWs[i].ecosocialLearning,dataEBWs[i].ecosocialLearningGroup,dataEBWs[i].localeName) 
            
                // Mapa academic cycle to academic years
                const academicYears = academicCycleToAcademicYears(dataEBWs[i].academicCycle)
                // console.log("Resulting academic years array: ", academicYears)
                const ecosocialBasicWisdom: EcosocialBasicWisdom = await getEcosocialBasicWisdom(dataEBWs[i].content, ecosocialLearning.id, academicYears)
                if(!ecosocialBasicWisdom) {
                    // console.log("eBW not found, creating...", dataEBWs[i])
                    try {
                        // console.log("---------------------------")
                        const ecosocialBasicWisdom = await createEcosocialBasicWisdom(dataEBWs[i].content,ecosocialLearning.id,academicYears)
                        if(!ecosocialBasicWisdom){
                            console.log("Failed creating eEvCr", dataEBWs[i])
                            console.log("Resulting academic years array: ", academicYears)
            
                        }
                    } catch(e) {
                        console.log("Catch error, failed creating eBW", dataEBWs[i])
                        console.log(e)
                    }    
                }
            } catch(e) {
                console.log(e)
                console.log(dataBWs[i])
            }
            // console.log("Created basic wisdom: ", ecosocialBasicWisdom)
        }
        // // Check whats failing
        // let loadedEBWs: Array<object> = await findAllRecords('ecosocialBasicWisdom')
        // loadedEBWs = loadedEBWs.map(l => l.content)
        // let failedEBWs = []
        // dataEBWs.forEach(data => {
        //     if(!loadedEBWs.includes(data.content)){
        //         failedEBWs.push(data)
        //     } else {
        //         const i = loadedEBWs.indexOf(data.content)
        //         loadedEBWs.splice(i,1)
        //     }
        // })
        // console.log("Failed or repeated loads: ", failedEBWs)
        loadEEvCrs()
    })
} 

interface EcosocialEvaluationCriteriaRow {
    content: string
    ecosocialLearning: string
    ecosocialLearningGroup: string
    academicCycle: string
    localeName: string
}

const dataEEvCrs: Array<EcosocialEvaluationCriteriaRow> = [] 

const loadEEvCrs = () => {
    // console.log("----------------- eEvCrs -------------------")
    fs.createReadStream("./prisma/hepe-data-eEvCrs_20240709.csv")
    .pipe(parse({columns:true}))
    .on("data", function (row) {
        dataEEvCrs.push(row)
    })
    .on("end", async () => {
        console.log("Start loading eEvCrs")
        for( let i=0; i<dataEEvCrs.length; i++) {
            
            try{

                // console.log("---------------------------")
                // console.log("Next eEvCr: ", dataEEvCrs[i])
                // Get or create ecosocial learning
                // const ecosocialLearning: EcosocialLearning = await createEcosocialLearning(dataEEvCrs[i].ecosocialLearning,dataEEvCrs[i].ecosocialLearningGroup,dataEEvCrs[i].localeName) 
                try {
                    const ecosocialLearning: EcosocialLearning = await getEcosocialLearning(dataEEvCrs[i].ecosocialLearning,dataEEvCrs[i].ecosocialLearningGroup,dataEEvCrs[i].localeName) 
                    // Mapa academic cycle to academic years
                    const academicYears = academicCycleToAcademicYears(dataEEvCrs[i].academicCycle)
                    // console.log("Resulting academic years array: ", academicYears)
                    // console.log("Ecosocial learning: ", ecosocialLearning)
                    const ecosocialEvaluationCriteria = await createEcosocialEvaluationCriteria(dataEEvCrs[i].content,ecosocialLearning.id,academicYears)
                    if(!ecosocialEvaluationCriteria){
                        console.log("Failed creating eEvCr", dataEEvCrs[i])
                        console.log("Resulting academic years array: ", academicYears)
                        
                    }
                } catch(e) {
                    console.log(e)
                    console.log(dataEEvCrs[i])
                }
            } catch(e) {
                console.log("Catch error, failed creating eEvCr", dataEEvCrs[i])
                console.log(e)
            }
            
            // console.log("Created eEvCr: ", ecosocialEvaluationCriteria)
        }
        // // Check whats failing
        // let loadedEEvCrs: Array<object> = await findAllRecords('ecosocialEvaluationCriteria')
        // loadedEEvCrs = loadedEEvCrs.map(l => l.content)
        // let failedEEvCrs = []
        // dataEEvCrs.forEach(data => {
        //     if(!loadedEEvCrs.includes(data.content)){
        //         failedEEvCrs.push(data)
        //     } else {
        //         const i = loadedEEvCrs.indexOf(data.content)
        //         loadedEEvCrs.splice(i,1)
        //     }
        // })
        // console.log(dataEEvCrs.length)
        // console.log("Failed or repeated loads: ", failedEEvCrs)

    })
    console.log("----------------- eEvCrs -------------------")

} 

// Licenses and users
const licenseData = [
  {
    name: 'Admin',
    code: 'MIMI-100',
    numUsers: 1,
    maxUsers: 1,
    usageLimit: 1,
    users: {
      create: {
        name: 'Luis',
        email: 'luis@amoved.es',
        passwordHash: '12345678',
        role: admin,
        verified: true
      }
    }
  },
  {
    name: 'Colegio Público de Tetuán',
    code: 'TET302-3',
    numUsers: 1,
    maxUsers: 10,
    usageLimit: 3,
    users: {
      create: {
        name: 'Carlos',
        email: 'carlos@amoved.es',
        passwordHash: '12345678',
        role: user,
        verified: true,
      }
    }
  },
  {
    name: 'Colegio Público de Vallecas',
    code: 'VAL001-5',
    numUsers: 1,
    maxUsers: 15,
    usageLimit: 3,
    users: {
      create: {
        name: 'Elena',
        email: 'elena@amoved.es',
        passwordHash: '12345678',
        role: user,
        verified: true,
      }
    }
  }
]

async function loadLicensesAndUsers(){
    for (const l of licenseData) {
        let salt = await bcrypt.genSalt(10)
        l.users.create.passwordHash = await bcrypt.hash(l.users.create.passwordHash,salt)
        const user = await prisma.license.create({
          data: l,
        })
        console.log(`Created user with id: ${user.id}`)
    }
    loadBWs()

}

async function main() {
    console.log(`Loading data ...`)
    
    // This will bootstrap all the loading syncronously   
    await loadLicensesAndUsers()
    // loadBWs()
    // loadEvCrs()

    console.log(`Load finished.`)

    // loadEBWs()
    // loadEEvCrs()

}

main()
.then(async () => {
    await prisma.$disconnect()
})
.catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
})
