import { BasicWisdom } from './../ddd/education/domain/basicWisdom';
import { PrismaClient, type Status, type Role, type AcademicYear, type AcademicCycle, type EcosocialLearningGroup } from '@prisma/client'
import bcrypt from 'bcryptjs'

const prisma = new PrismaClient()

const admin: Role = "ADMIN"
const user: Role = "USER"
const status: Status = "LOGGED_OUT"
const primaria1: AcademicYear = "PRIMARIA_1"
const primaria: AcademicCycle = "PRIMARIA"
const secundaria2: AcademicYear = "SECUNDARIA_2"
const secundaria: AcademicCycle = "SECUNDARIA"
const ecodependencia: EcosocialLearningGroup = "ECODEPENDENCIA"
const funcionamientoDeLaBiosfera: EcosocialLearningGroup = "FUNCIONAMIENTO_DE_LA_BIOSFERA"

const licenseData = [
  {
    name: 'Admin',
    code: 'MIMI-100',
    numUsers: 1,
    maxUsers: 1,
    usageLimit: 1,
    users: {
      create: {
        name: 'Luis',
        email: 'luis@amoved.es',
        passwordHash: '12345678',
        role: admin,
        verified: true
      }
    }
  },
  {
    name: 'Colegio Público de Tetuán',
    code: 'TET302-3',
    numUsers: 1,
    maxUsers: 10,
    usageLimit: 3,
    users: {
      create: {
        name: 'Carlos',
        email: 'carlos@amoved.es',
        passwordHash: '12345678',
        role: user,
        verified: true,
      }
    }
  },
  {
    name: 'Colegio Público de Vallecas',
    code: 'VAL001-5',
    numUsers: 1,
    maxUsers: 15,
    usageLimit: 3,
    users: {
      create: {
        name: 'Elena',
        email: 'elena@amoved.es',
        passwordHash: '12345678',
        role: user,
        verified: true,
      }
    }
  }
]

const localeData = [
  {
    name: "Castellano",
    abbreviation: "cas",
    regions: {
      create: [
        {
          name: "Comunidad de Madrid",
        },
        {
          name: "País Valenciá",
        },
        {
          name: "País Vasco",
        }
      ]
    }
  },
  {
    name: "Catalán",
    abbreviation: "cat",
    regions: {
      connect: [
        {
          name: "País Valenciá"
        }
      ] 
    }
  },
  {
    name: "Euskera",
    abbreviation: "eus",
    regions: {
      connect: [
        {
          name: "País Vasco"
        }
      ] 
    }
  }
]

const courseData = [
  {
    name: "Física y Química",
    academicYear: secundaria2,
    academicCycle: secundaria,
    region: {
      connect: {
        name: "Comunidad de Madrid"
      }
    },
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    specificSkills: {
      create:  [
        {
          content: "CE1. Comprender y relacionar los motivos por los que ocurren los principales fenómenos fisicoquímicos del entorno, explicándolos en términos de las leyes y teorías científicas adecuadas para resolver problemas con el fin de aplicarlas para mejorar la calidad de vida humana.",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv1.1 Identificar los fenómenos fisicoquímicos cotidianos más relevantes utilizando la terminología científica adecuada."
              },
              {
                content: "CrEv1.2 Reconocer y describir de forma guiada situaciones problemáticas reales de índole científica en el entorno inmediato planteando posibles iniciativas en las que la ciencia, y en particular la física y la química, pueden contribuir a su solución."
              }
            ]
          }
        },
        {
          content: "CE2. Expresar las observaciones realizadas por el alumnado en forma de preguntas, formulando hipótesis para explicarlas y demostrando dichas hipótesis a través de la experimentación científica, la indagación y la búsqueda de evidencias, para desarrollar los razonamientos propios del pensamiento científico y mejorar las destrezas en el uso de las metodologías científicas.",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv2.1 Conocer las metodologías propias de la ciencia para identificar y describir fenómenos a partir de cuestiones a las que se pueda dar respuesta a través de la indagación, la deducción, el trabajo experimental y el razonamiento lógico-matemático, diferenciándolas de aquellas pseudocientíficas que no admiten comprobación experimental."
              },
              {
                content: "CrEv2.2 Seleccionar, de acuerdo con la naturaleza de las cuestiones que se traten, la mejor manera de comprobar o refutar las hipótesis formuladas, diseñando estrategias de indagación y búsqueda de evidencias de forma guiada, que permitan obtener conclusiones y respuestas ajustadas a la naturaleza de la pregunta formulada."
              }
            ]
          }
        }
      ]
    },
    // basicWisdoms: {
    //   create: [
    //     {
    //       content: "A. Las destrezas científicas básicas",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Aproximación a las metodologías de la investigación científica: identificación y formulación de cuestiones, elaboración de hipótesis y comprobación experimental de las mismas."
    //           },
    //           {
    //             content: "Introducción a los entornos y recursos de aprendizaje científico: el laboratorio y los entornos virtuales."
    //           },
    //           {
    //             content: "Iniciación al trabajo experimental mediante la realización de proyectos de investigación sencillos y de forma guiada."
    //           },
    //           {
    //             content: "Uso del lenguaje científico en la expresión de los resultados de un proyecto de investigación: unidades del Sistema Internacional y sus símbolos."
    //           },
    //           {
    //             content: "Valoración de la cultura científica y del papel de científicos en los principales hitos históricos y actuales de la física y la química."
    //           }
    //         ]
    //       }
    //     },
    //     {
    //       content: "B. La materia",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Aplicación de la teoría cinético-molecular a observaciones sobre la materia explicando sus propiedades, estados de agregación y la formación de mezclas y disoluciones."
    //           },
    //           {
    //             content: "Realización de experimentos sencillos y de forma guiada relacionados con los sistemas materiales para conocer y describir sus propiedades, su composición y su clasificación."
    //           },
    //           {
    //             content: "Estructura atómica: presentación del desarrollo histórico de los modelos atómicos y la ordenación de los elementos de la tabla periódica y su importancia para entender las uniones entre los átomos."
    //           }
    //         ]
    //       }
    //     }
    //   ]
    // }
  },
  {
    name: "Física y Química",
    academicYear: secundaria2,
    academicCycle: secundaria,
    region: {
      connect: {
        name: "País Valenciá"
      }
    },
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    specificSkills: {
      create: [
        {
          content: "CE1 Resolver problemas científicos abordables en el ámbito escolar a partir de trabajos de investigación de carácter experimental.",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv 1.1 Analizar y resolver problemas asociados a la medida de sólidos irregulares"
              },
              {
                content: "CrEv 1.2 Averiguar mediante diseños experimentales la influencia de factores como la temperatura o la concentración en la velocidad de las reacciones químicas"
              }
            ]
          }
        },
        {
          content: "CE2. Analizar y resolver situaciones problemáticas del ámbito de la Física y la Química utilizando la lógica científica y alternando las estrategias del trabajo individual con el trabajo en equipo",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv 2.1 Analizar los enunciados de las situaciones planteadas y describir la situación a la que se pretende dar respuesta, identificando las variables que intervienen"
              },
              {
                content: "CrEv 2.2 Elegir, al resolver un determinado problema, el tipo de estrategia más adecuada, justificando adecuadamente su elección"
              }
            ]
          }
        }
      ]
    },
    // basicWisdoms: {
    //   create: [
    //     {
    //       content: "Metodología de la ciencia",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas."
    //           },
    //           {
    //             content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...)."
    //           },
    //           {
    //             content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico."
    //           },
    //           {
    //             content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos."
    //           },
    //           {
    //             content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados."
    //           },
    //           {
    //             content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso."
    //           }
    //         ]
    //       }
    //     },
    //     {
    //       content: "La materia y su medida",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen."
    //           },
    //           {
    //             content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas."
    //           },
    //           {
    //             content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido."
    //           }
    //         ]
    //       }
    //     }
    //   ]
    // }
  },
  {
    name: "Física y Química",
    academicYear: secundaria2,
    academicCycle: secundaria,
    region: {
      connect: {
        name: "País Valenciá"
      }
    },
    locale: {
      connect: {
        name: "Catalán"
      }
    },
    specificSkills: {
      create: [
        {
          content: "CE 1. Resoldre problemes científics abordables en l’àmbit escolar a partir de treballs d’investigació de caràcter experimental.",
          evaluationCriterias: {
            create: [
              {
                content: "CrAv1.1 Analitzar i resoldre problemes associats a la mesura de sòlids irregulars."
              },
              {
                content: "CrAv1.2 Esbrinar, mitjançant dissenys experimentals, la influència de factors com la temperatura o la concentració en la velocitat de les reaccions químiques."
              }
            ]
          }
        },
        {
          content: "CE 2. Analitzar i resoldre situacions problemàtiques de l’àmbit de la física i la química utilitzant la lògica científica i alternant les estratègies del treball individual amb el treball en equip.",
          evaluationCriterias: {
            create: [
              {
                content: "CrAv2.1 Analitzar els enunciats de les situacions plantejades i descriure la situació a la qual es pretén donar resposta, identificant les variables que hi intervenen."
              },
              {
                content: "CrAv2.2 Triar, en resoldre un determinat problema, el tipus d’estratègia més adequada, i justificar-ne adequadament l’elecció."
              }
            ]
          }
        }
      ]
    },
    // basicWisdoms: {
    //   create: [
    //     {
    //       content: "Metodologia de la ciència",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Contribució de les grans científiques i científics en el desenvolupament de les ciències físiques i químiques."
    //           },
    //           {
    //             content: "Estratègies d’utilització d’eines digitals per a la cerca de la informació, la col·laboració i la comunicació de processos, resultats i idees en diferents formats ( presentació, pòster, informe, gràfic...)."
    //           },
    //           {
    //             content: "Llenguatge científic i vocabulari específic de la matèria d’estudi en la comprensió d’informacions i dades, la comunicació de les pròpie s idees, la discussió raonada i l’argumentació sobre problemes de caràcter científic."
    //           },
    //           {
    //             content: "Procediments experimentals en laboratori: control de variables, presa (error en la mesura) i representació de les dades (taules i gràfics), anàlisi i interpretació d’aque stes."
    //           },
    //           {
    //             content: "Pautes del treball científic en la planificació i execució d’un projecte d’investigació en equip: identificació de preguntes i plantejament de problemes que puguen respondre’s, formulació d’hipòtesis, contrastació i posada a prova mitjançant l’experi mentació, i comunicació de resultats."
    //           },
    //           {
    //             content: "Instruments, eines i tècniques pròpies del laboratori de Física i Química. Normes de seguretat en el laboratori. Resulta imprescindible conéixer les per a accedir al laboratori amb seguretat (primer cicle), però també reforçar les en cada curs."
    //           }
    //         ]
    //       }
    //     },
    //     {
    //       content: "La materia y su medida",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Magnituds físiques. Diversitat d’unitats, significats i ocupació. Necessitat de normalització: Sistema Internacional. Canvis d’unitats: massa, longitud, superfície i volum."
    //           },
    //           {
    //             content: "Mesura de volums de líquids: provetes, pipetes i buretes."
    //           },
    //           {
    //             content: "Volum ocupat per sòlids regulars i irregulars. Mètode geomètric i per desplaçament d’aigua o un altre líquid."
    //           }
    //         ]
    //       }
    //     }
    //   ]
    // }
  },
  {
    name: "Ciencias de la Naturaleza",
    academicYear: secundaria2,
    academicCycle: secundaria,
    region: {
      connect: {
        name: "País Vasco"
      },
    },
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    specificSkills: {
      create: [
        {
          content: "CE1. Comprender y relacionar las causas por las que ocurren los principales fenómenos y procesos naturales, utilizando el razonamiento científico, las leyes y teorías científicas y/o el pensamiento computacional para resolver problemas o dar explicación a procesos de la vida cotidiana.",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv 1.1 Explicar los fenómenos naturales cotidianos más relevantes, y analizarlos en términos de los principios, teorías y leyes científicas adecuadas y expresarlos empleando la argumentación."
              },
              {
                content: "CrEv 1.2 Explicar procesos naturales representándolos mediante modelos y diagramas y utilizando, cuando sea necesario, los pasos del diseño de ingeniería (identificación del problema, exploración, diseño, creación, evaluación y mejora) a través de herramientas analógicas y digitales."
              }, 
              {
                content: "CrEv 1.3 Resolver problemas o dar explicación a procesos naturales utilizando conocimientos, datos e información aportados, el razonamiento lógico, el pensamiento computacional o recursos digitales."
              }
            ]
          }
        },
        {
          content: "CE 2. Identificar, localizar y seleccionar información, utilizando eficientemente plataformas tecnológicas y recursos variados para resolver preguntas relacionadas con las ciencias, tanto de forma individual como de forma colaborativa.",
          evaluationCriterias: {
            create: [
              {
                content: "CrEv 2.1 Trabajar de forma adecuada y versátil con medios variados, tradicionales y digitales, en la resolución de cuestiones, en la consulta de información y en la creación de contenidos, seleccionando con criterio las fuentes más fiables y citándolas correctamente."
              },
              {
                content: "CrEv 2.2 Reconocer la información con base científica, distinguiéndola de pseudociencias, bulos, creencias infundadas, etc., y manteniendo una actitud escéptica ante estos."
              }
            ]
          }
        }
      ]
    },
    // basicWisdoms: {
    //   create: [
    //     {
    //       content: "Metodología de la ciencia",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas."
    //           },
    //           {
    //             content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...)."
    //           },
    //           {
    //             content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico."
    //           },
    //           {
    //             content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos."
    //           },
    //           {
    //             content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados."
    //           },
    //           {
    //             content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso."
    //           }
    //         ]
    //       }
    //     },
    //     {
    //       content: "La materia y su medida",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen."
    //           },
    //           {
    //             content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas."
    //           },
    //           {
    //             content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido."
    //           }
    //         ]
    //       }
    //     }
    //   ]
    // }
  },
  {
    name: "Ciencias de la Naturaleza",
    academicYear: secundaria2,
    academicCycle: secundaria,
    region: {
      connect: {
        name: "País Vasco"
      }
    },
    locale: {
      connect: {
        name: "Euskera"
      }
    },
    specificSkills: {
      create: [
        {
          content: "KE1. Fenomeno eta prozesu natural nagusien kausak ulertzea eta erlazionatzea, arrazoibide zientifikoa, legeak eta teoria zientifikoak eta/edo pentsamendu konputazionala erabiliz, problemak ebazteko edo eguneroko bizitzako prozesuak azaltzeko.",
          evaluationCriterias: {
            create: [
              {
                content: "EbIr 1.1 Eguneroko fenomeno natural garrantzitsuenak azaltzea, printzipio, teoria eta lege zientifiko egokien arabera aztertzea eta adieraztea argudioak erabiliz."
              },
              {
                content: "EbIr 1.2 Prozesu naturalak azaltzea, ereduen eta diagramen bidez irudikatuz, eta, beharrezkoa denean, ingeniaritzako diseinuaren urratsak erabiliz (arazoa identifikatzea, miatzea, diseinatzea, sortzea, ebaluatzea eta hobetzea), tresna analogikoen eta digitalen bidez."
              },
              {
                content: "EbIr 1.3 Problemak ebaztea edo prozesu naturalak azaltzea, emandako ezagutzak, datuak eta informazioa, arrazoibide logikoa, pentsamendu konputazionala edo baliabide digitalak erabiliz."
              }
            ]
          }
        },
        {
          content: "KE2. Informazioa identifikatzea, lokalizatzea eta hautatzea, plataforma teknologikoak eta askotariko baliabideak eraginkortasunez erabiliz, zientziekin lotutako galderak banaka zein elkarlanean ebazteko.",
          evaluationCriterias: {
            create: [
              {
                content: "EbIr 2.1 Modu egokian eta moldakorrean lan egitea askotariko baliabideekin, baliabide tradizionalekin eta digitalekin, gaiak ebaztean, informazioa kontsultatzean eta edukiak sortzean, iturri fidagarrienak irizpidez hautatuz eta behar bezala aipatuz."
              },
              {
                content: "EbIr 2.2 Informazioa oinarri zientifikoarekin aztertzea, sasizientzietatik, gezurretatik, funtsik gabeko sinesmenetatik eta abar bereiziz, eta horien aurrean jarrera eszeptikoa edukiz."
              }
            ]
          }
        }
      ]
    },
    // basicWisdoms: {
    //   create: [
    //     {
    //       content: "Metodología de la ciencia",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas."
    //           },
    //           {
    //             content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...)."
    //           },
    //           {
    //             content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico."
    //           },
    //           {
    //             content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos."
    //           },
    //           {
    //             content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados."
    //           },
    //           {
    //             content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso."
    //           }
    //         ]
    //       }
    //     },
    //     {
    //       content: "La materia y su medida",
    //       basicWisdomItems: {
    //         create: [
    //           {
    //             content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen."
    //           },
    //           {
    //             content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas."
    //           },
    //           {
    //             content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido."
    //           }
    //         ]
    //       }
    //     }
    //   ]
    // }
  }
]

const basicWisdomsData = [
  [
    {
      content: "A. Las destrezas científicas básicas",
      children: [
        {
          content: "Aproximación a las metodologías de la investigación científica: identificación y formulación de cuestiones, elaboración de hipótesis y comprobación experimental de las mismas.",
          children: [{
            content: "Prueba!",
            children: [{
              content: "Ultima prueba!!",
                children: []
            }]
          }]
        },
        {
          content: "Introducción a los entornos y recursos de aprendizaje científico: el laboratorio y los entornos virtuales.",
          children: []
        },
        {
          content: "Iniciación al trabajo experimental mediante la realización de proyectos de investigación sencillos y de forma guiada.",
          children: []
        },
        {
          content: "Uso del lenguaje científico en la expresión de los resultados de un proyecto de investigación: unidades del Sistema Internacional y sus símbolos.",
          children: []
        },
        {
          content: "Valoración de la cultura científica y del papel de científicos en los principales hitos históricos y actuales de la física y la química.",
          children: []        
        }
      ]
    },
    {
      content: "B. La materia",
      children: [
        {
          content: "Aplicación de la teoría cinético-molecular a observaciones sobre la materia explicando sus propiedades, estados de agregación y la formación de mezclas y disoluciones.",
          children: []
        },
        {
          content: "Realización de experimentos sencillos y de forma guiada relacionados con los sistemas materiales para conocer y describir sus propiedades, su composición y su clasificación.",
          children: []
        },
        {
          content: "Estructura atómica: presentación del desarrollo histórico de los modelos atómicos y la ordenación de los elementos de la tabla periódica y su importancia para entender las uniones entre los átomos.",
          children: []
        }
      ]
    }
  ],
  [
    {
      content: "Metodología de la ciencia",
      children: [
        {
          content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas.",
          children: []
        },
        {
          content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...).",
          children: []
        },
        {
          content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico.",
          children: []
        },
        {
          content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos.",
          children: []
        },
        {
          content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados.",
          children: []        
        },
        {
          content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso.",
          children: []        
        }
      ]
    },
    {
      content: "La materia y su medida",
      children: [
        {
          content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen.",
          children: []
        },
        {
          content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas.",
          children: []
        },
        {
          content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido.",
          children: []
        }
      ]
    }
  ],
  [
    {
      content: "Metodologia de la ciència",
      children: [
        {
          content: "Contribució de les grans científiques i científics en el desenvolupament de les ciències físiques i químique,s.",
          children: []
        },
        {
          content: "Estratègies d’utilització d’eines digitals per a la cerca de la informació, la col·laboració i la comunicació de processos, resultats i idees en diferents formats ( presentació, pòster, informe, gràfic...).",
          children: []
        },
        {
          content: "Llenguatge científic i vocabulari específic de la matèria d’estudi en la comprensió d’informacions i dades, la comunicació de les pròpie s idees, la discussió raonada i l’argumentació sobre problemes de caràcter científic.",
          children: []
        },
        {
          content: "Procediments experimentals en laboratori: control de variables, presa (error en la mesura) i representació de les dades (taules i gràfics), anàlisi i interpretació d’aque stes.",
          children: []
        },
        {
          content: "Pautes del treball científic en la planificació i execució d’un projecte d’investigació en equip: identificació de preguntes i plantejament de problemes que puguen respondre’s, formulació d’hipòtesis, contrastació i posada a prova mitjançant l’experi mentació, i comunicació de resultats.",
          children: []        
        },
        {
          content: "Instruments, eines i tècniques pròpies del laboratori de Física i Química. Normes de seguretat en el laboratori. Resulta imprescindible conéixer les per a accedir al laboratori amb seguretat (primer cicle), però també reforçar les en cada curs.",
          children: []        
        }
      ]
    },
    {
      content: "La materia y su medida",
      children: [
        {
          content: "Magnituds físiques. Diversitat d’unitats, significats i ocupació. Necessitat de normalització: Sistema Internacional. Canvis d’unitats: massa, longitud, superfície i volum.",
          children: []
        },
        {
          content: "Mesura de volums de líquids: provetes, pipetes i buretes.",
          children: []
        },
        {
          content: "Volum ocupat per sòlids regulars i irregulars. Mètode geomètric i per desplaçament d’aigua o un altre líquid.",
          children: []
        }
      ]
    }
  ],
  [
    {
      content: "Metodología de la ciencia",
      children: [
        {
          content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas.",
          children: []
        },
        {
          content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...).",
          children: []
        },
        {
          content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico.",
          children: []
        },
        {
          content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos.",
          children: []
        },
        {
          content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados.",
          children: []        
        },
        {
          content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso.",
          children: []        
        }
      ]
    },
    {
      content: "La materia y su medida",
      children: [
        {
          content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen.",
          children: []
        },
        {
          content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas.",
          children: []
        },
        {
          content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido.",
          children: []
        }
      ]
    }
  ],
  [
    {
      content: "Metodología de la ciencia",
      children: [
        {
          content: "Contribución de las grandes científicas y científicos en el desarrollo de las ciencias Físicas y Químicas.",
          children: []
        },
        {
          content: "Estrategias de utilización de herramientas digitales para la búsqueda de la información, la colaboración y la comunicación de procesos, resultados e ideas en diferentes formatos (infografía, presentación, póster, informe, gráfico...).",
          children: []
        },
        {
          content: "Lenguaje científico y vocabulario específico de la materia de estudio en la comprensión de informaciones y datos, la comunicación de las propias ideas, la discusión razonada y la argumentación sobre problemas de carácter científico.",
          children: []
        },
        {
          content: "Procedimientos experimentales en laboratorio: control de variables, toma (error en la medida) y representación de los datos (tablas y gráficos), análisis e interpretación de los mismos.",
          children: []
        },
        {
          content: "Pautas del trabajo científico en la planificación y ejecución de un proyecto de investigación en equipo: identificación de preguntas y planteamiento de problemas que puedan responderse, formulación de hipótesis, contrastación y puesta a prueba a través de la experimentación, y comunicación de resultados.",
          children: []        
        },
        {
          content: "Instrumentos, herramientas y técnicas propias del laboratorio de Física y Química. Normas de seguridad en el laboratorio. Resulta imprescindible conocerlas para acceder al laboratorio con seguridad (primer ciclo) pero también reforzarlas en cada curso.",
          children: []        
        }
      ]
    },
    {
      content: "La materia y su medida",
      children: [
        {
          content: "Magnitudes físicas. Diversidad de unidades, significados y empleo. Necesidad de normalización: Sistema Internacional. Cambios de unidades: masa, longitud, superficie y volumen.",
          children: []
        },
        {
          content: "Medida de volúmenes de líquidos: probetas, pipetas y buretas.",
          children: []
        },
        {
          content: "Volumen ocupado por sólidos regulares e irregulares. Método geométrico y por desplazamiento de agua u otro líquido.",
          children: []
        }
      ]
    }
  ]
]

const ecosocialLearningData = [
  {
    group: ecodependencia,
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    content: "AES1 Interiorizar la ecodependencia humana. Ser conscientes de que formamos parte del entramado de la vida.",
    ecosocialEvaluationCriterias: {
      create: [
        {
          content: "CrEvES1.1 Reconocer la conexión existente entre todos los elementos vivos y no vivos que, en conjunto, constituyen las condiciones de vida de las que dependemos.",
          academicCycle: secundaria,
        },
        {
          content: "CrEvES1.2 Expresar que el ser humano depende del conjunto de los ecosistemas para su supervivencia / No expresar la autosuficiencia del ser humano.",
          academicCycle: secundaria,
        }
      ]
    },
    ecosocialBasicWisdoms: {
      create: [
        {
          content: "Concepto de ecodependencia. Imposibilidad del ser humano de satisfacer sus necesidades de manera autónoma, sin el concurso de la naturaleza.",
          academicCycle: primaria,
        },
        {
          content: "Principales funciones ecosistémicas: fertilización, depuración de agua y aire, polinización, regulación climática, etc.",
          academicCycle: primaria,
        },
        {
          content: "Concepto de ecodependencia. Imposibilidad del ser humano de satisfacer sus necesidades de manera autónoma, sin el concurso de la naturaleza.",
          academicCycle: secundaria,
        },
        {
          content: "Principales funciones ecosistémicas: fertilización, depuración de agua y aire, polinización, regulación climática, etc.",
          academicCycle: secundaria,
        }
      ]
    }
  },
  {
    group: ecodependencia,
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    content: "AES2 Venerar la biosfera. Valorar la importancia de la biodiversidad y de los ecosistemas. Tener una visión crítica de la idea de que la naturaleza existe para ser controlada y explotada por los seres humanos. Trascender del antropocentrismo al ecocentrismo.",
    ecosocialEvaluationCriterias: {
      create: [
        {
          content: "CrEvES2.1 Poner en valor la importancia y belleza de la diversidad de los seres vivos que viven en su entorno natural.",
          academicCycle: secundaria,
        },
        {
          content: "CrEvES2.2 Realizar acciones de protección de la biodiversidad / Mostrar actitudes en favor de los equilibrios ecosistémicos. ",
          academicCycle: secundaria,
        }
      ]
    },
    ecosocialBasicWisdoms: {
      create: [
        {
          content: "Conexión espiritual con la naturaleza, es decir, sentir vínculo y armonía con ella.",
          academicCycle: primaria,
        },
        {
          content: "Otras cosmovisiones sobre la relación con los ecosistemas (comunidades indígenas, campesinas, etc.).",
          academicCycle: primaria,
        },
        {
          content: "Conexión espiritual con la naturaleza, es decir, sentir vínculo y armonía con ella.",
          academicCycle: secundaria,
        },
        {
          content: "Otras cosmovisiones sobre la relación con los ecosistemas (comunidades indígenas, campesinas, etc.).",
          academicCycle: secundaria,
        }
      ]
    }
  },
  {
    group: funcionamientoDeLaBiosfera,
    locale: {
      connect: {
        name: "Castellano"
      }
    },
    content: "AES3 Interpretar la biosfera (y las sociedades) como sistemas complejos que se rigen por un funcionamiento no lineal, multicausal y con bucles de realimentación.",
    ecosocialEvaluationCriterias: {
      create: [
        {
          content: "CrEvES3.1 Explicar someramente el funcionamiento no lineal y multicausal de algunos sistemas complejos.",
          academicCycle: secundaria,
        },
        {
          content: "CrEvES3.2 Argumentar las implicaciones de traspasar los umbrales de activación de bucles de realimentación positiva en sistemas complejos. Por ejemplo, argumentar lo que puede suceder si aumenta la temperatura planetaria más de 1,5ºC.",
          academicCycle: secundaria,
        }
      ]
    },
    ecosocialBasicWisdoms: {
      create: [
        {
          content: "Introducción a las interacciones de las partes de un ecosistema: factores del entorno físico, seres vivos, clima, etc. Por ejemplo, la relación de los bosques con el clima y ciclo del agua",
          academicCycle: primaria,
        },
        {
          content: "Introducción a la interacción de los elementos culturales, políticos, económicos y ambientales.",
          academicCycle: primaria,
        },
        {
          content: "Sistemas complejos: imprevisibilidad, bucles de realimentación, procesos no lineales, umbrales, reacciones en cadena, cambios cualitativos al cambiar de escala, etc. Por ejemplo, el incremento de gases de efecto invernadero, incrementa la temperatura que a su vez provoca el deshielo, que a su vez incrementa el albedo que incrementa más la temperatura.",
          academicCycle: secundaria,
        },
        {
          content: "Funcionamiento de la Tierra como un todo. Imposibilidad de entender los ecosistemas como la suma de las partes (factores del entorno físico, seres vivos, clima, etc.). Interacción de los ecosistemas entre sí (terrestres y acuáticos).",
          academicCycle: secundaria,
        }
      ]
    }
  }
]

const createAndConnectBasicWisdoms = (basicWisdoms: Array<object>, parentId: number, courseId: string) => {
  basicWisdoms.forEach(async b => {
    const basicWisdom = await prisma.basicWisdomItem.create({
      data: {
        content: b.content,
        parentId,
        course: {
          connect: {
              id: courseId
          }
        }
      },
    })
    if(b.children.length > 0){
      createAndConnectBasicWisdoms(b.children,basicWisdom.id,courseId)
    }
  })
}

async function main() {
  console.log(`Start seeding ...`)
  for (const l of licenseData) {
    let salt = await bcrypt.genSalt(10)
    l.users.create.passwordHash = await bcrypt.hash(l.users.create.passwordHash,salt)
    const user = await prisma.license.create({
      data: l,
    })
    console.log(`Created user with id: ${user.id}`)
  }

  for (const l of localeData) {
    const locale = await prisma.locale.create({
      data: l,
    })
    console.log(`Created locale with id: ${locale.id}`)
  }

  courseData.forEach(async (c,i) => {
    const course = await prisma.course.create({
      data: c,
    })
    console.log("Creating and connecting basic wisdoms")
    createAndConnectBasicWisdoms(basicWisdomsData[i], '', course.id)
    console.log(`Created course with id: ${ course.id}`)
  })

  for (const e of ecosocialLearningData) {
    const ecosocialLearning = await prisma.ecosocialLearning.create({
      data: e,
    })
    console.log(`Created ecosocial learning with id: ${ ecosocialLearning.id}`)
  }

  console.log(`Seeding finished.`)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })