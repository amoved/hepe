import { authController, cryptoPort, loggerPort } from ".."

export default defineEventHandler(async (event) => {
    // loggerPort.debug(`[Middleware] Checking auth...`)
    try {
        const authorization = event.headers.get('authorization')
        // console.log(authorization)
        if(authorization) {
            const tokenData = cryptoPort.verifyAccessToken(authorization)
            if(tokenData){
                // Get session for internal operations
                const session = await authController.session(tokenData.id)
                event.context.auth = { ...session, token: authorization}
                // loggerPort.debug(`[Middleware] User ${tokenData.id} identified.`)
            }
        }
    } catch(e) {
        loggerPort.debug(`[Middleware] Error`)
        loggerPort.debug(e)
    }    
})
