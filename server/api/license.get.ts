import { Role } from "~/ddd/auth/domain/user";
import { authController, loggerPort } from "../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/license] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.ADMIN){
    const { id } = await getQuery(event)
    loggerPort.debug(`[api/license] Get license: ${id}`)

    const license = await authController.license(id)
    return license
  } else {
    loggerPort.info('[api/licenses] Not allowed')
  }
  return false
})