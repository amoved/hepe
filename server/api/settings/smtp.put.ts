import { configController } from "../../index";
import { z } from 'zod'

export default eventHandler(async (event) => {
  const smtpConfig = await readBody(event);
  console.log(`Store smtp config`)
  console.log(`Received params: ${smtpConfig.host}, ${smtpConfig.port}, ${smtpConfig.useTLS}, ${smtpConfig.email}, ${smtpConfig.password}`)

  // Define input schema
  const smptSchema = z.object({
      host: z.string(),
      port: z.string(),
      useTLS: z.boolean(),
      email: z.string().email(),
      password: z.string(),
  })

  // Validate input
  if(smptSchema.parse(smtpConfig)){
    const updated = await configController.updateSmtp(smtpConfig)
    console.log(`Update result:`)
    console.log(updated)

    if ( !updated ) {
        throw createError({
          statusCode: 403,
          statusMessage: "Error config SMTP",
        });
    } else {
      return updated;
    }
  } else {  
    console.log("Params are not okey")
    console.log(smtpConfig)
  }

  return false

  // configController.update()

});
