import { configController } from "../../../index";
import { z } from 'zod'

export default eventHandler(async (event) => {
  // Validate input
  const smtpConfig = await readBody(event);
  console.log(`Verify smtp`)

  const smptSchema = z.object({
      host: z.string(),
      port: z.string(),
      useTLS: z.boolean(),
      email: z.string().email(),
      password: z.string(),
  })

  if(smptSchema.parse(smtpConfig)){
    const validSMTP = await configController.verifySmtpConnection(smtpConfig)
    console.log(`Verification result: ${validSMTP}`)

    if ( !!!validSMTP ) {
        throw createError({
          statusCode: 403,
          statusMessage: $t('admin.smtp_test_error'),
        });
    } else {
      return validSMTP;
    }
  } else {  
    console.log("Params are not okey")
    console.log(smtpConfig)
  }

  return false
});
