import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {

  loggerPort.debug(`[api/education/generate-program] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { program } = await readBody(event)
    if(program.title != undefined && program.learningUnits != undefined  && program.texts != undefined) {
      // Use auth to load user Id and store the program asociated to it.
      loggerPort.debug(`[api/education/generate-program] User ${event.context.auth.id} is generating the downloadable program`)
      const blob = await educationController.generateProgramDoc(program.title, program.learningUnits, program.texts)
      event.node.res.setHeader('Content-Type',"application/vnd.openxmlformats-officedocument.wordprocessingml.document")
      event.node.res.setHeader('Content-Disposition',`attachment; filename=${new Date(Date.now()).toISOString().split('T')[0]}_program.docx;`)
      return blob
    } else {
      loggerPort.debug('[api/eductaion/generate-program] No program provided')
    }
  } else {
    loggerPort.info('[api/education/generate-program] Not allowed')
  }
  return false
  
})