import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../../index";

export default defineEventHandler(async (event) => {

  loggerPort.debug(`[api/education/program] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { id } = await readBody(event)
    if(id != undefined) {
      // Use auth to load user Id and store the program asociated to it.
      loggerPort.debug(`[api/education/program] User ${event.context.auth.id} has completed the program ${id}`)
      return educationController.completeProgram(id,event.context.auth.id) // cretate new program
    } else {
      loggerPort.debug('[api/eductaion/program] No program id provided')
    }
  } else {
    loggerPort.info('[api/education/progam] Not allowed')
  }
  return false
  
})