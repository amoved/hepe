import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/ecosocial-learnings] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
      const { ecosocialLearningIds, locale, academicYear } = await getQuery(event)
      if(locale == undefined || academicYear == undefined){
        loggerPort.error('[api/ecosocial-learnings] No locale or academic year provided')
        return false
      }
      if(ecosocialLearningIds != undefined) {
        const ecosocialLearnings = await educationController.ecosocialLearnings(locale, academicYear, ecosocialLearningIds)
        return ecosocialLearnings
      } else {
        const ecosocialLearnings = await educationController.ecosocialLearnings(locale, academicYear)
        return ecosocialLearnings
      }
      
  } else {
    loggerPort.info('[api/ecosocial-learnings] Not allowed')
  }
  
  return false
})