import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/locales] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { regionName } = await getQuery(event)
    if(regionName != undefined) {
      const locales = await educationController.locales(regionName)
      return locales
    } else {
      loggerPort.debug('[api/locales] No region was provided')
    }
  } else {
    loggerPort.info('[api/locales] Not allowed')
  }
  return false
})