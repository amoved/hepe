import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {

  loggerPort.debug(`[api/education/program] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
      // Use auth to load user Id and store the program asociated to it.
      loggerPort.debug(`[api/education/program] User ${event.context.auth.id} wants the program`)
      return educationController.getProgram(event.context.auth.id)
    
  } else {
    loggerPort.info('[api/education/progam] Not allowed')
  }
  return false
  
})