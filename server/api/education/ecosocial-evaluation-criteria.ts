import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/ecosocial-evaluation-criteria] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { ecosocialLearningIds, academicCycle } = await getQuery(event)
    if(ecosocialLearningIds != undefined || academicCycle != undefined) {
      const ecosocialEvaluationCriteria = await educationController.ecosocialEvaluationCriteria(ecosocialLearningIds.map(id => Number(id)),academicCycle)
      return ecosocialEvaluationCriteria
    } else {
      loggerPort.debug('[api/ecosocial-evaluation-criteria] No evaluation criteria ids or academic cycle was provided')
    }
  } else {
    loggerPort.info('[api/ecosocial-evaluation-criteria] Not allowed')
  }
  
  return false
})