import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/specific-skills] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { courseId } = await getQuery(event)
    if(courseId != undefined ) {
      const specificSkills = await educationController.specificSkills(Number(courseId))
      return specificSkills
    } else {
      loggerPort.debug('[api/specific-skills] No course was provided')
    }

  } else {
    loggerPort.info('[api/specific-skills] Not allowed')
  }
  
  return false
})