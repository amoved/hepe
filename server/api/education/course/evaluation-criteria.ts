import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/evaluation-criteria] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { specificSkillId } = await getQuery(event)
    if(specificSkillId != undefined ) {
      const evaluationCriteria = await educationController.evaluationCriteria(Number(specificSkillId))
      return evaluationCriteria
    } else {
      loggerPort.debug('[api/evaluation-criteria] No course was provided')
    }

  } else {
    loggerPort.info('[api/evaluation-criteria] Not allowed')
  }
  
  return false
})