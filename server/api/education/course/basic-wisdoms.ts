import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/basic-wisdoms] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { courseId } = await getQuery(event)
    if(courseId != undefined ) {
      const basicWisdoms = await educationController.basicWisdoms(Number(courseId))
      return basicWisdoms
    } else {
      loggerPort.debug('[api/basic-wisdoms] No course was provided')
    }

  } else {
    loggerPort.info('[api/basic-wisdoms] Not allowed')
  }
  
  return false
})