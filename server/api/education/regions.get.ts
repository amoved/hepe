import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/regions] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const regions = await educationController.regions()
    return regions
  } else {
    loggerPort.info('[api/regions] Not allowed')
  }
  return false
})