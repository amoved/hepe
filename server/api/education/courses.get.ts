import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/courses] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { regionName, localeName } = await getQuery(event)
    if(regionName != undefined || localeName != undefined ) {
      const courses = await educationController.courses(regionName, localeName)
      return courses
    } else {
      loggerPort.debug('[api/courses] No region was provided')
    }
  } else {
    loggerPort.info('[api/courses] Not allowed')
  }
  return false
})