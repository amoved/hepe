import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  // loggerPort.debug(`[api/course] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { id } = await getQuery(event)
    if(id != undefined ) {
      const course = await educationController.course(Number(id))
      return course
    } else {
      loggerPort.debug('[api/course] No id was provided')
    }
  } else {
    loggerPort.info('[api/course] Not allowed')
  }
  return false
})