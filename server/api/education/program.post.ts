import { Role } from "~/ddd/auth/domain/user";
import { educationController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {

  loggerPort.debug(`[api/education/program] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { id, programObj } = await readBody(event)
    if(programObj != undefined) {
      // Use auth to load user Id and store the program asociated to it.
      loggerPort.debug(`[api/education/program] User ${event.context.auth.id} is saving the program`)
      if(id == undefined){
        return educationController.updateProgram(-1, event.context.auth.id, programObj) // cretate new program
      } else {
        return educationController.updateProgram(id, event.context.auth.id, programObj)
      }
    } else {
      loggerPort.debug('[api/eductaion/program] No program provided')
    }
  } else {
    loggerPort.info('[api/education/progam] Not allowed')
  }
  return false
  
})