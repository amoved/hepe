import { authController } from "../index";

export default defineEventHandler(async (event) => {

  const { name, email, password, code, locale } = await readBody(event)

  return authController.register(name, email, password, code, locale)
})