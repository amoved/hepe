import { authController } from "../../index";

export default eventHandler(async (event) => {

  try {
    if(event.context.auth) {
      // Get user 
      const user = await authController.session(event.context.auth.id)
      // Return session data  
      return {
        user,
        token: event.context.auth.token
      }
    } else {
      return false
    }
  }
  catch(e){
    console.log(e)
    return false
  }
});
