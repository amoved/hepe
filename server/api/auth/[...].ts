import { NuxtAuthHandler } from '#auth'

export default NuxtAuthHandler({  
    secret: process.env.JWT_SECRET,
    pages: {    
        // Change the default behavior to use `/login` as the path for the sign-in page    
        signIn: '/login',
        newUser: '/register', 
    },
    providers: [
    ]   
})