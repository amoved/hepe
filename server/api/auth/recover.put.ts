import { authController } from "../../index";

export default eventHandler(async (event) => {
  // Validate input
  console.log(`Search email`)
  const { email } = await readBody(event);
  console.log(`Received params: ${email}`)

  const user = await authController.recover(email)
  console.log(`Recovery result: ${user}`)

  return {
    recoverySent: true
  }
});
