import { authController } from "../../index"

export default eventHandler(async (event) => {

  try {
    if(event.context.auth) {
      // Logout 
      const loggedOut = await authController.logout(event.context.auth.id)      
      // Return session data  
      return {
        loggedOut
      }
    } else {
      return false
    }
  }
  catch(e){
    console.log(e)
    return false
  }
});
