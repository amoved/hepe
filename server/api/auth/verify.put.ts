import { authController } from "../../index";

export default eventHandler(async (event) => {
  
  // this.loggerPort.info("[api/auth/verify] Received verification request")
  
  // Validate token
  const { temporaryAccessToken } = await readBody(event);
  const verified = await authController.verify(temporaryAccessToken)

  // Return token 
  return {
    verified
  };
});
