import { authController } from "../index";

export default defineEventHandler(async (event) => {

  const { id, name, maxUsers, usageLimit } = await readBody(event)
  
  return authController.updateLicense(id, name, Number(maxUsers), Number(usageLimit))
})