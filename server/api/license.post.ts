import { authController } from "../index";

export default defineEventHandler(async (event) => {

  const { name, maxUsers, usageLimit } = await readBody(event)
  
  return authController.createLicense(name, Number(maxUsers), Number(usageLimit))
})