import { Role } from "~/ddd/auth/domain/user";
import { authController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/license/usage-limit] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { id } = await getQuery(event)
    loggerPort.debug(`[api/license/usage-limit] Get license: ${id}`)

    const usageLimit = await authController.licenseUsageLimit(id)
    return usageLimit
  } else {
    loggerPort.info('[api/licenses/usage-limit] Not allowed')
  }
  return false
})