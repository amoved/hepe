import { Role } from "~/ddd/auth/domain/user";
import { authController, loggerPort } from "../../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/license/delete] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.USER || event.context.auth.role === Role.ADMIN){
    const { id } = await readBody(event)
    loggerPort.debug(`[api/license/delete] Delete license: ${id}`)

    const usageLimit = await authController.deleteLicense(id)
    return usageLimit
  } else {
    loggerPort.info('[api/licenses/delete] Not allowed')
  }
  return false
})