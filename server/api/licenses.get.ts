import { Role } from "~/ddd/auth/domain/user";
import { authController, loggerPort } from "../index";

export default defineEventHandler(async (event) => {
  // Check user permisions
  loggerPort.debug(`[api/licenses] User role is: ${event.context.auth.role}`)
  
  if(event.context.auth.role === Role.ADMIN){
    const licenses = await authController.licenses()
    return licenses
  } else {
    loggerPort.info('[api/licenses] Not allowed')
  }
  return false
})