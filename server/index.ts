import { AuthAdapterBcryptJWT } from './../ddd/shared/infrastructure/adapter.bcryptJwt';
import { AuthController } from '../ddd/auth/infrastructure/controller'
import { AuthRepositoryPrismaMySQL } from '~/ddd/auth/infrastructure/repository.mysqlPrisma';
import { AuthService } from "~/ddd/auth/application/auth.service";
import { LoggerAdapterWinston } from '~/ddd/log/infrastructure/adapter.winston';
import { config } from './config';
import { ConfigController } from '../ddd/config/infrastructure/controller';
import { ConfigService } from '../ddd/config/application/config.service';
import { ConfigRepositoryPrismaMySQL } from '../ddd/config/infrastructure/repository.mysqlPrisma';
import { MailerController } from '~/ddd/mailer/infrastructure/controller';
import { MailerService } from '~/ddd/mailer/application/mailer.service';
import type { MailerOptions } from '~/ddd/mailer/domain/ports/mailer.interface';
import { MailerAdapterEmailTemplates } from '~/ddd/mailer/infrastructure/adapter.emailTemplates';
import { EducationController } from '../ddd/education/infrastructure/controller'
import { EducationRepositoryPrismaMySQL } from '~/ddd/education/infrastructure/repository.mysqlPrisma';
import { EducationService } from "~/ddd/education/application/education.service";
import { EducationFileGeneratorDocx } from '~/ddd/education/infrastructure/adapter.docx';

// Auth service
export const loggerPort = new LoggerAdapterWinston(config);
export const cryptoPort = new AuthAdapterBcryptJWT();
const authRepository = new AuthRepositoryPrismaMySQL(loggerPort);
const authService = new AuthService(authRepository, cryptoPort, loggerPort);

export const authController = new AuthController(authService)

// Mail service
const mailerOptions: MailerOptions = {
    notifier: {
        fromName: config.appName,
        fromAddress: config.smtp.email,
        locale: config.locale,
    },
    smtpConnection: config.smtp,
}
const baseUrl = process.env.NODE_ENV == 'production' ? process.env.BASE_URL : 'http://localhost:3000' 
const mailerPort = new MailerAdapterEmailTemplates(mailerOptions)
const mailerService = new MailerService(baseUrl,mailerOptions,mailerPort,loggerPort)

export const mailerController = new MailerController(mailerService)

// Config service
const configRepository = new ConfigRepositoryPrismaMySQL(loggerPort);
const configService = new ConfigService(config, configRepository, cryptoPort, loggerPort, mailerPort);
configService.init()

export const configController = new ConfigController(configService)

// Education service
const educationRepository = new EducationRepositoryPrismaMySQL(loggerPort);
const educationFileGenerator = new EducationFileGeneratorDocx();
const educationService = new EducationService(educationRepository, educationFileGenerator, loggerPort);

export const educationController = new EducationController(educationService)

console.log("Controllers created!")