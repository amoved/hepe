import fs from 'fs'
import path from 'path'
import URL from 'url'

let config = {
  appName: process.env.APP_NAME || 'Plantilla Vue para amoved',
  locale: 'es',
  status: 'SETUP',
  baseurl: '',
  hostname: process.env.BASE_URL || '',
  server: {
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 13120
  },
  smtp: {
    host: process.env.SMTP_HOST || '',
    port: process.env.SMTP_PORT || '465',
    useTLS: !!process.env.SMTP_USE_TLS || true,
    email: process.env.SMTP_EMAIL || '',
    password: process.env.SMTP_PASSWORD || '',
  },
  log_level: 'debug',
  log_path: path.resolve(process.env.CWD || '', 'logs'),
  db: {},
  user_locale: path.resolve(process.env.CWD || '', 'user_locale'),
  upload_path: path.resolve(process.env.CWD || '', 'uploads'),
  proxy: {
    protocol: process.env.APP_PROXY_PROTOCOL || '',
    hostname: process.env.APP_PROXY_HOSTNAME || '',
    host: process.env.APP_PROXY_HOST || '',
    port: process.env.APP_PROXY_PORT || '',
    auth: {
      username: process.env.APP_PROXY_USERNAME || '',
      password: process.env.APP_PROXY_PASSWORD || ''
    },
    headers: process.env.APP_PROXY_HEADERS && JSON.parse(process.env.APP_PROXY_HEADERS) || {}
  },
  write (config_path= process.env.config_path || './config.json') {
    delete config.status
    return fs.writeFileSync(config_path, JSON.stringify(config, null, 2))
  },
  load () {
    // load configuration from file
    const config_path = process.env.config_path || './config.json'
    console.info(`> Reading configuration from: ${config_path}`)
    if (fs.existsSync(config_path)) {
      const configContent = fs.readFileSync(config_path)
      config = Object.assign(config, JSON.parse(configContent))
      config.status = 'CONFIGURED'
      if (!config.hostname) {
        config.hostname = new URL.URL(config.baseurl).hostname
      }
    } else {
      config.status = 'SETUP'
      console.info('> Configuration file does not exists, running setup..')
    }
  }
}

config.load()
export { config }