import { EcosocialLearningGroup } from "~/ddd/education/domain/ecosocialLearningGroup"
import { bordersColors } from "~/plugins/vuetify"

export function ecosocialLearningGroupToColor(ecosocialLearningGroup: EcosocialLearningGroup | string){
    let color = ''
    switch(ecosocialLearningGroup) {
        case 'ECODEPENDENCIA':
        case EcosocialLearningGroup.ECODEPENDENCIA:
            color = bordersColors.green
            break
        case 'FUNCIONAMIENTO_DE_LA_BIOSFERA':    
        case EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA:
            color = bordersColors.grey
            break
        case 'CRISIS_CIVILIZATORIA':    
        case EcosocialLearningGroup.CRISIS_CIVILIZATORIA:
            color = bordersColors.blue
            break
        case 'AGENTES_DE_CAMBIO_SOCIAL':    
        case EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL:
            color = bordersColors.red
            break
        case 'DESARROLLO_PERSONAL_Y_COMUNITARIO':    
        case EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO:
            color = bordersColors.pink
            break
        case 'JUSTICIA':    
        case EcosocialLearningGroup.JUSTICIA:
            color = bordersColors.teal
            break
        case 'DEMOCRACIA':    
        case EcosocialLearningGroup.DEMOCRACIA:
            color = bordersColors.violet
            break
        case 'TECNICAS_ECOSOCIALES':    
        case EcosocialLearningGroup.TECNICAS_ECOSOCIALES:
            color = bordersColors.orange
            break
    }
    return color

}

export function stringToEcosocialLearningGroup(group: string): EcosocialLearningGroup {
    switch(group) {
        case 'Ecodependencia':
            return EcosocialLearningGroup.ECODEPENDENCIA
        case 'Funcionamiento de la biosfera':    
            return EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA
        case 'Crisis civilizatoria':    
            return EcosocialLearningGroup.CRISIS_CIVILIZATORIA
        case 'Agentes de cambio social':    
            return EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL
        case 'Desarrollo personal y comunitario':    
            return EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO
        case 'Justicia':    
            return EcosocialLearningGroup.JUSTICIA
        case 'Democracia':    
            return EcosocialLearningGroup.DEMOCRACIA
        case 'Tecnicas ecosociales':    
            return EcosocialLearningGroup.TECNICAS_ECOSOCIALES
    }
    return EcosocialLearningGroup.ECODEPENDENCIA
}