export function academicYearToAcademicCycle(academicYear: string): string {

    switch(academicYear){
        case 'INFANTIL_1':
        case 'INFANTIL_2':
            return 'INFANTIL'
        case 'PRIMARIA_1':
        case 'PRIMARIA_2':
        case 'PRIMARIA_3':
        case 'PRIMARIA_4':
        case 'PRIMARIA_5':
        case 'PRIMARIA_6':
            return 'PRIMARIA'
        case 'SECUNDARIA_1':
        case 'SECUNDARIA_2':
        case 'SECUNDARIA_3':
        case 'SECUNDARIA_4':
            return 'SECUNDARIA'
    }

    return ''
}