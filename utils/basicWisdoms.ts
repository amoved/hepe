
// Returns the last basic wisdoms independently of the tree levels
export function getLeafBWs(basicWisdomTree: BasicWisdomTree): Array<BasicWisdom> {
    let leafBWs: Array<BasicWisdom> = []
    if(basicWisdomTree.children.length > 0) {
        basicWisdomTree.children.forEach((b: BasicWisdomTree) => leafBWs = leafBWs.concat(getLeafBWs(b)))
    } else {
        return [{
            id: basicWisdomTree.id,
            content: basicWisdomTree.content
        }]
    }
    return leafBWs
}