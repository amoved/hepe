import { EcosocialLearningGroup } from "~/ddd/education/domain/ecosocialLearningGroup"

export function ecosocialLearningGroupToLocaleIndex(ecosocialLearningGroup: EcosocialLearningGroup | string){
    let index = ''
    switch(ecosocialLearningGroup) {
        case 'ECODEPENDENCIA':
        case EcosocialLearningGroup.ECODEPENDENCIA:
            index = 'ecosocialLearningGroupEcodependencia'
            break
        case 'FUNCIONAMIENTO_DE_LA_BIOSFERA':    
        case EcosocialLearningGroup.FUNCIONAMIENTO_DE_LA_BIOSFERA:
            index = 'ecosocialLearningGroupFuncionamientoDeLaBiosfera'
            break
        case 'CRISIS_CIVILIZATORIA':    
        case EcosocialLearningGroup.CRISIS_CIVILIZATORIA:
            index = 'ecosocialLearningGroupCrisisCivilizatoria'
            break
        case 'AGENTES_DE_CAMBIO_SOCIAL':    
        case EcosocialLearningGroup.AGENTES_DE_CAMBIO_SOCIAL:
            index = 'ecosocialLearningGroupAgentesDeCambioSocial'
            break
        case 'DESARROLLO_PERSONAL_Y_COMUNITARIO':    
        case EcosocialLearningGroup.DESARROLLO_PERSONAL_Y_COMUNITARIO:
            index = 'ecosocialLearningGroupDesarrolloPersonalYComunitario'
            break
        case 'JUSTICIA':    
        case EcosocialLearningGroup.JUSTICIA:
            index = 'ecosocialLearningGroupJusticia'
            break
        case 'DEMOCRACIA':    
        case EcosocialLearningGroup.DEMOCRACIA:
            index = 'ecosocialLearningGroupDemocracia'
            break
        case 'TECNICAS_ECOSOCIALES':    
        case EcosocialLearningGroup.TECNICAS_ECOSOCIALES:
            index = 'ecosocialLearningGroupTecnicasEcosociales'
            break
    }
    return index

}