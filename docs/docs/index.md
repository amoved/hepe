# __Herramienta Escolar para la Programación Ecosocial__

Esta aplicación web pretende servir de ayuda al profesorado para la creación de programaciones escolares. Facilita la creación de programaciones que integren el currículo oficial y el ecosocial (desarrollado y mantenido por FUHEM). 

A lo largo de la documentación veremos cómo está diseñada la aplicación así como los componentes que la conforman.

## 🆎 Terminología 

Para la creación de una programación, el profesorado deberá completar un total de 10 pasos. A lo largo de estos pasos se irán concretando los Criterios de Evaluación (CrEv) asociados a cada Competencia Específica curricular (CE) así como los Aprendizajes Ecosociales (AES), los Criterios de  Evaluación Ecosociales (CrEvES), los Saberes Básicos Ecosociales (SBES) y los Saberes Básicos curriculares (SB). Además el profesorado podrá crear Saberes Básicos Ecosociales Definidos por la Usuaria (SBESDU). Esta terminología se encuentra presente en el código con las siguientes abreviaturas escogidas del inglés y _por comodidad para las programadoras serán las que utilicemos a lo largo de la documentación_:

- Competencia Específica curricular (CE) --> **Specific Skill (SS)**
- Criterios de Evaluación (CrEv) --> **Evaluation Criteria (EvCr)**
- Saberes Básicos curriculares (SB) --> **Basic Wisdom (BW)**
- Aprendizajes Ecosociales (AES) --> **Ecosocial Learning (EL)**
- Criterios de  Evaluación Ecosociales (CrEvES) --> **Ecosocial Evaluation Criteria (EEvCr)**
- Saberes Básicos Ecosociales (SBES) --> **Ecosocial Basic Wisdom (EBW)**
- Saberes Básicos Ecosociales Definidos por la Usuaria (SBESDU) --> **User Defined Ecosocial Basic Wisdom (UDEBW)**

## ⚙️ Desgranando los distintos pasos del proceso 

A continuación describiremos las lógicas asociadas a cada paso del proceso.

### Paso 8: Concreción de los saberes básicos

En este paso se muestran todos los saberes básicos (tanto los ecosociales, como los curriculares o los definidos por la usuaria).

Para ello debemos fusionar el mapeo de saberes básicos ecosociales (matrixEEvCrToEBW) en un único listado (partialEBWs).

#### Gestión del estado cuando retrocedemos pasos

Al retroceder en el proceso nos apoyaremos en el mapeo previamente definido para trasladar la información de este paso, en este sentido:

- Tendremos que limipiar del mapeo los saberes básicos que hemos descartado en el paso 8


