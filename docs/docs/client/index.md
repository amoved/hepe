# __Cliente__

El cliente de la aplicación está desarrollado sobre el framework Nuxt.js de Vue y utiliza Vue3, a continuación se muestran las dependencias del cliente:

- **Node** v20.10
- **Typescript** v5.2.2
- **Vue** v3.3.6
- **Nuxt** v3.11.2
- **Pug** v3.0.2
- **Plugin de Pug para Vue** v2.0.19
- **Sass** v1.71.1
- **Nuxt i18n** v8.0.0-rc.5
- **Vuetify** v3.4.0-alpha.1
- **MDI/font** v7.3.67
- **Pinia** v2.1.7
- **Plugin de Pinia para Nuxt** v0.5.1
- **Vee-validate plugin para Nuxt** v4.12.2
- **Plugin de Zod para Vee-validate** v4.12.2
- **Vuedraggable** v4.1.0

A lo largo de los tutoriales vamos a ir recorriendo cada una de las dependencias para mostrar cómo se han integrado en el código y cómo podríamos extenderlo.

## API

La aplicación cuenta con una serie de _composables_ que facilitan la obtención de algunos datos. 


