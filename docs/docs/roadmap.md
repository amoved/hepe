# Hoja de ruta

En este documento recogemos las funcionalidades a desarrollar, para ello redactamos las historias de usuario utilizando los principios INVEST. Para ello nos basamos en este [artículo sobre "How to write user stories" de Stormotion](https://stormotion.io/blog/how-to-write-a-good-user-story-with-examples-templates/)

Utilizamos un sistema de clasificación de las diferentes funcionalidades mapeadas durante las sesiones de diseño para identificar aquellas que son imprescindibles de las que serían recomendables y las ideales:

!!! error "Funcionalidad necesaria"
    Funcionalidad necesaria y dentro de nuestro alcance, son la prioridad.

!!! warning "Funcionalidad recomendada"
    Una funcionalidad recomendada pero que queda fuera de nuestro alcance actual.

!!! important "Funcionalidad deseable"
    Una funcionalidad que mejoraría la herramienta, es opcional y queda fuera de nuestro alcance.


## Alcance del proyecto

En nuestro caso, el alcance actual del proyecto está contemplado en la documentación privada interna. Consiste en un pliegue de condiciones donde se especifican las funcionalidades necesarias. Contacta con la persona en coordinación para tener más información.

---

## **HEPE - v0.1**

La fase inicial del proyecto cubre los siguientes objetivos de desarrollo:

### Hito | **Paso 1: presentación de la programación ecosocial**

"Como persona usuaria quiero tener un punto de acceso a la aplicación para saber cómo empezar a interactuar con ella."


#### Épica: Página de incio

??? error "Como persona usuaria, la primera vez que accedo a la aplicación, quiero tener un resumen del proceso a completar para estar informada."
    - La primera vez que se hace login, salta un modal con una infografía.
    - Al pinchar fuera del modal se oculta la infografía.

!!! error "Como persona usuaria quiero ver una presentación en texto para que me invite a reflexionar sobre la programación ecosocial."
    - 


#### Épica: Formulario de contexto

!!! error "Como persona usuaria quiero seleccionar la región para la cual estaré desarrollando el programa." 
    - Formulario con campos con valores por defecto.
    - Validación de campos.

!!! warning "As a user I want to filter reported cases by proffesion, region and type of case so I can have a segregated reference of the problem." 
    - Three different dropdowns let the user filter the cases by proffesion, city and type of case. 
    - Once a filter is selected the map markers are updated.

### Hito | **Autoguardado**

"Como persona usuaria quiero tener un punto de acceso a la aplicación para saber cómo empezar a interactuar con ella."


#### Épica: Página de incio

!!! error "Como persona usuaria, la primera vez que accedo a la aplicación, quiero tener un resumen del proceso a completar para estar informada."
    - La primera vez que se hace login, salta un modal con una infografía.
    - Al pinchar fuera del modal se oculta la infografía.

!!! error "Como persona usuaria quiero ver una presentación en texto para que me invite a reflexionar sobre la programación ecosocial."
    - 


#### Épica: Formulario de contexto

!!! error "Como persona usuaria quiero seleccionar la región para la cual estaré desarrollando el programa." 
    - Formulario con campos con valores por defecto.
    - Validación de campos.

!!! warning "As a user I want to filter reported cases by proffesion, region and type of case so I can have a segregated reference of the problem." 
    - Three different dropdowns let the user filter the cases by proffesion, city and type of case. 
    - Once a filter is selected the map markers are updated.