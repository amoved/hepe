# __Servidor__

El servidor de HEPE contiene toda la _lógica de negocio_ de la aplicación, está diseñado en base a una arquitectura DDD (Domain Driven Desing) perteneciente a la familia de Arquitecturas Limpias y divide la solución en 4 microservicios bien diferenciados:

- __Auth__: gestión de de usuarios y licencias de acceso.
- __Config__: gestión de la configuración global de la aplicación.
- __Notifier__: servicio de notificaciones, implemente notificaciones por correo.
- __Education__: implementa la lógica de negocio de creación de una programación escolar.

## Dependencias
El servidor de HEPE utiliza el framework Nuxt junto con Vue3 para implementar sus servicios. 
A continuación encontramos un listado de las dependencias utilizadas:


