import { EcosocialLearningGroup } from "~/ddd/education/domain/ecosocialLearningGroup"

export function useEcosocialLearningGroups(): object {
    const ecosocialLearningGroups: Ref<Array<string | EcosocialLearningGroup>> = ref(Object.keys(EcosocialLearningGroup).filter((v) => isNaN(Number(v))))
    return { ecosocialLearningGroups }
}