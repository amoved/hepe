export function useSpecificSkillsIndexes(
    specificSkills: Array<SpecificSkill>,
    basicWisdoms: Array<BasicWisdom>,
    sSId: number, 
    evCrId: number = -1, 
    bWId: number = -1
): SSIndexes {

    const sSIndex = specificSkills.findIndex(sS => sS.id == sSId)
    const sS = specificSkills[sSIndex]
    let evCrIndex = -1
    if(evCrId != -1){
        evCrIndex = sS.evCrs.findIndex(e => e.id == evCrId)
    }
    let bWIndex = -1
    if (bWId != -1) {
        bWIndex = basicWisdoms.findIndex(b => b.id == bWId)
    }

    return {
        sSIndex,
        evCrIndex,
        bWIndex
    }
}