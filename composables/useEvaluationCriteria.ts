export async function useEvaluationCriteria(specificSkillId: string): object {

    const { data: session } = useAuthState()

    // console.log("[useEvaluationCriteria] Getting specific skill evaluation criteria...")
    const { data: evaluationCriteria } = await useFetch('/api/education/course/evaluation-criteria', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            specificSkillId,
        }
    })

    return { evaluationCriteria }
}