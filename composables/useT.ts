// source: https://stackoverflow.com/questions/77594888/how-to-use-i18n-messages-in-a-nuxt3-pinia-store
export function useT (key: string, options: object = {}): string {
    return useNuxtApp().$i18n.t(key, options)
}