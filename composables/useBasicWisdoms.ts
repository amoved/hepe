export async function useBasicWisdoms(courseId: string): object {

    const { data: session } = useAuthState()

    const { data: basicWisdoms } = await useFetch('/api/education/course/basic-wisdoms', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            courseId,
        }
    })

    return { basicWisdoms }
}