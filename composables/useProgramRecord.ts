export async function useProgramRecord(): object {

    const { data: session } = useAuthState()

    const { data } = await useFetch('/api/education/program', {
        method: "get",
        headers: {
            authorization: session.value.token
        }
    })

    // Return first entry
    return { programRecord: data.value[0] }
}