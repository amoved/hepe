
export function useLearningUnitBWIndexes(
    learningUnits: Array<LearningUnit>,
    lUName: string,
    basicWisdoms: Array<BasicWisdom> = [],
    bWId: number = -1,
    isUserDefined: boolean = false
): LUBWIndexes {

    const lUIndex = learningUnits.findIndex(lU => lU.name == lUName)
    const lU: LearningUnit = learningUnits[lUIndex]
    let pBWIndex = -1
    let uDBWIndex = -1
    if(isUserDefined){
        uDBWIndex = lU.filterUDBWs.findIndex((isAdded,i) => lU.uDBWs[i].id == bWId)
    } else {
        pBWIndex = lU.partialBWs.findIndex(indexes => basicWisdoms[indexes.bWIndex].id == bWId)
    }

    return {
        lUIndex,
        pBWIndex,
        uDBWIndex
    }
}