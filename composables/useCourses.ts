export async function useCourses(regionName: string, localeName: string): object {

    const { data: session } = useAuthState()

    // console.log("[useCourses] Getting courses...")
    const { data: courses } = await useFetch('/api/education/courses', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            regionName,
            localeName
        }
    })

    return { courses }
}