export function useSort(): object {

    const { locale } = useI18n()

    function sortAlphabetically<T>(arr: Array<T>): Array<T> {
        arr.sort((s1: T, s2: T) => s1.content.localeCompare(s2.content,locale,{numeric: true}))
        return arr
    }

    const sortSSAlphabetically = sortAlphabetically<SpecificSkill>
    const sortEvCrAlphabetically = sortAlphabetically<EvaluationCriteria>
    const sortBwAlphabetically = sortAlphabetically<BasicWisdom>
    const sortELAlphabetically = sortAlphabetically<EcosocialLearning>
    const sortEEvCrAlphabetically = sortAlphabetically<EcosocialEvaluationCriteria>

    return { 
        sortSSAlphabetically,
        sortEvCrAlphabetically,
        sortBwAlphabetically,
        sortELAlphabetically,
        sortEEvCrAlphabetically
    }
}

