export async function useSpecificSkills(courseId: string): object {

    const { data: session } = useAuthState()

    const { data: specificSkills } = await useFetch('/api/education/course/specific-skills', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            courseId,
        }
    })

    return { specificSkills }
}