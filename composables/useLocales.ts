export async function useLocales(regionName: string): object {

    const { data: session } = useAuthState()

    // console.log("[useLocales] Getting locales...")
    const { data: localesObjectsArray } = await useFetch('/api/education/locales', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            regionName
        }
    })

    // console.log(localesObjectsArray)
    
    const locales = ref([])
    if(localesObjectsArray.value.length > 0) {
        // console.log("[useLocales] Procesing results...")
        localesObjectsArray.value.forEach(l => {
            locales.value.push(l.name)    
        });
    } else {
        console.log("[useLocales] No results found")
    }

    return { locales }
}