export async function useRegions (): Ref<Array<string>> {

    const { data: session } = useAuthState()

    // console.log("[useRegions] Getting regions...")
    const { data: regionObjectsArray } = await useFetch('/api/education/regions', {
        method: "get",
        headers: {
            authorization: session.value.token
        }
    })
    
    const regions = ref([])
    if(regionObjectsArray.value.length > 0) {
        // console.log("[useRegions] Procesing results...")
        regionObjectsArray.value.forEach(r => {
            regions.value.push(r.name)    
        });
    } else {
        console.log("[useRegions] No results found")
    }

    return { regions }
}