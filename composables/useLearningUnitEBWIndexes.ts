export function useLearningUnitEBWIndexes(
    learningUnits: Array<LearningUnit>,
    lUName: string,
    ecosocialLearnings: Array<EcosocialLearning> = [],
    eLId: number = -1, 
    eBWId: number = -1,
    isUserDefined: boolean = false
): LUEBWIndexes {

    const lUIndex = learningUnits.findIndex(lU => lU.name == lUName)
    const lU: LearningUnit = learningUnits[lUIndex]
    let eLIndex = -1
    let eBWIndex = -1
    let uDEBWIndex = -1
    if(ecosocialLearnings.length > 0){
        eLIndex = ecosocialLearnings.findIndex(eL => eL.id == eLId)
        const eL: EcosocialLearning = ecosocialLearnings[eLIndex]
        if(eL && eBWId != -1 && isUserDefined){
            uDEBWIndex = eL.uDEBWs.findIndex(b => b.id == eBWId)
        } else if (eL && eBWId != -1) {
            eBWIndex = eL.eBWs.findIndex(b => b.id == eBWId)
        }
    }

    return {
        lUIndex,
        eLIndex,
        eBWIndex,
        uDEBWIndex
    }
}