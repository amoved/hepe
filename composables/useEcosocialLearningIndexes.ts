export function useEcosocialLearningIndexes(
    ecosocialLearnings: Array<EcosocialLearning>,
    eLId: number, 
    eEvCrId: number = -1, 
    eBWId: number = -1,
    isUserDefined: boolean = false
): ELIndexes {

    const eLIndex = ecosocialLearnings.findIndex(eL => eL.id == eLId)
    const eL = ecosocialLearnings[eLIndex]
    let eEvCrIndex = -1
    if(eEvCrId != -1){
        eEvCrIndex = eL.eEvCrs.findIndex(e => e.id == eEvCrId)
    }
    let eBWIndex = -1
    if(eBWId != -1 && isUserDefined){
        eBWIndex = eL.uDEBWs.findIndex(b => b.id == eBWId)
    } else if (eBWId != -1) {
        eBWIndex = eL.eBWs.findIndex(b => b.id == eBWId)
    }

    return {
        eLIndex,
        eEvCrIndex,
        eBWIndex
    }
}