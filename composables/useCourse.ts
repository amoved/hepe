export async function useCourse(id: string): object {

    const { data: session } = useAuthState()

    const { data: course } = await useFetch('/api/education/course', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            id
        }
    })

    return { course }
}