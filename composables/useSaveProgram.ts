export function useSaveProgram(): object {
    const { data: session } = useAuthState()
    const { data: auth } = useAuth()
    const programStore = useProgramStore()
    const { id } = storeToRefs(programStore)
    const saved: Ref<boolean> = ref(false)
    const loading: Ref<boolean> = ref(false)

    function trimProgram(): object {
        // Deep copy the store state
        let programObj: ProgramState = JSON.parse(JSON.stringify(programStore.$state))

        // Remove all the content
        programObj.specificSkills.forEach(sS => {
            delete sS.content
            sS.evCrs.forEach(evCr => delete evCr.content)
        })
        programObj.basicWisdoms.forEach(bW => {
            delete bW.content
        })
        programObj.ecosocialLearnings.forEach(eL => {
            delete eL.content 
            eL.eEvCrs.forEach(eEvCr => delete eEvCr.content)
            eL.eBWs.forEach(eBW => delete eBW.content)
        })

        return programObj
    }

    async function save() {
        const programObj = trimProgram()
        if(!saved.value){
            loading.value = true
            const { data: savedProgram } = await useFetch('/api/education/program', {
                method: "post",
                headers: {
                    authorization: session.value.token
                },
                body: {
                    id: id.value,
                    userId: auth.value.user.id,
                    programObj,
                }
            })
            if(id.value == -1) {
                programStore.setId(savedProgram.value.id)
            }
            console.log("[save] Saved")
        } else {
            console.log("[save] Nothing to save")
        }
        
        saved.value = true
        loading.value = false
    }

    programStore.$subscribe((mutation) => {
        saved.value = false
    })

    const autoSave = async () => {
        console.log("Auto save...")
        await save()
        setTimeout(autoSave,180000) // Save updates each 3min
    }

    return {
        startAutoSave: () => {
            onNuxtReady(autoSave)
        },
        // stopAutoSave: () => {},
        loading,
        save,
        saved
    }
}