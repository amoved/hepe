export async function useCanCreateProgram(session: object): object {

    // const { getSession } = useAuth()
    // const { data: session } = useAuthState()

    // await getSession({ force: true })
    const { licenseUsageCounter, licenseId } = session.user

    // Get license usage limit
    const { data: limit } = await useFetch('/api/license/usage-limit', {
        method: "get",
        headers: {
            authorization: session.token
        },
        query: {
            id: licenseId
        }
    })
    const usageLimit = limit.value
    const canCreateProgram = licenseUsageCounter < usageLimit

    return {
        licenseUsageCounter,
        usageLimit,
        canCreateProgram
    }

}