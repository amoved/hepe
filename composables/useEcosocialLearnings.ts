interface UseEcosocialLearningsOptions {
    academicYear: string,
    locale: string
    ecosocialLearningIds?: Array<string>,
}

export async function useEcosocialLearnings(options: UseEcosocialLearningsOptions ): object {

    const { data: session } = useAuthState()

    const { data: ecosocialLearnings } = await useFetch('/api/education/ecosocial-learnings', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: options
    })

    return { ecosocialLearnings }
}