export async function useCompleteProgram(id: string): Promise<boolean> {

    const { data: session } = useAuthState()

    const { data } = await useFetch('/api/education/program/complete', {
        method: "post",
        headers: {
            authorization: session.value.token
        },
        body: {
            id
        }
    })

    return data.value
}