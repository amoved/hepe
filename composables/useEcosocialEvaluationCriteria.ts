export async function useEcosocialEvaluationCriteria(ecosocialLearningIds: Array<string>, academicCycle: string): object {

    const { data: session } = useAuthState()

    console.log("[useEcosocialEvaluationCriteria] Getting ecosocial learnings from a specific academic cycle...")
    const { data: ecosocialEvaluationCriteria } = await useFetch('/api/education/ecosocial-evaluation-criteria', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            ecosocialLearningIds,
            academicCycle
        }
    })

    return { ecosocialEvaluationCriteria }
}