export function useStep(): object {
    const programStore = useProgramStore()
    const { currentStep } = storeToRefs(programStore)
    const totalSteps = 10

    function incrementStep() {
        if(currentStep.value < totalSteps) {
            programStore.setCurrentStep(currentStep.value+1)
        }
    }

    function decrementStep() {
        if(currentStep.value > 1) {
            programStore.setCurrentStep(currentStep.value-1)
        }
    }

    // Only for develoment purposes
    function setStep(step: number) {
        if(step >= 1 && step <= totalSteps){
            programStore.setCurrentStep(step)
        } else {
            console.log("[stepStore] Incorrect step provided")
        }
    } 

    return {
        currentStep,
        totalSteps,
        incrementStep,
        decrementStep,
        setStep,
    }
}