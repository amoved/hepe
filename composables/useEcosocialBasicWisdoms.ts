export async function useEcosocialBasicWisdoms(ecosocialEvaluationCriteriaIds: Array<string>): object {

    const { data: session } = useAuthState()

    console.log("[useEcosocialBasicWisdoms] Getting ecosocial basic wisdoms from evaluation criteria...")
    const { data: ecosocialBasicWisdoms } = await useFetch('/api/education/ecosocial-evaluation-criteria', {
        method: "get",
        headers: {
            authorization: session.value.token
        },
        query: {
            ecosocialEvaluationCriteriaIds,
        }
    })

    return { ecosocialBasicWisdoms }
}